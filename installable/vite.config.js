import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue2'
import laravel from 'laravel-vite-plugin';
import path from "path";
import { nodePolyfills } from 'vite-plugin-node-polyfills'
import {viteStaticCopy} from 'vite-plugin-static-copy';

export default defineConfig({
    build: {
        minify: true,
        chunkSizeWarningLimit: 900,
        rollupOptions: {
            output:{
                manualChunks(id) {
                    if (id.includes('node_modules')) {
                        return id.toString().split('node_modules/')[1].split('/')[0].toString();
                    }
                }
            }
        }
    },
    plugins: [
        nodePolyfills(),
        viteStaticCopy({
            targets: [
                {src: path.join(__dirname, '/resources/svg'), dest: path.join(__dirname, '/public')},
            ],
        }),
        laravel({
            input: [
                'resources/js/app.js',
                'resources/js/enso.js',
                'resources/sass/enso.scss',
            ],
            refresh: true,
        }),
        vue({}),
    ],
    resolve: {
        alias: [
            { find: 'vue', replacement: 'vue/dist/vue.esm.js' },
            { find: "@svg", replacement: path.resolve(__dirname, "./resources/svg") },
            { find: "@fire", replacement: path.resolve(__dirname, "./enso-assets/node_modules/") }
        ]
    }
});
