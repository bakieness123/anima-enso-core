<?php

return [

    'type' => 'generic',

    'types' => [
        'generic' => \Yadda\Enso\Crud\Forms\Rows\SignupForms\Generic::class,
    ],

];
