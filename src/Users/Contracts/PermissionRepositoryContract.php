<?php

namespace Yadda\Enso\Users\Contracts;

use Illuminate\Support\Collection;

/**
 * Interface that defines a permission repository
 */
interface PermissionRepositoryContract
{

    /**
     * Set the current Permission list
     *
     * @return \Illuminate\Support\Collection Permission list to set
     */
    public function getPermissionList();

    /**
     * Set the current Permission list
     *
     * @param \Illuminate\Support\Collection $permission_list Permission list to set
     */
    public function setPermissionList(Collection $permission_list);

    /**
     * Accepts a mixed set of data, and parses it to find the return a set array
     * of ids of all the matching Permission items.
     *
     * @param mixed $identifiers data to work from
     *
     * @return array ids of matching Permissions
     */
    public function findIdsFromMixed($identifiers);

    /**
     * Checks to see whether any of a given, mixed list of permissions or identifiers
     * are present within a set of Permission.
     *
     * @todo Andrew sort these params out please
     *
     * @param mixed $permission_list List of permissions to check against
     * @param mixed $permissions     List of permissions to look for
     * @param bool  $all             Whether to match all or just one
     * @return bool Success status
     */
    public function permissionListContainsPermissions($permissions, $all = false);
}
