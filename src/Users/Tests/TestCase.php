<?php

namespace Yadda\Enso\Users\Tests;

use Faker\Factory as FakerFactory;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Filesystem\ClassFinder;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Yadda\Enso\Users\EnsoUserServiceProvider;

abstract class TestCase extends BaseTestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    protected $roles;

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../vendor/laravel/laravel/bootstrap/app.php';

        $app->register(EnsoUserServiceProvider::class);

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }

    /**
     * Add the parent setUp function by setting up an in-memory sqlite db
     * and then running all the migrations in this package
     */
    public function setUp()
    {
        parent::setUp();

        // Database setup
        $this->app['config']->set('database.default', 'sqlite');
        $this->app['config']->set('database.connections.sqlite.database', ':memory:');
        $this->migrate();

        // Sets up Laravel Model factories for file in given path. Usage:
        // $this->factory->of([CLASS])->times(1)->make() or ->create(); etc
        $this->factory = Factory::construct(
            FakerFactory::create(),
            __DIR__ . '/../src/database/factories'
        );
    }

    /**
     * run package database migrations
     *
     * @return void
     */
    public function migrate()
    {
        $fileSystem = new Filesystem;
        $classFinder = new ClassFinder;

        foreach ($fileSystem->files(__DIR__ . '/../src/database/migrations') as $file) {
            $path = $file->getRealPath();
            $fileSystem->requireOnce($path);
            $migrationClass = $classFinder->findClass($path);

            (new $migrationClass)->up();
        }
    }
}
