<?php

use Yadda\Enso\Users\Models\Permission;
use Yadda\Enso\Users\Models\Role;

use Yadda\Enso\Users\Tests\TestCase;

class HasPermissionsTraitTest extends TestCase
{
    public function test_expected_failures()
    {
        $role = $this->factory->of(Role::class)->times(1)->create();

        // Check expected exception on bad input
        $this->setExpectedException('ErrorException');
        $role->addPermissions((object) ['invalid' => 'Invalid']);
    }

    /**
     * Tests that an instance of HasPermission can add permissions to it's relations list by
     * either instance of Permission, permission id or permission slug.
     *
     * @return void
     */
    public function test_a_user_can_add_a_single_permission()
    {
        $this->assertEquals(0, Role::count());

        $role = $this->factory->of(Role::class)->times(1)->create();
        $this->assertEquals($role->permissions()->count(), 0);

        // Add Permission by instance of Permission
        $permission = $this->factory->of(Permission::class)->times(1)->create();
        $role->addPermission($permission);
        $this->assertEquals($role->permissions->count(), 1);

        // Add permission by permission id
        $permission = $this->factory->of(Permission::class)->times(1)->create();
        $role->addPermission($permission->id);
        $this->assertEquals($role->permissions->count(), 2);

        // Add permission by permission slug
        $permission = $this->factory->of(Permission::class)->times(1)->create();
        $role->addPermission($permission->slug);
        $this->assertEquals($role->permissions->count(), 3);

        // Test that adding a duplicate does not fail nor add a new item
        $role->addPermissions($permission);
        $this->assertEquals($role->permissions->count(), 3);

        // Check expected exception on bad input
        $this->setExpectedException('ErrorException');
        $role->addPermissions((object) ['invalid' => 'Invalid']);
    }

    /**
     * Tests that an instance of HasPermission can add multiple permissions to it's relations,
     * by Collection or Permission instances or an array of either instances of Permission,
     * permission ids, permissions slugs, or a mixture of these.
     *
     * @return void
     */
    public function test_a_user_can_add_many_permissions()
    {
        $this->assertEquals(0, Role::count());

        $role = $this->factory->of(Role::class)->times(1)->create();
        $this->assertEquals($role->permissions->count(), 0);

        // Add Permissions by Collection
        $permissions_collection = $this->factory->of(Permission::class)->times(2)->create();
        $role->addPermissions($permissions_collection);
        $this->assertEquals($role->permissions->count(), 2);

        // Add permissions by array of Permission instances
        $permission_1 = $this->factory->of(Permission::class)->times(1)->create();
        $permission_2 = $this->factory->of(Permission::class)->times(1)->create();
        $role->addPermissions([$permission_1, $permission_2]);
        $this->assertEquals($role->permissions->count(), 4);

        // Add permissions by array of Permission ids
        $permission_1 = $this->factory->of(Permission::class)->times(1)->create();
        $permission_2 = $this->factory->of(Permission::class)->times(1)->create();
        $role->addPermissions([$permission_1->id, $permission_2->id]);
        $this->assertEquals($role->permissions->count(), 6);

        // Add permissions by array of permission slugs
        $permission_1 = $this->factory->of(Permission::class)->times(1)->create();
        $permission_2 = $this->factory->of(Permission::class)->times(1)->create();
        $role->addPermissions([$permission_1->slug, $permission_2->slug]);
        $this->assertEquals($role->permissions->count(), 8);

        // Add permissions by array of mixed types
        $permission_1 = $this->factory->of(Permission::class)->times(1)->create();
        $permission_2 = $this->factory->of(Permission::class)->times(1)->create();
        $permission_3 = $this->factory->of(Permission::class)->times(1)->create();
        $role->addPermissions([$permission_1, $permission_2->id, $permission_3->slug]);
        $this->assertEquals($role->permissions->count(), 11);

        // Defer to addPermission when single provided
        $permission_1 = $this->factory->of(Permission::class)->times(1)->create();
        $role->addPermissions($permission_1);
        $this->assertEquals($role->permissions->count(), 12);

        // Test that adding a duplicate does not fail nor add a new item
        $permission_2 = $this->factory->of(Permission::class)->times(1)->create();
        $role->addPermissions([$permission_1, $permission_2]);
        $this->assertEquals($role->permissions->count(), 13);
    }

    /**
     * Tests that a user has a given permission
     *
     * @return void
     */
    public function test_a_user_has_a_given_permission()
    {
        $role = $this->factory->of(Role::class)->times(1)->create();
        $permission = $this->factory->of(Permission::class)->times(1)->create();

        // Check use doesn't have permission by various methods. First entry passes
        // true to force refresh the relationship
        $this->assertFalse($role->hasPermission($permission));
        $this->assertFalse($role->hasPermission($permission->id));
        $this->assertFalse($role->hasPermission($permission->slug));

        // Add permission to user
        $role->addPermissions($permission);

        // Add permission and check user does have permission by various methods. First entry
        // passes true to force refresh the relationship

        $this->assertTrue($role->hasPermission($permission));
        $this->assertTrue($role->hasPermission($permission->id));
        $this->assertTrue($role->hasPermission($permission->slug));
    }

    /**
     * Tests that a user belongs to any one of multiple permissions
     *
     * @return void
     */
    public function test_a_user_has_one_of_an_array_of_permissions()
    {
        $role = $this->factory->of(Role::class)->times(1)->create();
        $permissions = $this->factory->of(Permission::class)->times(6)->create();

        // Adds last 3 permissions, then refreshes the user instance
        $role->addPermissions([$permissions[3], $permissions[4], $permissions[5]]);

        // Test one of many from instance
        $this->assertFalse($role->hasPermissions([$permissions[0], $permissions[1]]));
        $this->assertTrue($role->hasPermissions([$permissions[0], $permissions[4]]));

        // Test one of many from id
        $this->assertFalse($role->hasPermissions([$permissions[1]->id, $permissions[2]->id]));
        $this->assertTrue($role->hasPermissions([$permissions[1]->id, $permissions[5]->id]));

        // Test one of many from slug
        $this->assertFalse($role->hasPermissions([$permissions[0]->slug, $permissions[2]->slug]));
        $this->assertTrue($role->hasPermissions([$permissions[0]->slug, $permissions[5]->slug]));

        // Test one of many from mixed
        $this->assertFalse($role->hasPermissions([$permissions[0], $permissions[1]->id, $permissions[2]->slug]));
        $this->assertTrue($role->hasPermissions([$permissions[0], $permissions[1]->id, $permissions[5]->slug]));

        // Test one of many with Single input
        $this->assertFalse($role->hasPermissions($permissions[0]));
        $this->assertTrue($role->hasPermissions($permissions[3]));
    }

    public function test_a_user_has_all_of_an_array_of_permissions()
    {
        $role = $this->factory->of(Role::class)->times(1)->create();
        $permissions = $this->factory->of(Permission::class)->times(6)->create();

        // Adds last 3 permissions, then refreshes the user instance
        $role->addPermissions([$permissions[3], $permissions[4], $permissions[5]]);

        // Test all of from instance
        $this->assertFalse($role->hasAllPermissions([$permissions[0], $permissions[4]]));
        $this->assertTrue($role->hasAllPermissions([$permissions[3], $permissions[4]]));

        // Test all of from id
        $this->assertFalse($role->hasAllPermissions([$permissions[1]->id, $permissions[4]->id]));
        $this->assertTrue($role->hasAllPermissions([$permissions[3]->id, $permissions[4]->id]));

        // Test all of from slug
        $this->assertFalse($role->hasAllPermissions([$permissions[2]->slug, $permissions[5]->slug]));
        $this->assertTrue($role->hasAllPermissions([$permissions[3]->slug, $permissions[5]->slug]));

        // Test all of from mixed
        $this->assertFalse($role->hasAllPermissions([$permissions[1], $permissions[2]->id, $permissions[5]->slug]));
        $this->assertTrue($role->hasAllPermissions([$permissions[3], $permissions[4]->id, $permissions[5]->slug]));

        // Test all of with Single input
        $this->assertFalse($role->hasAllPermissions($permissions[0]));
        $this->assertTrue($role->hasAllPermissions($permissions[3]));
    }

    /**
     * Test that a single permission can be removed from an instance of HasPermission
     * @return [type] [description]
     */
    public function test_a_user_can_remove_a_single_permission()
    {
        $role = $this->factory->of(Role::class)->times(1)->create();
        $permissions = $this->factory->of(Permission::class)->times(4)->create();

        // Add 4 permissions, then refreshes the user instance
        $role->addPermissions($permissions);

        // Test remove by Permission instance
        $role->removePermission($permissions[0]);
        $this->assertEquals($role->permissions()->count(), 3);

        // Test remove by Permission instance
        $role->removePermission($permissions[1]->id);
        $this->assertEquals($role->permissions()->count(), 2);

        // Test remove by Permission instance
        $role->removePermission($permissions[2]->slug);
        $this->assertEquals($role->permissions()->count(), 1);

        // Test that passing a permission that the user doesn't have makes no changes
        $role->removePermission($permissions[0]);
        $this->assertEquals($role->permissions()->count(), 1);
    }

    /**
     * Test removal of permissions in bulk
     * @return [type] [description]
     */
    public function test_a_user_can_remove_many_permissions()
    {
        $role = $this->factory->of(Role::class)->times(1)->create();
        $permissions = $this->factory->of(Permission::class)->times(14)->create();

        $role->addPermissions($permissions);

        // Add Permissions by Collection
        $permissions_collection = collect([$permissions[0], $permissions[1]]);
        $role->removePermissions($permissions_collection);
        $this->assertEquals($role->permissions()->count(), 12);

        // Add permissions by array of Permission instances
        $role->removePermissions([$permissions[2], $permissions[3]]);
        $this->assertEquals($role->permissions()->count(), 10);

        // Add permissions by array of Permission ids
        $role->removePermissions([$permissions[4]->id, $permissions[5]->id]);
        $this->assertEquals($role->permissions()->count(), 8);

        // Add permissions by array of permission slugs
        $role->removePermissions([$permissions[6]->slug, $permissions[7]->slug]);
        $this->assertEquals($role->permissions()->count(), 6);

        // Add permissions by array of mixed types
        $role->removePermissions([$permissions[8], $permissions[9]->id, $permissions[10]->slug]);
        $this->assertEquals($role->permissions()->count(), 3);

        // Defer to addPermission when single provide
        $role->removePermissions($permissions[11]);
        $this->assertEquals($role->permissions()->count(), 2);

        // Test that adding a duplicate does not fail nor add a new item
        $role->removePermissions([$permissions[0], $permissions[12]]);
        $this->assertEquals($role->permissions()->count(), 1);
    }
}
