<?php

namespace Yadda\Enso\Users\Repositories;

use Illuminate\Support\Collection;

use Yadda\Enso\Users\Contracts\PermissionRepositoryContract;

class PermissionRepository implements PermissionRepositoryContract
{

    protected $permission_list;

    protected $permission_class;

    public function __construct(Collection $permission_list = null)
    {
        $this->permission_class = config('enso.users.classes.permission');

        $permission_list = $permission_list ?: new Collection();
        $this->setPermissionList($permission_list);
    }

    /**
     * Set the current Permission list
     *
     * @return \Illuminate\Support\Collection Permission list to set
     */
    public function getPermissionList()
    {
        return $this->permission_list;
    }

    /**
     * Set the current Permission list
     *
     * @param \Illuminate\Support\Collection $permission_list Permission list to set
     *
     * @return PermissionRepository self
     */
    public function setPermissionList(Collection $permission_list)
    {
        $this->permission_list = $permission_list;

        return $this;
    }

    /**
     * Takes a mixed identifier and works out an id from it (or returns a slug
     * if not specified to lookup by slug)
     *
     * @param mixed $identifier  Identifier to check
     * @param bool  $lookup_slug Whether to lookup models by slug and return id instead
     * @param bool  $lookup_id   Whether to lookup models by id to check for existence
     *
     * @return mixed Id or (string) case identifier it not looking up slugs
     */
    protected function findIdentifierFromMixedSingle($identifier, $lookup_slug = false, $lookup_id = false)
    {
        // Shouldn't need to do a lookup, the fact that is an instance of permission
        // (with an id) mean it 'should' already exist.
        if ($identifier instanceof $this->permission_class) {
            return $identifier->id;
        }

        // Otherwise, return an id (potentially running a lookup to pre-check)
        if (is_numeric($identifier)) {
            if ($lookup_id) {
                return is_null($permission = $this->permission_class::find($identifier)) ?
                null :
                $permission->id;
            } else {
                return $identifier;
            }
        }

        // Optionally, lookup Permission by slug and return it's id.
        if ($lookup_slug) {
            return is_null($permission = $this->permission_class::where('slug', $identifier)->first()) ?
                null :
                $permission->id;
        }

        // Otherwise return as string. If a totally invalid argument it passed,
        // this should be expected to throw an exception.
        return (string) $identifier;
    }

    /**
     * Accepts a mixed set of data, and parses it to find the return a set array
     * of ids of all the matching Permission items.
     *
     * Accepted types: Collection, Permission instance, permission id, permission slug or an array
     * of any of the singular forms.
     *
     * @param mixed $identifiers data to work from
     *
     * @return array ids of matching Permissions
     */
    public function findIdsFromMixed($identifiers)
    {
        if ($identifiers instanceof Collection) {
            return $identifiers->pluck('id')->all();
        }

        if (!is_array($identifiers)) {
            return [$this->findIdentifierFromMixedSingle($identifiers, true)];
        }

        // Gets all ids and slugs from the provided argumet
        $id_array = $slug_array = [];
        foreach ($identifiers as $permission_argument) {
            $identifier = $this->findIdentifierFromMixedSingle($permission_argument);

            // Occurs when instance of Permission is passed but doesn't have an id.
            if (is_null($identifier)) {
                continue;
            }

            // Otherwise, place in correct data array.
            if (is_numeric($identifier)) {
                $id_array[] = $identifier;
            } else {
                $slug_array[] = $identifier;
            }
        }

        // Performs a query to get id's based on any slugs passed, them merges
        // with current list of ids from arguments.
        if (!empty($slug_array)) {
            $permissions_from_slugs = $this->permission_class::whereIn('slug', $slug_array)->get();
            if ($permissions_from_slugs->count()) {
                $id_array = array_unique(array_merge($id_array, $permissions_from_slugs->pluck('id')->all()));
            }
        }

        return $id_array;
    }

    /**
     * Checks to see whether any of a given, mixed list of permissions or identifiers
     * are present within a set of Permission.
     *
     * @param mixed $permissions List of permissions to look for
     * @param bool  $all         Whether to match all or just one
     *
     * @return bool Success status
     */
    public function permissionListContainsPermissions($permissions, $all = false)
    {
        $id_array = $this->findIdsFromMixed($permissions);

        $match_statuses = [];
        foreach ($id_array as $permission) {
            $match_statuses[] = $this->permission_list->contains($permission);
        }

        // If no valid permissions to match, then it implicity fails
        if (empty($match_statuses)) {
            return false;
        }

        // Shrink the array to just success and fail
        $statuses = array_unique($match_statuses);

        return $all ?
            $all && ! in_array(false, $statuses) : // if $all is set, then any fails should return false
            $all || in_array(true, $statuses); // if $all is not set, then any success should return true
    }
}
