<?php

namespace Yadda\Enso\Users\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Yadda\Enso\Users\Contracts\Role as RoleContract;

class RoleSeeder extends Seeder
{
    public function run()
    {
        $role_class = get_class(resolve(RoleContract::class));

        $superuser = $role_class::create([
            'name'        => 'Super User',
            'slug'        => 'superuser',
            'description' => 'Highest level admin. Access all areas.',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now(),
            'left_id'     => 1,
            'right_id'    => 4,
        ]);

        $admin = $role_class::create([
            'name'        => 'Admin',
            'slug'        => 'admin',
            'description' => 'Site administrators.',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now(),
            'parent_id'   => $superuser->id,
            'left_id'     => 2,
            'right_id'    => 3,
        ]);
    }
}
