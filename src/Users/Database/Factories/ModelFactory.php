<?php

use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Yadda\Enso\Users\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'username' => $faker->unique()->name,
        'email' => $faker->unique()->safeEmail,
        'display_name' => $faker->name,
        'real_name' => $faker->name,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => Str::random(10),
    ];
});

$factory->define(Yadda\Enso\Users\Models\Role::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'slug' => $faker->unique()->slug,
        'description' => $faker->sentence,
        'parent_id' => null,
        'left_id' => 0,
        'right_id' => 0,
        'depth' => 0
    ];
});

$factory->define(Yadda\Enso\Users\Models\Permission::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'slug' => $faker->unique()->slug,
        'description' => $faker->sentence
    ];
});
