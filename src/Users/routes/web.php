<?php

Route::group(['middleware' => ['web', 'enso']], function () {
    Route::get('admin/users/{user}/login-as')
        ->uses(EnsoCrud::controllerClass('user', true) . '@loginAsUser')
        ->name('admin.users.login-as-user');

    EnsoCrud::crudRoutes('admin/users', 'user', 'admin.users');
    EnsoCrud::crudRoutes('admin/roles', 'role', 'admin.roles');
});
