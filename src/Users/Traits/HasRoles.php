<?php

namespace Yadda\Enso\Users\Traits;

use App;
use Yadda\Enso\Users\Contracts\Role as RoleContract;
use Yadda\Enso\Users\Contracts\RoleRepositoryContract;

/**
 * Provides all the required functionality for an Eloquent model to have a
 * roles relationship.
 *
 * Designed to fulfil the Yadda\Enso\Users\Interfaces\HasRoles interface.
 */
trait HasRoles
{
    protected $role_repository;

    /**
     * Gets the table name for this relationship. It checks the model on which
     * this trait is being used for a definition first, then falls back to what
     * eloquent would default to.
     *
     * @return string Table name for relationship
     */
    protected function getRolePivotTableName()
    {
        $role_class = get_class(App::make(RoleContract::class));
        if (property_exists($this, 'roles_pivot_table_name')) {
            return $this->roles_pivot_table_name;
        } else {
            return $this->joiningTable($role_class);
        }
    }

    /**
     * Gets an instance of the Role Repository to use
     *
     * @return \Yadda\Enso\Users\Contracts\RoleRepositoryContract
     */
    public function getRoleRepository()
    {
        if (empty($this->role_repository)) {
            $this->role_repository = App::make(RoleRepositoryContract::class)->setRoleList($this->roles);
        }

        return $this->role_repository;
    }

    /**
     * Defines the Roles relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function roles()
    {
        $role_class = get_class(App::make(RoleContract::class));
        return $this->belongsToMany($role_class, $this->getRolePivotTableName())->withTimestamps();
    }

    /**
     * Adds a given role to the current implementation of HasRoles.
     *
     * Uses the sync command on a relationship with the second arguments set to
     * false. This means that it adds it if it doesn't already exists, but
     * doesn't delete other records while doing so (which would happen if false
     * was left off).
     *
     * @param mixed $role          Role instance, role id or role slug
     * @param bool  $refresh_roles Role instance, role id or role slug
     *
     * @return \Yadda\Enso\Users\Interfaces\HasRoles self
     */
    public function addRole($role, $refresh_roles = true)
    {
        $id_array = $this->getRoleRepository()->findIdsFromMixed($role, true);

        if (!empty($id_array)) {
            $this->roles()->sync($id_array, false);
        }

        if ($refresh_roles) {
            $this->load('roles');
            $this->getRoleRepository()->setRoleList($this->roles);
        }

        return $this;
    }

    /**
     * Adds given roles to the current implementation of HasRoles. $roles may be
     * either a Collection of Roles or an mixed array of Role instances, Role ids
     * or Role slugs.
     *
     * Note: Defers to addRole if the argument is neither a Collection
     * nor an array.
     *
     * @param mixed   $roles         Collection or array of roles to add.
     * @param boolean $refresh_roles Role instance, role id or role slug
     *
     * @return \Yadda\Enso\Users\Interfaces\HasRoles self
     */
    public function addRoles($roles, $refresh_roles = true)
    {
        $id_array = $this->getRoleRepository()->findIdsFromMixed($roles);

        if (!empty($id_array)) {
            $this->roles()->sync($id_array, false);
        }

        if ($refresh_roles) {
            $this->load('roles');
            $this->getRoleRepository()->setRoleList($this->roles);
        }

        return $this;
    }

    /**
     * Test that the current HasRoles instance has a given role.
     *
     * @param mixed $role    Role instance, Role id, role slug
     * @param bool  $refresh Whether to force the model to reload it's relationship
     *
     * @return bool Whether HasRoles has Role
     */
    public function hasRole($role, $refresh = false)
    {
        if (!$this->relationLoaded('roles') || $refresh) {
            $this->load('roles');
            $this->getRoleRepository()->setRoleList($this->roles);
        }

        return $this->getRoleRepository()->roleListContainsRoles($role);
    }

    /**
     * Tests that the current HasRoles instance has the given roles. By default,
     * it will return true if it has at least ONE of the given list. Setting $all
     * will require that it has all of them before returning true.
     *
     * @param mixed $roles   Roles, role ids, or role slugs to test against
     * @param bool  $all     Whether all given items must match
     * @param bool  $refresh Whether to force the model to reload it's relationship
     *
     * @return bool Successfull match status
     */
    public function hasRoles($roles, $all = false, $refresh = false)
    {
        if (!$this->relationLoaded('roles') || $refresh) {
            $this->load('roles');
            $this->getRoleRepository()->setRoleList($this->roles);
        }

        return $this->getRoleRepository()->roleListContainsRoles($roles, $all);
    }

    /**
     * Intuitive deferrer for hasRoles($roles, true, $refresh)
     *
     * @param mixed $roles   Roles, role ids, or role slugs to test against
     * @param bool  $refresh Whether to force the model to reload it's relationship
     *
     * @return bool Successfully match status
     */
    public function hasAllRoles($roles, $refresh = false)
    {
        return $this->hasRoles($roles, true, $refresh);
    }

    /**
     * Removes a single role from the current HasRoles instance
     *
     * @param mixed $role          Role, role id, role slug to remove
     * @param bool  $refresh_roles Role instance, role id or role slug
     *
     * @return \Yadda\Enso\Users\Interfaces\HasRoles self
     */
    public function removeRole($role, $refresh_roles = true)
    {
        $identifier = $this->getRoleRepository()->findIdsFromMixed($role, true);

        if (!is_null($identifier)) {
            $this->roles()->detach($identifier);
        }

        if ($refresh_roles) {
            $this->load('roles');
            $this->getRoleRepository()->setRoleList($this->roles);
        }

        return $this;
    }

    /**
     * Removes a number of given roles from the current HasUser instance. Roles
     * can be either a Collection or a mixed array of Role instances, role ids
     * and roles slugs.
     *
     * @param mixed   $roles         Roles to remove
     * @param boolean $refresh_roles Role instance, role id or role slug
     *
     * @return \Yadda\Enso\Users\Interfaces\HasRoles self
     */
    public function removeRoles($roles, $refresh_roles = true)
    {
        $id_array = $this->getRoleRepository()->findIdsFromMixed($roles);

        if (!empty($id_array)) {
            $this->roles()->detach($id_array);
        }

        if ($refresh_roles) {
            $this->load('roles');
            $this->getRoleRepository()->setRoleList($this->roles);
        }

        return $this;
    }

    /**
     * Checks whether any of the HasRoles instance's roles are a superuser
     *
     * @return bool Whether HasRoles has superuser role
     */
    public function isSuperuser()
    {
        return $this->getRoleRepository()->roleListContainsSuperuser();
    }

    public function scopeSuperusers($query)
    {
        return $query->whereHas('roles', function ($relation) {
            return $relation->isSuperuser();
        });
    }
}
