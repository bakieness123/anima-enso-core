<?php

namespace Yadda\Enso\Users\Interfaces;

/**
 * Add to eloquent models to provide all the requirements for a permissions relationship
 */
interface HasPermissions
{
    /**
     * Defines the Permissions relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function permissions();

    /**
     * Adds a given permission to the current implementation of HasPermissions.
     *
     * @param mixed $permission          Permission instance, permission id or permission slug
     * @param bool  $refresh_permissions Permission instance, permission id or permission slug
     *
     * @return \Yadda\Enso\Users\Interfaces\HasPermissions self
     */
    public function addPermission($permission, $refresh_permissions = true);

    /**
     * Adds given permissions to the current implementation of HasPermissions.
     *
     * @param mixed   $permissions         Collection or array of permissions to add.
     * @param boolean $refresh_permissions Permission instance, permission id or permission slug
     *
     * @return \Yadda\Enso\Users\Interfaces\HasPermissions self
     */
    public function addPermissions($permissions, $refresh_permissions = true);

    /**
     * Test that the current HasPermissions instance has a given permission.
     *
     * @param mixed $permission  Permission instance, Permission id, permission slug
     * @param bool  $refresh     Whether to force the model to reload it's relationship
     *
     * @return bool whether HasPermissions has Permission
     */
    public function hasPermission($permission, $refresh = false);

    /**
     * Tests that the current HasPermissions instance has the given permissions. By default,
     * it will return true if it has at least one of the given list. Setting $all
     * will require that it has all of them before returning true.
     *
     * @param mixed $permissions Permissions, permission ids, or permission slugs to test against
     * @param bool  $all         Whether all given items must match
     * @param bool  $refresh     Whether to force the model to reload it's relationship
     *
     * @return bool Successfully match status
     */
    public function hasPermissions($permissions, $all = false, $refresh = false);

    /**
     * Intuitive deferrer for hasPermissions($permissions, true, $refresh)
     *
     * @param mixed $permissions Permissions, permission ids, or permission slugs to test against
     * @param bool  $refresh     Whether to force the model to reload it's relationship
     *
     * @return bool Successfully match status
     */
    public function hasAllPermissions($permissions, $refresh = false);

    /**
     * Removes a single permission from the current HasPermissions instance
     *
     * @param mixed   $permission          Permission, permission id, permission slug to remove
     * @param boolean $refresh_permissions Permission instance, permission id or permission slug
     *
     * @return \Yadda\Enso\Users\Interfaces\HasPermissions self
     */
    public function removePermission($permission, $refresh_permissions = true);

    /**
     * Removes a number of given permissions from the current HasUser instance. Permissions
     * can be either a Collection or a mixed array of Permission instances, permission ids
     * and permissions slugs.
     *
     * @param mixed   $permissions         Permissions to remove
     * @param boolean $refresh_permissions Permission instance, permission id or permission slug
     *
     * @return \Yadda\Enso\Users\Interfaces\HasPermissions self
     */
    public function removePermissions($permissions, $refresh_permissions = true);
}
