<?php

namespace Yadda\Enso\Users\Interfaces;

/**
 * Add to eloquent models to provide all the requirements for a roles relationship
 */
interface HasRoles
{

    /**
     * Defines the Roles relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function roles();

    /**
     * Adds a given role to the current implementation of HasRoles.
     *
     * @param mixed $role          Role instance, role id or role slug
     * @param bool  $refresh_roles Role instance, role id or role slug
     *
     * @return \Yadda\Enso\Users\Interfaces\HasRoles self
     */
    public function addRole($role, $refresh_roles = true);

    /**
     * Adds given roles to the current implementation of HasRoles.
     *
     * @param mixed $roles         Collection or array of roles to add.
     * @param bool  $refresh_roles Role instance, role id or role slug
     *
     * @return \Yadda\Enso\Users\Interfaces\HasRoles self
     */
    public function addRoles($roles, $refresh_roles = true);

    /**
     * Test that the current HasRoles instance has a given role.
     *
     * @param mixed $role    Role instance, Role id, role slug
     * @param bool  $refresh Whether to force the model to reload it's relationship
     *
     * @return bool whether HasRoles has Role
     */
    public function hasRole($role, $refresh = false);

    /**
     * Tests that the current HasRoles instance has the given roles. By default,
     * it will return true if it has at least one of the given list. Setting $all
     * will require that it has all of them before returning true.
     *
     * @param mixed $roles      Roles, role ids, or role slugs to test against
     * @param bool  $all        Whether all given items must match
     * @param bool  $refresh    Whether to force the model to reload it's relationship
     *
     * @return bool Successfully match status
     */
    public function hasRoles($roles, $all = false, $refresh = false);

    /**
     * Intuitive deferrer for hasRoles($roles, true, $refresh)
     *
     * @param mixed $roles   Roles, role ids, or role slugs to test against
     * @param bool  $refresh Whether to force the model to reload it's relationship
     *
     * @return bool Successfully match status
     */
    public function hasAllRoles($roles, $refresh = false);

    /**
     * Removes a single role from the current HasRoles instance
     *
     * @param mixed $role          Role, role id, role slug to remove
     * @param bool  $refresh_roles Role instance, role id or role slug
     *
     * @return \Yadda\Enso\Users\Interfaces\HasRoles self
     */
    public function removeRole($role, $refresh_roles = true);

    /**
     * Removes a number of given roles from the current HasUser instance. Roles
     * can be either a Collection or a mixed array of Role instances, role ids
     * and roles slugs.
     *
     * @param mixed $roles         Roles to remove
     * @param bool  $refresh_roles Role instance, role id or role slug
     *
     * @return \Yadda\Enso\Users\Interfaces\HasRoles self
     */
    public function removeRoles($roles, $refresh_roles = true);

    /**
     * Checks whether any of the HasRoles instance's roles are a superuser
     *
     * @return bool Whether HasRoles has superuser role
     */
    public function isSuperuser();
}
