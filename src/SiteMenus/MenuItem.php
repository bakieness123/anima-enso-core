<?php

namespace Yadda\Enso\SiteMenus;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Yadda\Enso\Crud\Contracts\IsCrudModel as ContractsIsCrudModel;
use Yadda\Enso\Crud\Traits\IsCrudModel;
use Yadda\Enso\SiteMenus\Contracts\Menu;
use Yadda\Enso\SiteMenus\Contracts\MenuItem as MenuItemContract;

class MenuItem extends Model implements MenuItemContract, ContractsIsCrudModel
{
    use IsCrudModel;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'enso_site_menu_items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'site_menu_id',
        'label',
        'url',
        'target',
        'order',
    ];

    /**
     * The value to use at this Model's CRUD label. Name and title are most
     * frequently used, falling back to ID as it's always going to be available.
     *
     * @return string
     */
    public function getCrudLabel(): string
    {
        return $this->label;
    }

    /**
     * Accessor method for this MenuItem's 'rel' attribute (based on it's target)
     *
     * @return string
     */
    public function getRelAttribute(): string
    {
        return $this->target === '_blank' ? 'noopener noreferrer' : '';
    }

    /**
     * Accessor method for this MenuItem's target, returning as a string instead
     * of an option array element.
     *
     * NOTE: This is out of date. Target is stored as just the string (instead)
     *       of a json_encoded array.
     *
     * @todo - Check to see what is going on here, and if this can be removed
     *
     * @return string
     */
    public function getTargetStrAttribute(): string
    {
        return Arr::get(json_decode($this->target), 'id', '_self');
    }

    /**
     * Menu that this item is in
     *
     * @return BelongsTo
     */
    public function menu(): BelongsTo
    {
        return $this->belongsTo(App::make(Menu::class), 'site_menu_id');
    }
}
