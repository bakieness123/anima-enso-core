<?php

namespace Yadda\Enso\SiteMenus;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\URL;
use Yadda\Enso\SiteMenus\Contracts\Menu;

class SiteMenu
{
    /**
     * Get name for the cache for a given menu's slug
     */
    protected function cacheKey(string $slug): string
    {
        return 'enso-menus-' . $slug;
    }

    /**
     * Determines whether the given URL is present within any site menus
     *
     * @param string $url
     *
     * @return boolean
     */
    public function containsUrl(string $url): bool
    {
        foreach ($this->menuSlugs() as $slug) {
            $menu = $this->get($slug);

            foreach ($menu->items as $menu_item) {
                $menu_url = ((substr($menu_item->url, 0, 1) === '/') && !(substr($menu_item->url, 1, 1) === '/'))
                    ? URL::to($menu_item->url)
                    : $menu_item->url;

                if ($menu_url === $url) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Collection of menu items for the given menu.
     *
     * @return Collection|false
     */
    public function get(string $slug)
    {
        $cache_key = $this->cacheKey($slug);

        return Cache::rememberForever($cache_key, function () use ($slug) {
            $menu = App::make(Menu::class)::with('items')->where('slug', $slug)->first();

            if (!$menu) {
                return false;
            }

            return $menu;
        });
    }

    /**
     * Whether the given menu has any items
     */
    public function hasItems(string $slug): bool
    {
        $menu_cache = $this->get($slug) ?? [];

        return count(Arr::get($menu_cache, 'items', [])) > 0;
    }

    /**
     * Slugs of all menus
     *
     * @return array
     */
    public function menuSlugs(): array
    {
        return Cache::rememberForever('enso-menu-list', function () {
            return App::make(Menu::class)->query()->pluck('slug')->toArray();
        });
    }
}
