<?php

namespace Yadda\Enso\SiteMenus\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TorMorten\Eventy\Facades\Eventy;
use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Controller as BaseController;
use Yadda\Enso\SiteMenus\Contracts\MenuController as ContractsMenuController;

class MenuController extends BaseController implements ContractsMenuController
{
    /**
     * Name of this crud type
     *
     * This will be used to find config/model/controller
     *
     * @var string
     */
    protected $crud_name = 'sitemenu';

    /**
     * Menu slugs for which to prevent slug editing
     *
     * @var array
     */
    protected static $disabled_slugs = [
        'main-navigation',
        'footer',
    ];

    /**
     * Disables editing of the slug field on a Menu if it is listed in the
     * disabled slugs array on this Controller. Superadmin access overrides this
     * restriction.
     *
     * @param Config  $controller
     * @param Request $request
     *
     * @return void
     */
    public static function disableSlugs(Config $config, Request $request)
    {
        $model_instance = $config->getEditForm()->getModelInstance();

        if (is_null($model_instance) || empty($model_instance->slug)) {
            throw new Exception('Cannot add Menu fields to an edit form with no item');
        }

        if (Auth::guest()) {
            throw new Exception('Must be logged in');
        }

        if (in_array($model_instance->slug, static::$disabled_slugs) && !Auth::user()->hasPermission('site-menu-update')) {
            $form = $config->getEditForm();
            $form->getSection('main')->getField('slug')->setDisabled(true);
        }
    }

    /**
     * Configures the hooks and filters for this controller
     *
     * @return void
     */
    protected function configureHooks()
    {
        Eventy::addAction('crud.edit', [static::class, 'disableSlugs'], 20, 2);
        Eventy::addAction('crud.update.before', [static::class, 'disableSlugs'], 10, 2);
    }
}
