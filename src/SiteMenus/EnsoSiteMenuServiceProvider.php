<?php

namespace Yadda\Enso\SiteMenus;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Yadda\Enso\Facades\EnsoMenu;
use Yadda\Enso\SiteMenus\Contracts\Menu as MenuContract;
use Yadda\Enso\SiteMenus\Contracts\MenuController as MenuControllerContract;
use Yadda\Enso\SiteMenus\Contracts\MenuCrud as MenuCrudContract;
use Yadda\Enso\SiteMenus\Contracts\MenuItem as MenuItemContract;
use Yadda\Enso\SiteMenus\Controllers\MenuController;
use Yadda\Enso\SiteMenus\Crud\Menu as MenuCrud;
use Yadda\Enso\SiteMenus\Menu;

/**
 * Service Provider for the Enso user and roles system
 */
class EnsoSiteMenuServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(MenuContract::class, Menu::class);
        $this->app->bind(MenuItemContract::class, MenuItem::class);
        $this->app->bind(MenuCrudContract::class, MenuCrud::class);
        $this->app->bind(MenuControllerContract::class, MenuController::class);
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $installable_dir = __DIR__ . '/../../installable/';

        $this->mergeConfigFrom($installable_dir . 'config/crud/sitemenu.php', 'enso.crud.sitemenu');

        $this->loadMigrationsFrom([__DIR__ . '/Database/Migrations/']);

        $this->loadRoutesFrom(__DIR__ . '/Routes/web.php');

        EnsoMenu::addItem(Config::get('enso.crud.sitemenu.menuitem'));
    }
}
