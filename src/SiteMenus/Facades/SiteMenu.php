<?php

namespace Yadda\Enso\SiteMenus\Facades;

use Illuminate\Support\Facades\Facade;
use Yadda\Enso\SiteMenus\SiteMenu as SiteMenuClass;

class SiteMenu extends Facade
{
    protected static function getFacadeAccessor()
    {
        return SiteMenuClass::class;
    }
}
