<?php

namespace Yadda\Enso;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Yadda\Enso\Console\Commands\DiscoverFiles;
use Yadda\Enso\Facades\EnsoMenu;
use Yadda\Enso\Media\Console\Commands\RegenerateImages;
use Yadda\Enso\Media\Console\Commands\TransferFiles;
use Yadda\Enso\Media\Contracts\AudioFile as AudioFileContract;
use Yadda\Enso\Media\Contracts\ImageFile as ImageFileContract;
use Yadda\Enso\Media\Contracts\MediaFile as MediaFileContract;
use Yadda\Enso\Media\Contracts\UploadsController as UploadsControllerContract;
use Yadda\Enso\Media\Contracts\VideoFile as VideoFileContract;
use Yadda\Enso\Media\Controllers\UploadsController as UploadsControllerConcrete;
use Yadda\Enso\Media\Media as MediaHelper;
use Yadda\Enso\Media\Models\AudioFile as AudioFileConcrete;
use Yadda\Enso\Media\Models\ImageFile as ImageFileConcrete;
use Yadda\Enso\Media\Models\MediaFile as MediaFileConcrete;
use Yadda\Enso\Media\Models\VideoFile as VideoFileConcrete;

class EnsoMediaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $installable_dir = __DIR__ . '/../installable/';
        $resources_dir = __DIR__ . '/../resources/';

        $this->mergeConfigFrom($installable_dir . 'config/crud/file.php', 'enso.crud.file');

        $this->loadRoutesFrom(__DIR__ . '/Media/routes/web.php');

        $this->loadViewsFrom($resources_dir . 'views/media', 'enso-media');

        $this->loadMigrationsFrom([
            __DIR__ . '/Media/database/migrations/',
        ]);

        if ($this->app->runningInConsole()) {
            $this->commands([
                DiscoverFiles::class,
                RegenerateImages::class,
                TransferFiles::class,
            ]);
        }

        EnsoMenu::addItem(Config::get('enso.crud.file.menuitem'));
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        app()->bind(MediaFileContract::class, MediaFileConcrete::class);
        app()->bind(ImageFileContract::class, ImageFileConcrete::class);
        app()->bind(AudioFileContract::class, AudioFileConcrete::class);
        app()->bind(VideoFileContract::class, VideoFileConcrete::class);

        app()->bind(UploadsControllerContract::class, UploadsControllerConcrete::class);

        $this->app->singleton('ensomedia', MediaHelper::class);
    }
}
