<?php

namespace Yadda\Enso\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'enso:install {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates Enso migrations and publish assets';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        set_time_limit(0);

        $force = $this->option('force');

        if (!$force) {
            $this->line('');
            $this->error('==========');
            $this->error(' WARNING! ');
            $this->error('==========');
            $this->line('');
            $this->error('You should only use this command to create a new Ensō website,');
            $this->error('NOT when installing a copy of an existing site.');

            $continue = $this->confirm('Are you sure you want to continue?', true);

            if (!$continue) {
                return;
            }
        }

        $this->info('=================');
        $this->info(' Installing Ensō ');
        $this->info('=================');

        $users_table_exists = Schema::hasTable('users');

        if ($users_table_exists) {
            $continue = $this->confirm('The users table already exists. Continue?');

            if (!$continue) {
                return;
            }
        }

        // Make somewhere to put Enso configuration stuff
        $this->call('make:provider', [
            'name' => 'EnsoServiceProvider',
        ]);

        // Generate Auth scaffolding
        $this->info('Setting up auth...');

        // $this->call('package:discover');
        $this->call('ui', [
            'type' => 'vue',
            '--auth' => true,
            '--no-interaction' => true,
        ]);

        // Publish all package assets.
        // We use force because we'll be overwriting default Laravel files
        $this->call('vendor:publish', [
            '--force' => true,
            '--all' => true,
        ]);

        // @todo - Do we really need this as well as the above code?
        $this->call('vendor:publish', [
            '--provider' => 'Mews\Purifier\PurifierServiceProvider',
            '--force' => true,
        ]);

        // We'll be using our own migrations, so let's get rid of the defaults
        $this->info('Removing default migrations...');

        $process = Process::fromShellCommandline('rm database/migrations/*');

        try {
            $process->mustRun();

            $this->info($process->getOutput());
        } catch (ProcessFailedException $exception) {
            $this->error('There was an error removing default migrations');
            $this->info($exception->getMessage());
        }

        $this->info('Clearing config...');
        $this->call('config:clear');

        // Make some tables
        $this->info('Running migrations...');
        $this->call('migrate');

        // ...and put some stuff in them
        $this->info('Seeding roles...');
        $this->call('db:seed', [
            '--class' => 'Yadda\\Enso\\Users\\Database\\Seeders\\RoleSeeder',
        ]);

        $this->info('Seeding users...');
        $this->call('db:seed', [
            '--class' => 'Yadda\\Enso\\Users\\Database\\Seeders\\UserSeeder',
        ]);

        $this->info('Seeding pages...');
        $this->call('db:seed', [
            '--class' => 'Yadda\\Enso\\Pages\\Database\\Seeders\\PageSeeder',
        ]);

        $this->info('Seeding menus...');
        $this->call('db:seed', [
            '--class' => 'Yadda\\Enso\\SiteMenus\\Database\\Seeders\\EnsoMenuSeeder',
        ]);

        $this->info("All done!\n");

        $this->info('A default user has been created with the following details:');
        $this->info('You should change the password ASAP!');
        $this->line('  Email:    studio@maya.agency');
        $this->line('  Password: changeme');

        // Update Yarn dependencies
        $this->info('Install Yarn dependencies:');
        $this->line('  yarn remove axios');
        $this->line('  yarn add laravel-mix@^6.0.25 tailwindcss imagemin-webpack-plugin svg-spritemap-webpack-plugin enso-carousel --dev');
    }
}
