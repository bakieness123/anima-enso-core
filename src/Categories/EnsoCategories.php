<?php

namespace Yadda\Enso\Categories;

use Illuminate\Support\Facades\Route;

class EnsoCategories
{
    /**
     * Create frontend JSON routes for categories
     *
     * @param string $path
     * @param string $class
     * @param string $name
     * @return void
     */
    public function routes($path, $class, $name)
    {
        Route::group(['middleware' => 'web'], function () use ($path, $class, $name) {
            Route::get($path, $class . '@index')->name($name . '.index');
            Route::get($path . '/{category}', $class . '@show')->name($name . '.show');
        });
    }
}
