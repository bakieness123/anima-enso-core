<?php

namespace Yadda\Enso\Categories\Controllers;

use Yadda\Enso\Categories\Models\Category;

class CategoryController extends TaxonomyController
{
    public function modelClass()
    {
        return Category::class;
    }

    public function taxonomyName()
    {
        return 'category';
    }
}
