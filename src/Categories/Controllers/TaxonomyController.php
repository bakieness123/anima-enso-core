<?php

namespace Yadda\Enso\Categories\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

abstract class TaxonomyController
{
    abstract public function modelClass();

    abstract public function taxonomyName();

    protected function childrenName()
    {
        return 'children';
    }

    public function index(Request $request)
    {
        $query = $this->modelClass()::query()->whereNull('parent_id');

        if ($request->input('include-children')) {
            $query->with($this->childrenName());
        }

        $items = $query->paginate();

        return response()->json($items);
    }

    public function show(Request $request, $slug)
    {
        $query = $this->modelClass()::where('slug', $slug);

        if ($request->input('include-children')) {
            $query->with($this->childrenName());
        }

        $item = $query->firstOrFail();

        return response()->json([
            $this->taxonomyName() => $item
        ]);
    }

    /**
     * Recursively convert an item and its children to a nested array
     */
    protected function mapItems(Model $item): array
    {
        $result = $this->toArray($item);

        $result[$this->childrenName()] = $item->{$this->childrenName}->map(function ($child) {
            return $this->toArray($child);
        });

        return $result;
    }

    /**
     * Convert an item to an array
     */
    protected function toArray(Model $item): array
    {
        return $item->toArray();
    }
}
