<?php

Route::group(['middleware' => ['web', 'enso']], function () {
    EnsoCrud::crudRoutes('admin/categories', 'category', 'admin.categories');
});
