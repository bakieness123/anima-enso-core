<?php

namespace Yadda\Enso\Pages\Crud;

use Illuminate\Validation\Rule;
use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Contracts\Config\IsPublishable as ConfigIsPublishable;
use Yadda\Enso\Crud\Forms\Fields\BelongsToField;
use Yadda\Enso\Crud\Forms\Fields\DateTimeField;
use Yadda\Enso\Crud\Forms\Fields\FileUploadFieldResumable;
use Yadda\Enso\Crud\Forms\Fields\FlexibleContentField;
use Yadda\Enso\Crud\Forms\Fields\SelectField;
use Yadda\Enso\Crud\Forms\Fields\SlugField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Crud\Tables\Text;
use Yadda\Enso\Crud\Traits\Config\HasProtectedRecords;
use Yadda\Enso\Crud\Traits\Config\HasTemplates;
use Yadda\Enso\Crud\Traits\Config\IsPublishable;
use Yadda\Enso\Crud\Traits\HasDefaultRowSpecs;
use Yadda\Enso\Crud\Traits\ModelHasTemplates;
use Yadda\Enso\Meta\Crud\MetaSection;

class Page extends Config implements ConfigIsPublishable
{
    use HasDefaultRowSpecs,
        HasProtectedRecords,
        HasTemplates,
        IsPublishable;

    /**
     * Defines configuration for this CRUD item
     *
     * @return void
     */
    public function configure()
    {
        $page_model = config('enso.crud.page.model');

        $this->model($page_model)
            ->route('admin.pages')
            ->views('page')
            ->name('Page')
            ->orderable('order')
            ->order('order', 'desc')
            ->searchColumns(['title'])
            ->columns([
                Text::make('title'),
                $this->isPublishablePublishTableCell(),
            ])
            ->setItemFilters([
                'search' => \Yadda\Enso\Crud\Filters\PageFilter::make(),
            ])
            ->rules([
                'main.title' => 'required|string|max:255',
                // Null slugs will cause a new unique slug to be automatically generated
                'main.slug' => ['nullable', 'string', Rule::unique($page_model, 'slug')],
                'main.template' => 'required',
            ]);
    }

    /**
     * Default form configuration.
     *
     * @return \Yadda\Enso\Crud\Forms\Form
     */
    public function create(Form $form)
    {
        $page_model = config('enso.crud.page.model');

        $form->addSections([
            Section::make('main')->addFields([
                TextField::make('title')
                    ->setPurifyHTML(false),
                SlugField::make('slug')
                    ->setRoute(route('pages.show', '%SLUG%'))
                    ->setSource('title'),
                FileUploadFieldResumable::make('thumbnail_id')
                    ->setRelationshipName('thumbnail')
                    ->setUploadPath('pages'),
                DateTimeField::make('publish_at'),
                SelectField::make('template')
                    ->setLabel('Page Template')
                    ->setOptions($this->hasTemplatesList()),
                BelongsToField::make('parentPage')
                    ->setLabel('Parent Page')
                    ->useAjax(route('admin.pages.index'), $page_model),
            ]),
            Section::make('content')->addFields([
                TextField::make('excerpt'),
                FlexibleContentField::make('content')->addRowSpecs(
                    $this->defaultRowSpecs(),
                ),
            ]),
            new MetaSection,
        ]);

        return $form;
    }
}
