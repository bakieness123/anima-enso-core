<?php

namespace Yadda\Enso\Pages\Middleware;

use App\Http\Controllers\HoldingPageController;
use Closure;
use Illuminate\Support\Facades\Auth;
use Yadda\Enso\Settings\Facades\EnsoSettings;

class HoldingPage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!EnsoSettings::get('holding_page_enabled')) {
            return $next($request);
        }

        if (Auth::check() && Auth::user()->hasPermission('bypass-holding-page')) {
            return $next($request);
        }

        return response((new HoldingPageController)->index());
    }
}
