<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique()->nullable();
            $table->integer('order')->unsigned()->default(0)->index();
            $table->integer('thumbnail_id')->unsigned()->nullable()->index();
            $table->boolean('published')->default(false)->index();
            $table->integer('parent_id')->nullable()->index();
            $table->string('title')->nullable();
            $table->string('template')->nullable();
            $table->text('excerpt')->nullable();
            $table->json('content')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
