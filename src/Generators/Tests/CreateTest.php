<?php

namespace Yadda\Enso\Generators\Tests;

use Illuminate\Foundation\Testing\TestResponse;
use Yadda\Enso\Generators\Tests\BaseTest;

/**
 * Provides common functionality for testing the Enso crud create route
 */
abstract class CreateTest extends BaseTest
{
    /**
     * Route name suffix
     *
     * @return string
     */
    protected function getRouteNameSuffix(): string
    {
        return 'create';
    }

    /**
     * Test that an Admin request to the index route
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function runAdminTest(): TestResponse
    {
        $response = $this->makeAdminRequest();

        $response->assertStatus(200);

        return $response;
    }

    /**
     * Test that the index route returns a login redirect response
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function runUnauthenticatedTest(): TestResponse
    {
        $response = $this->makeRequest();

        $response->assertRedirect('login');

        return $response;
    }
}
