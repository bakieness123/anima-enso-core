<?php

namespace Yadda\Enso\Generators\Tests;

use Illuminate\Foundation\Testing\TestResponse;
use Yadda\Enso\Generators\Tests\BaseTest;

/**
 * Provides common functionality for testing the Enso crud update route
 */
abstract class UpdateTest extends BaseTest
{
    /**
     * Route name suffix
     *
     * @return string
     */
    protected function getRouteNameSuffix(): string
    {
        return 'update';
    }

    /**
     * Makes valid request to correct endpoint
     *
     * @param array $parameters
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function makeRequest(array $parameters = []): TestResponse
    {
        return $this->patchJson(
            $this->getRoute([$this->getModelInstance()->getKey()]),
            $this->validModelData($parameters),
            $this->getJsonHeaders()
        );
    }

    /**
     * Test that an Admin request returns a success reponse and updates the
     * model accordingly
     *
     * @param array $updates
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function runAdminTest(array $initial, array $updates): TestResponse
    {
        $this->setModelInstance(factory($this->getModelClass())->create($initial));

        $response = $this->makeAdminRequest($updates);

        $response->assertStatus(200);

        $this->getModelInstance()->refresh();

        foreach ($updates as $property => $value) {
            $this->assertEquals($value, $this->getModelInstance()->$property);
        }

        return $response;
    }

    /**
     * Test that the index route returns a 401 response
     *
     * @param array $updates
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function runUnauthenticatedTest(array $initial, array $updates): TestResponse
    {
        $this->setModelInstance(factory($this->getModelClass())->create($initial));

        $response = $this->makeRequest($updates);

        $response->assertStatus(401);

        $this->getModelInstance()->refresh();

        foreach ($initial as $property => $value) {
            $this->assertEquals($value, $this->getModelInstance()->$property);
        }

        return $response;
    }
}
