<?php

namespace Yadda\Enso\Generators\Tests;

use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Yadda\Enso\Generators\Tests\BaseTest;

/**
 * Provides common functionality for testing the Enso crud route that provides
 * a paginated list for the index.
 */
abstract class IndexJsonTest extends BaseTest
{
    /**
     * Route name suffix
     *
     * @return string
     */
    protected function getRouteNameSuffix(): string
    {
        return 'index';
    }

    /**
     * Makes valid request to correct endpoint
     *
     * @param array $parameters
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function makeRequest(array $parameters = []): TestResponse
    {
        $crud = App::make($this->getCrudClass());

        return $this->getJson(
            $this->getRoute(
                array_merge(
                    [
                        'orderby' => $crud->getOrderBy(),
                        'order' => $crud->getOrder(),
                    ],
                    $parameters
                )
            ),
            $this->getJsonHeaders()
        );
    }

    /**
     * Test that an Admin request to the index route
     *
     * @param array $attributes
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function runAdminTest(array $attributes): TestResponse
    {
        $this->setModelInstance(factory($this->getModelClass())->create($attributes));

        $response = $this->makeAdminRequest();

        $response->assertStatus(200);

        $this->assertCount(1, $response->json());

        foreach ($attributes as $attribute => $value) {
            $this->assertEquals($value, Arr::get($response->json()[0], $attribute));
        }

        return $response;
    }

    /**
     * Test that the index route returns a login redirect response
     *
     * @param array $attributes
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function runUnauthenticatedTest(array $attributes): TestResponse
    {
        $this->setModelInstance(factory($this->getModelClass())->create($attributes));

        $response = $this->makeRequest();

        $response->assertStatus(401);

        return $response;
    }
}
