<?php

namespace Yadda\Enso\Generators\Tests;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\TestCase;
use Tests\Traits\CreatesUsers;

/**
 * Provides common functionality for testing any of the Enso crud routes
 */
abstract class BaseTest extends TestCase
{
    use CreatesUsers;

    /**
     * Instance of model to test with.
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model_instance;

    /**
     * Class for this test's Crud
     *
     * @return string
     */
    abstract protected function getCrudClass(): string;

    /**
     * Class for this test's Model
     *
     * @return string
     */
    abstract protected function getModelClass(): string;

    /**
     * Route name prefix
     *
     * @return string
     */
    abstract protected function getRouteNamePrefix(): string;

    /**
     * Route name suffix
     *
     * @return string
     */
    abstract protected function getRouteNameSuffix(): string;

    /**
     * Headers necessary to denote a json request
     *
     * @return array
     */
    protected function getJsonHeaders(): array
    {
        return [
            'X-Requested-With' => 'XMLHttpRequest',
        ];
    }

    /**
     * Gets an factory-created instance of the model for this test
     *
     * @param bool $persist
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    protected function getModelInstance(bool $persist = false): Model
    {
        if (is_null($this->model_instance)) {
            $this->model_instance = factory($this->getModelClass())->make();

            if ($persist) {
                $this->model_instance->save();
            }
        }

        return $this->model_instance;
    }

    /**
     * Name of the route this test covers
     *
     * @param array $parameters
     *
     * @return string
     */
    protected function getRoute(array $parameters = []): string
    {
        return route(
            $this->getRouteNamePrefix() . '.' . $this->getRouteNameSuffix(),
            $parameters
        );
    }

    /**
     * Makes the request an a admin user.
     *
     * @param array $parameters
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function makeAdminRequest(array $parameters = []): TestResponse
    {
        return $this->actingAs(
            $this->createAdminUser()
        )->makeRequest($parameters);
    }

    /**
     * Makes valid request to correct endpoint
     *
     * @param array $parameters
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function makeRequest(array $parameters = []): TestResponse
    {
        return $this->get($this->getRoute($parameters));
    }

    /**
     * Gets an factory-created instance of the model for this test
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return \Tests\Concerns\Admin\BaseTest
     */
    protected function setModelInstance(Model $model): BaseTest
    {
        $this->model_instance = $model;

        return $this;
    }

    /**
     * Valid data for storing a new Page
     *
     * @param array $attributes
     *
     * @return array
     */
    protected function validModelData(array $attributes = []): array
    {
        $crud_class = $this->getCrudClass();

        return (new $crud_class())->getEditForm()->setModelInstance(
            factory($this->getModelClass())->make($attributes)
        )->getFormData();
    }
}
