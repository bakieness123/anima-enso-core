<?php

namespace Yadda\Enso\Settings\Contracts;

/**
 * Contract to allow binding the appropriate implementation
 */
interface Model
{
    //
}
