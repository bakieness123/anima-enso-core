<?php

namespace Yadda\Enso\Settings;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use Yadda\Enso\Crud\Forms\CollectionSectionRepositoryInterface;
use Yadda\Enso\Settings\Contracts\Model as ModelContract;
use Yadda\Enso\Settings\Contracts\Repository as RepositoryContract;

class Settings implements RepositoryContract, CollectionSectionRepositoryInterface
{
    protected $class;
    protected $settings;

    /**
     * Collects all the settings from the DB
     */
    public function __construct()
    {
        $this->class = get_class(resolve(ModelContract::class));

        if (Config::get('enso.settings.cache_enso_settings')) {
            $this->settings = Cache::rememberForever('enso-settings', function () {
                return $this->class::all()->keyBy('slug');
            });
        } else {
            $this->settings = $this->class::all()->keyBy('slug');
        }
    }

    /**
     * Gets the actual setting model (as opposed to the value)
     * of a given setting, by slug
     *
     * @param string $name Slug of setting
     *
     * @return Setting Matched setting
     */
    public function getSetting($name)
    {
        if ($this->has($name)) {
            return $this->all()->get($name);
        } else {
            return null;
        }
    }

    /**
     * Gets the internal collection of all current settings
     *
     * @return \Illuminate\Support\Collection Settings collection
     */
    public function all()
    {
        return $this->settings;
    }

    /**
     * Checks whether a given setting exists, by slug
     *
     * @param string $name Slug of setting
     *
     * @return boolean True if setting exists in the database
     */
    public function has($name)
    {
        return $this->all()->has($name);
    }

    /**
     * Gets the value of a given setting, with an optional fallback value to
     * return if the settings does not exist, the setting name is null or if
     * the setting found has an empty value.
     *
     * @param string $name     Slug of setting
     * @param mixed  $fallback Value to return if setting is empty
     *
     * @return mixed
     */
    public function get($name, $fallback = null)
    {
        if (is_null($name)) {
            return $fallback;
        }

        $value = $this->getStrict($name, $fallback);

        return empty($value) ? $fallback : $value;
    }

    /**
     * Gets the value of a given setting, with an optional fallback value to
     * return if the settings does not exist. Otherwise, return the value
     * storing the setting.
     *
     * @param string $name     Slug of setting
     * @param mixed  $fallback Value to return if setting does not exist
     *
     * @return mixed
     */
    public function getStrict($name, $fallback = null)
    {
        $setting = $this->getSetting($name);

        return $setting ? $setting->decoded : $fallback;
    }

    /**
     * Sets a given setting, either updating an existing setting or
     * creating a new one
     *
     * @param string $name      Slug of setting to set
     * @param mixed  $value     Setting value to set
     * @param array  $data_type Array describing the type of data to store
     *
     * @return void
     */
    public function set($name, $value, $data_type = null)
    {
        if (is_null($data_type)) {
            $data_type = [
                'type' => 'scalar',
                'content' => [
                    'id' => $name,
                ],
            ];
        }

        if ($this->has($name)) {
            $setting = $this->getSetting($name);
            $setting->fill([
                'value' => $setting->encodeData($value, $data_type)
            ]);
            $setting->save();
        } else {
            $setting = $this->newItem($name);
            $setting->fill([
                'value' => $setting->encodeData($value, $data_type),
            ]);
            $setting->save();

            $this->all()->put($name, $setting);
        }

        if (Config::get('enso.settings.cache_enso_settings')) {
            Cache::forget('enso-settings');
        }
    }

    /**
     * Creates a new instance of the class that this Repository relates to
     *
     * @return object New instance of correct class
     */
    public function newItem($name)
    {
        return new $this->class([
            'name' => Str::title(preg_replace('#([-_]+)#', ' ', $name)),
            'slug' => $name,
        ]);
    }
}
