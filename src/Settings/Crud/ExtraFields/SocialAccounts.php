<?php

namespace Yadda\Enso\Settings\Crud\ExtraFields;

use Yadda\Enso\Crud\Forms\Fields\DividerField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Settings\Contracts\ExtraSettings;

class SocialAccounts implements ExtraSettings
{
    /**
     * Take a form and process it by adding, removing
     * or updating sections or fields.
     *
     * @param Form $form
     *
     * @return Form
     */
    public static function updateForm(Form $form): Form
    {
        $form
            ->getSection('main')
            ->addFields([
                DividerField::make('social-accounts')
                    ->setTitle('Social Accounts'),
                TextField::make('twitter_url')->addFieldsetClass('is-half'),
                TextField::make('facebook_url')->addFieldsetClass('is-half'),
                TextField::make('instagram_url')->addFieldsetClass('is-half'),
            ]);

        return $form;
    }
}
