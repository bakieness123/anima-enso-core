<?php

namespace Yadda\Enso;

use EnsoMenu;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Yadda\Enso\Settings\Contracts\Controller as ControllerContract;
use Yadda\Enso\Settings\Contracts\Crud as CrudContract;
use Yadda\Enso\Settings\Contracts\Model as ModelContract;
use Yadda\Enso\Settings\Contracts\Repository as RepositoryContract;
use Yadda\Enso\Settings\Controllers\SettingController as ControllerConcrete;
use Yadda\Enso\Settings\Crud\Setting as CrudConcrete;
use Yadda\Enso\Settings\Models\Setting as ModelConcrete;
use Yadda\Enso\Settings\Settings as RepositoryConcrete;

class EnsoSettingsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $installable_dir = __DIR__ . '/../installable/';

        $this->mergeConfigFrom($installable_dir . 'config/crud/setting.php', 'enso.crud.setting');

        $this->loadRoutesFrom(__DIR__ . '/Settings/routes/web.php');

        $this->publishes([
            $installable_dir . 'src/Crud/Setting.php' => app_path('Crud/Setting.php'),
        ], 'enso-crudconfig');

        $this->publishes([
            $installable_dir . 'src/Models/Setting.php' => app_path('Models/Setting.php'),
        ], 'enso-models');

        $this->publishes([
            $installable_dir . 'src/Controllers/Admin/SettingController.php'
                => app_path('Http/Controllers/Admin/SettingController.php'),
        ], 'enso-controllers');

        $this->loadMigrationsFrom([
            __DIR__ . '/Settings/database/migrations/',
        ]);

        EnsoMenu::addItem(Config::get('enso.crud.setting.menuitem'));
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        app()->bind(ControllerContract::class, ControllerConcrete::class);
        app()->bind(CrudContract::class, CrudConcrete::class);
        app()->bind(ModelContract::class, ModelConcrete::class);
        app()->bind(RepositoryContract::class, RepositoryConcrete::class);

        $this->app->singleton('ensosettings', function () {
            return $this->app->make(RepositoryContract::class);
        });
    }
}
