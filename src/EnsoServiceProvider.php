<?php

namespace Yadda\Enso;

use Auth;
use Blade;
use Carbon\Carbon;
use EnsoMenu;
use Gate;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Money\Currencies\ISOCurrencies;
use Money\Formatter\IntlMoneyFormatter;
use Money\Parser\AggregateMoneyParser;
use Money\Parser\DecimalMoneyParser;
use Money\Parser\IntlMoneyParser;
use Route;
use Validator;
use View;
use Yadda\Enso\Console\Commands\Install;
use Yadda\Enso\Console\Commands\MakeCrud;
use Yadda\Enso\Console\Commands\MakeCrudTests;
use Yadda\Enso\Console\Commands\MakeFlexRow;
use Yadda\Enso\Console\Commands\MakePolicyTest;
use Yadda\Enso\Crud\Contracts\FlexibleFieldHandler as FlexibleFieldHandlerContract;
use Yadda\Enso\Crud\Contracts\FlexibleFieldParser as FlexibleFieldParserContract;
use Yadda\Enso\Crud\Crud;
use Yadda\Enso\Crud\Forms\FlexibleFieldParser as FlexibleFieldParserConcrete;
use Yadda\Enso\Crud\Handlers\FlexibleField as FlexibleFieldHandlerConcrete;
use Yadda\Enso\Enso;
use Yadda\Enso\EnsoMenu\ViewComposers\Menu as MenuViewComposer;
use Yadda\Enso\Users\Contracts\PermissionRepositoryContract;
use Yadda\Enso\Users\Contracts\RoleRepositoryContract;

class EnsoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Enso DateTime fields rely on the Carbon v1 json serialization format.
         * For this reason, we are overriding the Carbon serialize method to get
         * the old behaviour.
         */
        Carbon::serializeUsing(function ($date) {
            return [
                'date' => $date->toDateTimeString(),
            ] + (array) $date->tz;
        });

        $installable_dir = __DIR__ . '/../installable/';
        $resources_dir = __DIR__ . '/../resources/';

        $this->mergeConfigFrom($installable_dir . 'config/crud/page.php', 'enso.crud.page');

        $this->loadViewsFrom($resources_dir . 'views/', 'enso');
        $this->loadViewsFrom($resources_dir . 'views/crud', 'enso-crud');
        $this->loadViewsFrom($resources_dir . 'views/dev', 'enso-dev');
        $this->loadViewsFrom($resources_dir . 'views/meta', 'enso-meta');
        $this->loadViewsFrom($resources_dir . 'views/fields', 'enso-fields');

        $this->publishes([
            $installable_dir . 'public/enso/js/enso-admin.js' => public_path('enso/js/enso-admin.js'),
            $installable_dir . 'public/enso/css/enso-admin.css' => public_path('enso/css/enso-admin.css'),
            $installable_dir . 'public/enso/mix-manifest.json' => public_path('enso/mix-manifest.json'),
            $installable_dir . 'resources/images' => resource_path('images/enso'),
            $installable_dir . 'resources/svg' => resource_path('svg'),
        ], 'enso-assets');

        if (file_exists($installable_dir . 'public/enso/js/enso-admin.js.map') && file_exists($installable_dir . 'public/enso/css/enso-admin.css.map')) {
            $this->publishes([
                $installable_dir . 'public/enso/js/enso-admin.js.map' => public_path('enso/js/enso-admin.js.map'),
                $installable_dir . 'public/enso/css/enso-admin.css.map' => public_path('enso/css/enso-admin.css.map'),
            ], 'enso-source-maps');
        }

        $this->publishes([
            $installable_dir . 'tailwind.config.js' => base_path('tailwind.config.js'),
            $installable_dir . 'resources/css/app.css' => resource_path('css/app.css'),
            $installable_dir . 'resources/css/components/buttons.css' => resource_path('css/components/buttons.css'),
            $installable_dir . 'resources/css/components/carousel.css' => resource_path('css/components/carousel.css'),
            $installable_dir . 'resources/css/utilities/media.css' => resource_path('css/utilities/media.css'),
            $installable_dir . 'resources/js/app.js' => resource_path('js/app.js'),
            $installable_dir . 'resources/js/enso' => resource_path('js/enso'),
        ], 'enso-first-time-assets');

        $this->publishes([
            $installable_dir . 'resources/js/enso.js' => resource_path('js/enso.js'),
            $installable_dir . 'resources/js/vendor/enso' => resource_path('js/vendor/enso'),
            $installable_dir . 'resources/sass/enso' => resource_path('sass/enso'),
            $installable_dir . 'resources/sass/enso.scss' => resource_path('sass/enso.scss'),
        ], 'enso-assets-source');

        $this->publishes([
            $installable_dir . 'resources/views/dashboard.blade.php'
                => resource_path('views/admin/dashboard.blade.php'),
            $installable_dir . 'resources/views/pages/templates/default.blade.php'
                => resource_path('views/pages/templates/default.blade.php'),
            $installable_dir . 'resources/views/errors' => resource_path('views/errors'),
            $installable_dir . 'resources/views/layouts/app.blade.php'
                => resource_path('views/layouts/app.blade.php'),
            $installable_dir . 'resources/views/parts' => resource_path('views/parts'),
        ], 'enso-views');

        $this->publishes([
            $installable_dir . 'resources/views/auth' => resource_path('views/auth'),
        ], 'enso-auth-views');

        $this->publishes([
            $installable_dir . 'webpack.mix.js' => base_path('webpack.mix.js'),
            $installable_dir . 'vite.config.js' => base_path('vite.config.js'),
        ], 'enso-buildfiles');

        $this->publishes([
            $installable_dir . '.eslint.json' => base_path('.eslint.json'),
            $installable_dir . '.prettierrc' => base_path('.prettierrc'),
            $installable_dir . '.stylelintrc.json' => base_path('.stylelintrc.json'),
            $installable_dir . 'phpcs.xml' => base_path('phpcs.xml'),
            $installable_dir . '.editorconfig' => base_path('.editorconfig'),
        ], 'enso-code-quality-tools');

        $this->publishes([
            $installable_dir . 'enso' => base_path('enso'),
            $installable_dir . 'docker-compose.yml' => base_path('docker-compose.yml'),
            $installable_dir . 'docker' => base_path('docker'),
        ], 'enso-docker');

        $this->publishes([
            $installable_dir . 'src/Crud/Page.php' => app_path('Crud/Page.php'),
        ], 'enso-crudconfig');

        $this->publishes([
            $installable_dir . 'src/Models/Page.php' => app_path('Models/Page.php'),
        ], 'enso-models');

        $this->publishes([
            $installable_dir . 'config' => config_path('enso'),
        ], 'enso-config');

        $this->publishes([
            $installable_dir . 'src/Controllers/DashboardController.php'
                => app_path('Http/Controllers/Admin/DashboardController.php'),
            $installable_dir . 'src/Controllers/Admin/PageController.php'
                => app_path('Http/Controllers/Admin/PageController.php'),
            $installable_dir . 'src/Controllers/PageController.php'
                => app_path('Http/Controllers/PageController.php'),
            $installable_dir . 'src/Controllers/HomeController.php'
                => app_path('Http/Controllers/HomeController.php'),
            $installable_dir . 'src/Controllers/HoldingPageController.php'
                => app_path('Http/Controllers/HoldingPageController.php'),
        ], 'enso-controllers');

        $this->publishes([
            $installable_dir . 'routes/web.php' => base_path('routes/web.php'),
            $installable_dir . 'routes/api.php' => base_path('routes/api.php'),
        ], 'enso-routes');

        View::composer('enso::layouts.partials.navbar', MenuViewComposer::class);

        $this->loadMigrationsFrom([
            __DIR__ . '/Meta/Database/Migrations/',
            __DIR__ . '/Users/Database/Migrations/',
            __DIR__ . '/Pages/Database/Migrations/',
        ]);

        if (config('app.debug')) {
            Route::get('dev/bulma', '\Yadda\Enso\Dev\Controllers\DevController@bulma');
            Route::get('dev/bootstrap', '\Yadda\Enso\Dev\Controllers\DevController@bootstrap4');
        }

        /**
         * Define a blade directive for inserting SVGs from sprites, allowing for
         * dynamic properties. Relies on the sprite sheet being loaded on the page.
         */
        Blade::directive('svg', function ($argument) {
            $args = explode(',', str_replace(['\'', ' '], '', $argument));

            foreach ($args as $index => $arg) {
                $args[$index] = trim($arg, '\"\'');
            }

            $svg = '<svg ';

            if (isset($args[2]) && !empty($args[2])) {
                $svg .= 'id="<?php echo with("' . $args[2] . '") ?>" ';
            }

            if (isset($args[1]) && !empty($args[1])) {
                $svg .= 'class="<?php echo with("' . $args[1] . '") ?>"';
            }

            $svg .= '><use xlink:href="';
            $svg .= "<?php echo mix('svg/sprite.svg') . '#' . with(\"" . $args[0] . "\"); ?>";
            $svg .= '"></use></svg>';

            return $svg;
        });

        Blade::directive('flexibleField', function ($expression) {
            $class = get_class(resolve(FlexibleFieldHandlerContract::class));

            $args = explode(',', str_replace(['\'', ' '], '', $expression));

            foreach ($args as $index => $arg) {
                $args[$index] = trim($arg, '\"\'');
            }

            $content_call = '<?php echo ' . $class . '::getRowContent(';

            if (isset($args[0]) && !empty($args[0])) {
                $content_call .= 'with(' . $args[0] . ')';
            }

            for ($i = 1; $i < count($args); $i++) {
                if (isset($args[$i])) {
                    if (!empty($args[$i])) {
                        $content_call .= ', with("' . $args[$i] . '")';
                    }
                } else {
                    break;
                }
            }

            $content_call .= '); ?>';

            return $content_call;
        });

        /**
         * @todo Once we are using Laravel 5.5 we can split this off into its
         * own Rule file.
         */
        Validator::extend('wysiwyg_required', function ($attribute, $value, $parameters, $validator) {
            return !empty($value['html']);
        });

        Gate::define('has-permission', function ($user, $permission) {
            return Auth::user()->hasPermission($permission);
        });

        if ($this->app->runningInConsole()) {
            $this->commands([
                Install::class,
                MakeCrud::class,
                MakeCrudTests::class,
                MakeFlexRow::class,
                MakePolicyTest::class,
            ]);
        }

        EnsoMenu::addItem(Config::get('enso.crud.page.menuitem'));

        $router = $this->app['router'];
        $router->pushMiddlewareToGroup('enso', \Illuminate\Auth\Middleware\Authenticate::class);
        $router->pushMiddlewareToGroup('holding-page', \Yadda\Enso\Pages\Middleware\HoldingPage::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Registers in IOC container.
        $this->app->singleton('enso', function () {
            return new Enso;
        });

        $this->app->singleton('ensomenu', 'Yadda\Enso\EnsoMenu\Menu');

        $this->app->singleton('ensocrud', function () {
            return new Crud;
        });

        $this->app->singleton('ensometa', 'Yadda\Enso\Meta\Meta');

        $this->app->bind(RoleRepositoryContract::class, function ($app) {
            $role_repository_concrete_class = config('enso.users.role_repository_concrete_class');
            return new $role_repository_concrete_class();
        });

        $this->app->bind(PermissionRepositoryContract::class, function ($app) {
            $permission_repository_concrete_class = config('enso.users.permission_repository_concrete_class');
            return new $permission_repository_concrete_class();
        });

        $this->app->singleton('moneyparser', function () {
            $currencies = new ISOCurrencies();
            $number_formatter = new \NumberFormatter('en_US', \NumberFormatter::CURRENCY);
            $intl_parser = new IntlMoneyParser($number_formatter, $currencies);
            $decimal_parser = new DecimalMoneyParser($currencies);
            $money_parser = new AggregateMoneyParser([
                $intl_parser,
                $decimal_parser,
            ]);

            return $money_parser;
        });

        $this->app->singleton('moneyformatter', function () {
            $currencies = new ISOCurrencies();
            $number_formatter = new \NumberFormatter('en_US', \NumberFormatter::CURRENCY);
            $money_formatter = new IntlMoneyFormatter($number_formatter, $currencies);

            return $money_formatter;
        });

        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('MoneyFormatter', \Yadda\Enso\Facades\MoneyFormatter::class);
        $loader->alias('MoneyParser', \Yadda\Enso\Facades\MoneyParser::class);

        // @deprecated v0.2.38 - We still need to make the menu, but not add items.
        // @todo - Remove in the future
        $this->app->make('ensomenu')->addItems(config('enso.menu.items', []));

        app()->bind(FlexibleFieldParserContract::class, FlexibleFieldParserConcrete::class);
        app()->bind(FlexibleFieldHandlerContract::class, FlexibleFieldHandlerConcrete::class);
    }
}
