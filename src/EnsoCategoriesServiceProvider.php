<?php

namespace Yadda\Enso;

use EnsoMenu;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

class EnsoCategoriesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $installable_dir = __DIR__ . '/../installable/';

        $this->mergeConfigFrom($installable_dir . 'config/crud/category.php', 'enso.crud.category');

        $this->loadRoutesFrom(__DIR__ . '/Categories/Routes/web.php');

        $this->loadMigrationsFrom([
            __DIR__ . '/Categories/Database/Migrations/',
        ]);

        EnsoMenu::addItem(Config::get('enso.crud.category.menuitem'));
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('ensocategories', 'Yadda\Enso\Categories\EnsoCategories');
    }
}
