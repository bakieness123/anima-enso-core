<?php

namespace Yadda\Enso\Media;

use ColorThief\ColorThief;
use Exception;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use InvalidArgumentException;
use Mexitek\PHPColors\Color;
use RuntimeException;
use Yadda\Enso\Media\Contracts\ImageFile;
use Yadda\Enso\Media\Exceptions\MediaException;

/**
 * Class to determine color related properties on an ImageFile.
 *
 * Using this class requires that you have run the installed:
 *
 * composer require ksubileau/color-thief-php
 * composer require mexitek/phpcolors dev-master
 */
class Colors
{
    /**
     * Updates the given ImageFile's fileinfo to include data about the lightness
     * state of specific areas of an image.
     *
     * Optionally, specify a preset. If you do not specify a preset it will
     * generate is_light values for the base image, and save then under 'no_preset'
     *
     * @param ImageFile $image
     * @param array     $area_names
     * @param string    $preset_name
     *
     * @return ImageFile
     */
    public static function setLightDarkFor(ImageFile $image, $area_names = [], $preset_name = null): ImageFile
    {
        $valid_areas = Config::get('enso.media.light_dark_areas', []);

        foreach ($area_names as $area_key) {
            if (!array_key_exists($area_key, $valid_areas)) {
                throw new MediaException(
                    'Requested light_dark area has not been defined: `' . $area_key . '`'
                );
            }

            // Fileinfo is an array, so no need to clone
            $fileinfo_copy = $image->fileinfo;

            if ($preset_name) {
                // Image resize MUST exist
                $image->makeResize($preset_name);

                $fileinfo_copy['is_light'][$preset_name][$area_key] = static::isLight(
                    Storage::disk($image->getDisk())->path($image->getResizePath($preset_name)),
                    $valid_areas[$area_key]
                );
            } else {
                $fileinfo_copy['is_light']['no_preset'][$area_key] = static::isLight(
                    Storage::disk($image->getDisk())->path($image->getPath()),
                    $valid_areas[$area_key]
                );
            }

            $image->fileinfo = $fileinfo_copy;
        }

        $image->save();

        return $image;
    }

    /**
     * Determine if a given area of an image at a specified path is "Light".
     *
     * Given area must be compatible with the parseArea function. If parsing is
     * unable to complete, assume it _is_ light.
     *
     * @param string $image
     * @param mixed  $area
     *
     * @return boolean
     */
    public static function isLight($image, $area)
    {
        try {
            $rgb = ColorThief::getColor($image, 10, static::parseArea(
                $area,
                $image
            ));

            $hex = Color::rgbToHex(array_combine(
                ['R', 'G', 'B'],
                $rgb
            ));

            return (new Color($hex))->isLight();
        } catch (RuntimeException $e) {
            // This could occur if the image isn't parseable, probably due to
            // being a 'blank or transparent' image. As such, assume light.
            return true;
        } catch (Exception $e) {
            return true;
        }
    }

    /**
     * Parses the $area argument to ensure an area array has keys (implemented
     * to enable smaller configs) and to parse percentage values. An image path
     * must be specified (for assessing image size) to parse percentages.
     *
     * @param mixed       $area
     * @param string|null $image
     *
     * @return array
     */
    public static function parseArea($area, string $image = null): array
    {
        $area = static::validateArea($area);

        if ($area && !array_key_exists('x', $area)) {
            $area = array_combine(
                ['x', 'y', 'w', 'h'],
                $area
            );
        }

        // Get width and height for parsing percentages, if possible.
        if ($image) {
            $image_size_data = getimagesize($image);

            $area = static::parsePercentageValues(
                $area,
                $image_size_data[0],
                $image_size_data[1]
            );
        }

        return $area;
    }

    /**
     * Validates the area argument. This can pull an area definition from the
     * config by passing a $area as an array key of `enso.media.light_dark_areas`
     *
     * @throws InvalidArgumentException
     * @throws MediaException
     *
     * @param mixed $area
     *
     * @return array
     */
    protected static function validateArea($area): array
    {
        if (!is_array($area)) {
            $valid_areas = Config::get('enso.media.light_dark_areas', []);

            if (!array_key_exists($area, $valid_areas)) {
                throw new InvalidArgumentException(
                    '$area must be an array of values or the name of a valid '
                        . 'area defined in the enso.media.light_dark_areas config'
                );
            }

            $area = $valid_areas[$area];
        }

        if (!is_array($area) || !count($area) === 4) {
            throw new MediaException(
                'An area definition must be an array containing 4 elements, and
                optionally keyed as `x`, `y`, `w` and `h`'
            );
        }

        return $area;
    }

    /**
     * Parses percentage values in the area array into integers.
     *
     * @param array   $area
     * @param integer $width
     * @param integer $height
     *
     * @return array
     */
    protected static function parsePercentageValues($area, $width, $height): array
    {
        // Convert percentage width values
        foreach (['x', 'w'] as $key) {
            if (strpos((string) $area[$key], '%') !== false) {
                $area[$key] = ($width / 100)
                    * intVal(str_replace('&#37;', '', $area[$key]));
            }
        }

        // Convert percentage height values
        foreach (['y', 'h'] as $key) {
            if (strpos((string) $area[$key], '%') !== false) {
                $area[$key] = ($height / 100)
                    * intVal(str_replace('&#37;', '', $area[$key]));
            }
        }

        return $area;
    }
}
