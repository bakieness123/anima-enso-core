<?php

namespace Yadda\Enso\Media\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Storage;
use Yadda\Enso\Media\Contracts\ImageFile;
use Yadda\Enso\Media\Contracts\MediaFile;
use Yadda\Enso\Media\Filters\Preset;

class TransferFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'enso:transfer-files
                            {old_disk : The disk that files currently reside}
                            {new_disk : The disk that files should be copied to}
                            {--l|limit= : Number of Files to transfer}
                            {--p|page= : Page of results to transfer}
                            {--b|background= : Override background color to apply ' .
        'to images that don\'t cover the whole area. ' .
        'Comma separated rgba values e.g. 255,255,255,0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Transfer Files from one Storage Disk to another';

    protected $old_disk;

    protected $new_disk;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!$this->validateDiskArguments()) {
            return;
        }

        $page = $this->option('page');
        if (!is_null($page) && (!is_numeric($page) || $page <= 0)) {
            $this->line('Page must me a number greater than 0');
            return;
        }

        $limit = $this->option('limit');
        if (!is_null($limit) && (!is_numeric($limit) || $limit <= 0)) {
            $this->line('Limit must me a number greater than 0');
            return;
        }

        $background_string = $this->option('background');
        if (!$background_string) {
            $background = [255, 255, 255, 0];
        } else {
            $background = explode(',', $background_string);

            if (!is_array($background) || count($background) !== 4) {
                $this->line('Invalid Background format.');
                return;
            }
        }

        $query = resolve(MediaFile::class);

        $total = (clone ($query))->count();

        if ($limit && $page) {
            $query = $query->take($limit)->offset(($page - 1) * $limit);

            $start = (($page - 1) * $limit);
            $end = ($page * $limit) - 1;

            $amount = max(0, min($total - $start, $limit));

            $message = 'Found ' . $amount . ' of ' . $total . ' files. Continue?';
            $bar_count = $amount;
        } else {
            $message = 'Found ' . $total . ' files. Continue?';
            $bar_count = $total;
        }

        if (!$this->confirm($message)) {
            $this->line('Aborting.');
            return;
        }

        $this->presets = $this->getPresetNames();
        $this->media_file_class = get_class(resolve(ImageFile::class));

        if ($limit && $page) {
            /**
             * Chunk can't be called on a query with limit and offset, so need to
             * get the full query when working on a single page.
             */
            $this->transferFiles($query->get(), $background);
        } else {
            /**
             * If working on the entire data-set, chunk the results in case there is a lot.
             */
            $query->chunk(100, function ($files) use ($background) {
                $this->transferFiles($files, $background);
            });
        }

        $this->line('');
        $this->comment('Done.');
    }

    /**
     * Transfers a Collection of files from one disk to another, potentially
     * transfering image resizes as well.
     *
     * @param Collection $files
     * @param array $background
     * @param array $presets
     * @param string $media_file_class
     *
     * @return void
     */
    public function transferFiles($files, $background)
    {
        $correct_files = $files->map(function ($file) {
            return resolve(MediaFile::class)::getCorrectModelType($file);
        });

        foreach ($correct_files as $file) {
            try {
                $success = $this->transferOriginalFile($file->getPath());

                if ($success && ($file instanceof $this->media_file_class)) {
                    foreach ($this->presets as $preset) {
                        $this->transferResizeFile($file->getResizePath($preset), false);
                    }
                }
            } catch (Exception $e) {
                $this->error('File ' . $file->getKey() . ': ' . $e->getMessage());
                continue;
            }
        }
    }

    /**
     * Ensures that the received arguments are valid disk names
     *
     * @return boolean
     */
    private function validateDiskArguments()
    {
        $disks = array_keys(config('filesystems.disks'));

        $this->old_disk = $this->argument('old_disk');
        $this->new_disk = $this->argument('new_disk');

        if (!in_array($this->old_disk, $disks)) {
            $this->error('Invalid `old_disk`');
            return false;
        }

        if (!in_array($this->new_disk, $disks)) {
            $this->error('Invalid `new_disk`');
            return false;
        }

        return true;
    }

    /**
     * Gets the full list of media presets available.
     *
     * @return array
     */
    private function getPresetNames()
    {
        return array_keys(
            array_merge(
                config('enso.media.presets'),
                Preset::$builtin_presets
            )
        );
    }

    /**
     * Gets a resource from the old disk and transfers it to the new disk.
     *
     * @param string $file_path
     * @param boolean $is_original
     *
     * @return boolean
     */
    private function transferFile($file_path, $is_original = true)
    {
        try {
            $resource = Storage::disk($this->old_disk)->get($file_path);

            if ($resource) {
                if ($is_original) {
                    $this->info('Copying file: ' . $file_path);
                } else {
                    $this->line('Resize: ' . $file_path);
                }

                Storage::disk($this->new_disk)->put($file_path, $resource);
            }

            return true;
        } catch (FileNotFoundException $e) {
            /**
             * Only output errors on orignal files not existing.
             */
            if ($is_original) {
                $this->error('Source file not found: ' . $file_path);
            }
        } catch (Exception $e) {
            $this->error($file_path . ' transfer: ' . $e->getMessage());
        }

        return false;
    }

    /**
     * Defers to transferFile, specifying that this is the original file.
     *
     * @param string $file_path
     *
     * @return boolean
     */
    private function transferOriginalFile($file_path)
    {
        return $this->transferFile($file_path, true);
    }

    /**
     * Defers to transferFile, specifying that this is not the original file.
     *
     * @param string $file_path
     *
     * @return boolean
     */
    private function transferResizeFile($file_path)
    {
        $this->transferFile($file_path, false);
    }
}
