<?php

namespace Yadda\Enso\Media\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use stdClass;
use Yadda\Enso\Crud\Contracts\IsCrudModel as ContractsIsCrudModel;
use Yadda\Enso\Crud\Traits\IsCrudModel;
use Yadda\Enso\Facades\EnsoMedia;
use Yadda\Enso\Media\Contracts\MediaFile as MediaFileContract;
use Yadda\Enso\Media\Exceptions\FileInUseException;
use Yadda\Enso\Users\Contracts\User as UserContract;

/**
 * Model representing a file uploaded to Enso
 */
class MediaFile extends Model implements MediaFileContract, ContractsIsCrudModel
{
    use IsCrudModel;

    /**
     * Database table on which to store file data
     *
     * @var string
     */
    protected $table = 'files';

    /**
     * Fields that should be cast to other types
     *
     * @var array
     */
    protected $casts = [
        'filesize' => 'integer',
        'fileinfo' => 'array'
    ];

    /**
     * Attributes to append when converting to JSON or array
     */
    protected $appends = [
        'filetype',
    ];

    /**
     * Fields that are bulk fillable
     *
     * @var array
     */
    protected $fillable = [
        'alt_text',
        'caption',
        'filename',
        'filesize',
        'focus_x',
        'focus_y',
        'original_filename',
        'path',
        'title',
    ];

    /**
     * The base folder for this file type
     *
     * @var string
     */
    protected $folder = 'files';

    /**
     * Holds SplFileInfo of the file that this model represents, if it exists
     *
     * @var SplFileInfo
     */
    protected $file;

    /**
     * Storage disk to store this file on
     *
     * @var string
     */
    protected $disk;

    /**
     * Amount of RAM to allow when resizing images
     *
     * This will be passed to ini_set('memory_limit') so should be in the format
     * '128M' or '1G'
     *
     * @var string
     */
    protected $memory_limit;

    /**
     * Maximum file length
     *
     * @var int
     */
    const MAX_FILENAME_LENGTH = 255;

    /**
     * Create a new MediaFile
     *
     * The create or make static methods should usually be used instead of this
     *
     * @param array $attributes values to set on model attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->memory_limit = config('enso.media.memory_limit', '1G');

        if (!$this->disk) {
            $this->disk = config('enso.media.disk');
        }
    }

    /**
     * The value to use at this Model's CRUD label.
     *
     * NOTE: title can be null or empty string in some circumstances
     *
     * @return string
     */
    public function getCrudLabel(): string
    {
        return empty($this->title) ?  $this->filename : $this->title;
    }

    /**
     * Gets the username of the owner of this image, or empty string
     * if not available
     *
     * @return string
     */
    public function getOwnerUsername(): string
    {
        return $this->user ? (string) $this->user->username : '';
    }

    /**
     * Gets the Display name of the owner of this image, or empty string if not
     * available.
     *
     * @return string
     */
    public function getOwnerDisplayName(): string
    {
        return $this->user ? (string) $this->user->display_name : '';
    }

    /**
     * Check if this file has been attached to any models
     *
     * @return bool
     */
    public function isUsed(): bool
    {
        return $this->usageCount() > 0;
    }

    /**
     * Count uses of this file in other models
     *
     * @return int
     */
    public function usageCount(): int
    {
        return DB::table('file_model')
            ->where('file_id', $this->getKey())
            ->count();
    }

    /**
     * Gets the User that uploaded / owns this MediaFile
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(resolve(UserContract::class), 'user_id');
    }

    /**
     * Concatenated all the non-empty segments for the path/url of the file that
     * this model represents. Empty parts are ignored.
     *
     * @param array $segments path segments to concatenate
     *
     * @return string
     */
    protected function concatenateFilepathSegments(array $segments): string
    {
        $segments = array_filter($segments, 'strlen');

        return implode('/', $segments);
    }

    /**
     * Get a fileinfo property by name
     *
     * @param string $property name of property to get
     *
     * @return mixed
     */
    public function getFileinfoProperty($property)
    {
        return isset($this->fileinfo[$property]) ? $this->fileinfo[$property] : null;
    }

    /**
     * Sets a given value on the fileinfo property.
     *
     * @param string $property property name
     * @param mixed  $value    value to set
     *
     * @return void
     */
    public function setFileinfoProperty($property, $value): void
    {
        $fileinfo = $this->fileinfo;
        $fileinfo[$property] = $value;
        $this->fileinfo = $fileinfo;
    }

    /**
     * Adds an item to an array property in fileinfo, if it is not already
     * present in the array
     *
     * @param string $property property name
     * @param mixed  $value    value to add to array
     *
     * @return void
     */
    protected function addArrayFileinfoProperty($property, $value): void
    {
        $current_resizes = $this->getFileinfoProperty($property);

        if (!in_array($value, $current_resizes)) {
            $current_resizes[] = $value;
            $this->setFileinfoProperty($property, $current_resizes);
        }
    }

    /**
     * Removes an item from an array property in fileinfo, if it is present in
     * the array
     *
     * @param string $property property to remove from
     * @param mixed  $value    value to remove
     *
     * @return void
     */
    protected function removeArrayFileinfoProperty($property, $value): void
    {
        $current_resizes = $this->getFileinfoProperty($property);
        $diff = array_diff($current_resizes, (array) $value);

        if (count($diff) !== count($current_resizes)) {
            $this->setFileinfoProperty($property, $diff);
        }
    }

    /**
     * Loads the file into the local $file property. If it doesn't exist, put
     * an empty string in there instead, to signify that it has tried to find it
     * and failed.
     *
     * @return void
     */
    protected function getFile(): void
    {
        if (is_null($this->file)) {
            try {
                $this->file = Storage::disk($this->disk)->get($this->getPath());
            } catch (Exception $e) {
                Log::error('Failed attempting to open file: ' . $this->getPath());
            }
        }
    }

    /**
     * Checks to see whether the file that this model represents actually exists
     * at it's specified path
     *
     * @return bool true if file exists
     */
    public function fileExists(): bool
    {
        return Storage::disk($this->disk)->exists($this->getPath());
    }

    /**
     * Gets the url to this file that this model represents
     *
     * @return string full url of file
     */
    public function getUrl(): ?string
    {
        $relative_path = $this->getPath();

        return Storage::disk($this->disk)->url($relative_path);
    }

    /**
     * Gets the path for the file that this model represents relative to the
     * root storage path. This can be passed to Storage::url() or similar
     * to get the actual URL. Or you can just use getUrl() because that's
     * what that does.
     *
     * @param string $filename Override the filename
     *
     * @return string full path to file
     */
    public function getPath($filename = null): string
    {
        if (is_null($filename)) {
            $filename = $this->filename;
        }

        return $this->concatenateFilepathSegments([
            config('enso.media.directory'),
            $this->folder,
            $this->path,
            $filename
        ]);
    }

    /**
     * The name of the folder to store this file type in
     */
    public function getFolder(): string
    {
        return $this->folder;
    }

    /**
     * Return all potential paths for this file. These paths may not point to
     * existing files, it is an exhasutive list for making sure everything
     * gets deleted.
     *
     * @return array
     */
    public function getAllPaths(): array
    {
        return [
            $this->getPath(),
        ];
    }

    /**
     * Gets the full path for the directory that the file this model represents
     * is in
     *
     * @return string Directory of file
     */
    public function getDirectory(): string
    {
        return public_path($this->getRelativeDirectory());
    }

    /**
     * Gets the Disk associated with this image.
     *
     * @return string
     */
    public function getDisk(): string
    {
        return $this->disk ?? config('enso.media.disk');
    }

    /**
     * Get the path for the directory that this file is in,
     * relative to the public directory
     *
     * @return string
     */
    public function getRelativeDirectory(): string
    {
        return $this->concatenateFilepathSegments([
            config('enso.media.directory'),
            $this->folder,
            $this->path,
        ]);
    }

    /**
     * Upload a given file into a given subdirectory of the media directory
     *
     * @param \Illuminate\Http\File $file The file to use
     * @param string                $path The path to store the file relative
     *                                    to the media storage path
     *
     * @return void
     */
    public function useUpload($file, $path, $filename = null): void
    {
        $this->path = $path;

        if (is_null($filename)) {
            $this->original_filename = $file->getClientOriginalName();
        } else {
            $this->original_filename = $filename;
        }

        $dir = $this->getRelativeDirectory();
        $this->filename = $this->makeUniqueFilename();

        Storage::disk($this->disk)->putFileAs($dir, $file, $this->filename);
    }

    /**
     * Take a file and store it to disk
     *
     * @param \Illuminate\Http\File $file The file to use
     * @param string                $path The path to store the file relative
     *                                    to the media base path
     * @return void
     */
    public function useFile(File $file, $path, $original_filename = null): void
    {
        $this->path = $path;

        if (!$original_filename) {
            $this->original_filename = basename($file);
        } else {
            $this->original_filename = $original_filename;
        }

        $this->filename = $this->makeUniqueFilename();
        $dir = $this->getRelativeDirectory();

        Storage::disk($this->disk)->putFileAs($dir, $file, $this->filename);
    }

    /**
     * Download a file and store it to disk
     *
     * @param string $url  URL to get file from
     * @param string $path Path to store the file relative to media base path
     *
     * @return void
     */
    public function useRemoteFile($url, $path, $filename = null): void
    {
        if (!$filename) {
            $filename = basename(parse_url($url, PHP_URL_PATH));
        }

        $tmp_path = storage_path('app/temp');

        if (!is_dir($tmp_path)) {
            mkdir($tmp_path);
        }

        $temp_uri = $tmp_path . '/' . $filename;
        $fp = fopen($temp_uri, 'w');

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch);
        curl_close($ch);

        $uri = stream_get_meta_data($fp)['uri'];

        $file = new File($uri);

        fclose($fp);

        $this->useFile($file, $path, $filename);

        unlink($temp_uri);
    }

    /**
     * Make up a unique file name based on the original file's name
     * and set it as the filename
     *
     * @param string $original_filename a desired filename
     *
     * @return string
     */
    protected function makeUniqueFilename(string $original_filename = null): string
    {
        if (is_null($original_filename)) {
            $original_filename = $this->original_filename;
        }

        do {
            $filename = $this->makeFilename($original_filename);
        } while (Storage::disk($this->disk)->exists($this->getPath($filename)));

        return $filename;
    }

    /**
     * Create a new, hopefully unique, filename by appending a unique ID
     *
     * @param string $filename Original filename
     *
     * @return string
     */
    public function makeFilename($filename): string
    {
        $name = pathinfo($filename, PATHINFO_FILENAME);
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        $unique = uniqid();
        $name = Str::slug(substr($name, 0, self::MAX_FILENAME_LENGTH - strlen($unique) - strlen($ext) - 2));

        return $name . '-' . $unique . '.' . $ext;
    }

    /**
     * Sets and properties which are gleaned from accessing the file itself,
     * such as filesize, width + height of images etc.
     *
     * @return void
     */
    public function setDerivedProperties(): void
    {
        if (!$this->fileExists()) {
            throw new Exception('Attempting to update file information from a file that does not exist');
        }

        $initial_memory = ini_get('memory_limit');
        ini_set('memory_limit', $this->memory_limit);

        $this->getFile();

        $this->filesize = (int) Storage::disk($this->disk)->size($this->getPath());
        $this->mimetype = Storage::disk($this->disk)->mimeType($this->getPath());

        $this->setDerivedFileinfoProperties();

        ini_set('memory_limit', $initial_memory);
    }

    /**
     * A method for extending classes to define filetype specific properties in
     * the fileinfo property of this file.
     *
     * @return void
     */
    protected function setDerivedFileinfoProperties(): void
    {
        // Essentially setting default value for things that don't extend this
        $this->fileinfo = new stdClass;
    }

    /**
     * Get the type of file (image/video etc.) from the mime type
     *
     * @return string
     */
    protected function getFileTypeAttribute(): string
    {
        return EnsoMedia::typeFromMime($this->mimetype);
    }

    /**
     * Return the URL to a preview image
     *
     * @return string
     */
    public function getPreview(): string
    {
        if ($this->isSvg()) {
            return $this->getUrl();
        }

        return asset('svg/enso/icon-file-document.svg');
    }

    /**
     * Return the URL for a detailed preview image for
     * when viewing the file's details
     *
     * @return string
     */
    public function getPreviewFull(): string
    {
        if ($this->isSvg()) {
            return $this->getUrl();
        }

        return asset('svg/enso/icon-file-document.svg');
    }

    /**
     * Accessor for getting the preview URL as an attribute
     *
     * @return string
     */
    public function getPreviewAttribute(): string
    {
        return $this->getPreview();
    }

    /**
     * Whether this file is an SVG
     *
     * @return boolean
     */
    public function isSvg(): bool
    {
        return in_array($this->mimetype, ['image/svg', 'image/svg+xml']);
    }

    /**
     * Rename the file
     *
     * @param string $name the new filename
     *
     * @return bool True if successful
     */
    public function rename($name = null)
    {
        if (is_null($name)) {
            $name = $this->original_filename;
        }

        $name = $this->makeFilename($name);

        $from = $this->getPath();
        $to = pathinfo($from, PATHINFO_DIRNAME) . '/' . $name;

        return Storage::disk($this->disk)->move($from, $to);
    }

    /**
     * Converts this MediaFile into an array of data
     *
     * @return array
     */
    public function toArray(): array
    {
        $file = static::getCorrectModelType($this);

        return [
            'id' => $file->getKey(),
            'filename' => $file->filename,
            'filesize' => $file->filesize,
            'fileinfo' => $file->fileinfo,
            'mediatype' => EnsoMedia::typeFromMime($file->mimetype),
            'path' => $file->path,
            'preview' => $file->getPreview(),
            'preview_full' => $file->getPreviewFull(),
            'created_at' => $file->created_at,
            'updated_at' => $file->updated_at,
            'url' => $file->getUrl(),
            'original_filename' => $file->original_filename,
            'mimetype' => $file->mimetype,
            'username' => $file->getOwnerUsername(),
            'user_displayname' => $file->getOwnerDisplayName(),
            'usage_count' => $file->usageCount(),
            'title' => $file->title,
            'caption' => $file->caption,
            'alt_text' => $file->alt_text,
            'focus_x' => $file->focus_x,
            'focus_y' => $file->focus_y,
        ];
    }

    /**
     * @deprecated 0.2.253
     *
     * @return array
     */
    public function mapForJson(): array
    {
        return $this->toArray();
    }

    /**
     * Delete the database record and files
     *
     * @return bool|null
     */
    public function delete(): ?bool
    {
        if ($this->isUsed()) {
            throw new FileInUseException(
                'File is attached to another model and can\'t be deleted. '
                    . 'Detach and try again.'
            );
        }

        $this->deleteAllFiles();

        return parent::delete();
    }

    /**
     * Removes all files associated with this model.
     *
     * Does not delete the database record.
     *
     * @return void
     */
    public function deleteAllFiles(): void
    {
        $paths = $this->getAllPaths();

        foreach ($paths as $path) {
            Storage::disk($this->disk)->delete($path);
        }
    }

    /**
     * Take a model that extends MediaFile and return the appropriate model
     * type. E.g. if it is a record for an image, return an ImageFile.
     *
     * @param MediaFile $item the original model
     *
     * @return MediaFile the model of the correct type
     */
    public static function getCorrectModelType($item): MediaFile
    {
        $type = EnsoMedia::typeFromMime($item->mimetype);
        $model = EnsoMedia::getModelFromType($type);
        $attributes = $item->getAttributes();

        $out = new $model();

        foreach ($attributes as $name => $value) {
            $out->$name = $value;
        }

        $out->fileinfo = $item->fileinfo;
        $out->exists = $item->exists;
        $out->setRelations($item->getRelations());

        return $out;
    }
}
