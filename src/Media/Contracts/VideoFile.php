<?php

namespace Yadda\Enso\Media\Contracts;

use Yadda\Enso\Media\Contracts\MediaFile;

/**
 * Contract for binding the VideoFile implementation
 */
interface VideoFile extends MediaFile
{
    //
}
