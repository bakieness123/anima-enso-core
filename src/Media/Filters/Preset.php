<?php

namespace Yadda\Enso\Media\Filters;

use Illuminate\Support\Arr;
use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;
use Intervention\Image\ImageManagerStatic;
use Yadda\Enso\Media\Exceptions\InvalidPresetException;

class Preset implements FilterInterface
{
    /**
     * The name of an image preset
     *
     * @var float
     */
    protected $preset_name;

    public static $builtin_presets = [
        'og_large' => [1200, 630],
        'og_small' => [600, 315],
        'twitter_square_large' => [1024, 1024],
        'twitter_quare_small' => [512, 512],
        'twitter_wide_large' => [1024, 512],
        'twitter_wide_small' => [300, 157],
        'uploader_preview' => [200, 200],
        'uploader_preview_full' => [545, null],
        '1_1_640' => [640, 640],
    ];

    /**
     * Horizontal focal point of the image.
     *
     * 0 is left, 1 is right. If not set, 0.5 will be used.
     *
     * @var float
     */
    protected $focus_x;

    /**
     * Vertical focal point of the image.
     *
     * 0 is top, 1 is bottom. If not set, 0.5 will be used.
     *
     * @var float
     */
    protected $focus_y;

    /**
     * Create a new Preset
     */
    public function __construct($preset_name, $focus_x = null, $focus_y = null)
    {
        $this->preset_name = $preset_name;
        $this->focus_x = $focus_x ?? 0.5;
        $this->focus_y = $focus_y ?? 0.5;
    }

    /**
     * Applies filter to given image
     *
     * @param  \Intervention\Image\Image $image
     * @return \Intervention\Image\Image
     */
    public function applyFilter(Image $image)
    {
        $image = ImageManagerStatic::make($image);

        $preset = config('enso.media.presets.' . $this->preset_name);

        if (!$preset && array_key_exists($this->preset_name, self::$builtin_presets)) {
            $preset = self::$builtin_presets[$this->preset_name];
        }

        if (!$preset) {
            throw new InvalidPresetException('Unknown preset ' . $this->preset_name);
        }

        list($width, $height, $crop_style, $filter_style, $filter_value) = array_pad($preset, 6, null);

        if ($filter_style === 'lighten') {
            $image->filter(new Lighten($filter_value));
        }

        if ($filter_style === 'darken') {
            $image->filter(new Darken($filter_value));
        }

        $background_color = Arr::get($preset, 5);

        $image->filter(new Resize(
            $width,
            $height,
            $crop_style,
            $background_color,
            $this->focus_x,
            $this->focus_y
        ));

        return $image;
    }
}
