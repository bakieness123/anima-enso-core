<?php

namespace Yadda\Enso\Media\Traits;

use Yadda\Enso\Media\Contracts\MediaFile as MediaFileContract;

trait HasFilesTrait
{
    /**
     * Files that have been attached to this model.
     *
     * This should not be altered manually but should be automatically updated
     * by Enso CRUD.
     *
     * @return Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function files()
    {
        return $this
            ->morphToMany(resolve(MediaFileContract::class), 'model', 'file_model', 'model_id', 'file_id')
            ->withTimestamps();
    }

    /**
     * Boot the HasFiles trait
     *
     * @return void
     */
    public static function bootHasFiles()
    {
        static::deleting(function (HasMedia $model) {
            $model->detachAllFiles();
        });
    }

    /**
     * Detach all files from this model. This only remvoves files from the
     * "files" relationship not any other custom relationships that may exist
     * on the model so that they can be repopulated when updating from CRUD.
     *
     * You probaby don't want to call this method manually from outside Enso Core
     *
     * @return void
     */
    public function detachAllFiles()
    {
        $this->files()->detach();
    }
}
