<?php

namespace Yadda\Enso\Media\Controllers;

use EnsoMedia;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;
use Symfony\Component\HttpFoundation\File\File;
use Yadda\Enso\Media\Contracts\UploadsController as UploadsControllerContract;

class UploadsController extends Controller implements UploadsControllerContract
{
    /**
     * Upload a file the normal POST way
     *
     * @param \Illuminate\Http\Request $request Request object
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadFile(Request $request): JsonResponse
    {
        $uploaded_file = $request->file('file');
        $upload_type = $request->get('upload_type');
        $path = $request->get('path', 'unsorted');

        return EnsoMedia::uploadFile($uploaded_file, $upload_type, $path);
    }

    /**
     * Handle chunked upload POST requests
     *
     * @param \Illuminate\Http\Request                        $request
     * @param \Pion\Laravel\ChunkUpload\Receiver\FileReceiver $receiver
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadFileResumablePost(Request $request, FileReceiver $receiver): JsonResponse
    {
        if (!$receiver->isUploaded()) {
            throw new UploadMissingFileException();
        }

        $save = $receiver->receive();

        // Check if the upload has finished (in chunk mode it will send smaller files)
        if ($save->isFinished()) {
            $file = $save->getFile();
            $filename = $request->input('resumableFilename');
            $path = $request->input('path');
            return $this->saveFile($file, $filename, $path);
        }

        /** @var AbstractHandler $handler */
        $handler = $save->handler();

        // A chunk was received, send progress status
        return response()->json(['done' => $handler->getPercentageDone()]);
    }

    /**
     * Handle chunked upload GET requests
     *
     * This will be called by Resumable.js which sends a GET request to check if
     * a chunk has already been uploaded and, if it hasn't (i.e. if this
     * route returns a status > 400), then sends a POST request with the actual
     * data. This technique is currently not implemented in Enso.
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadFileResumableGet(): JsonResponse
    {
        return response()->json(
            ['status' => 'no file',],
            Response::HTTP_INTERNAL_SERVER_ERROR
        );
    }

    /**
     * Saves the file
     *
     * @param \Symfony\Component\HttpFoundation\File\File $file     File to save
     * @param string                                      $filename Name to use for file
     * @param string                                      $path     Relative path within storage
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function saveFile(File $file, string $filename, string $path): JsonResponse
    {
        ini_set('memory_limit', config('enso.media.memory_limit'));

        $mime = $file->getMimeType();
        $type = EnsoMedia::typeFromMime($mime);
        $file = $file->move(
            $file->getpath(),
            $filename . '.' . $file->getClientOriginalExtension()
        );
        $file = EnsoMedia::storeFile($file, $type, $path, $filename);

        if (is_null($file)) {
            return response()->json(
                'There was a problem with that file.',
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        } else {
            return response()->json(
                [
                    'status' => 'success',
                    'data' => $file->toArray(),
                    'filename' => $file->filename,
                    'filesize' => $file->filesize,
                ],
                Response::HTTP_OK
            );
        }
    }
}
