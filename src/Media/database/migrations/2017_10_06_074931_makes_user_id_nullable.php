<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakesUserIdNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('files', function (Blueprint $table) {
            $table->integer('temp_user_id')->nullable();
        });

        DB::table('files')->update(['temp_user_id' => DB::raw('user_id')]);

        Schema::table('files', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });

        Schema::table('files', function (Blueprint $table) {
            $table->integer('user_id')->after('id')->nullable()->index();
        });

        DB::table('files')->update([
            'user_id' => DB::raw('CASE WHEN temp_user_id = 0 THEN NULL ELSE temp_user_id END')
        ]);

        Schema::table('files', function (Blueprint $table) {
            $table->dropColumn('temp_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('files', function (Blueprint $table) {
            $table->integer('temp_user_id')->nullable();
        });

        DB::table('files')->update(['temp_user_id' => DB::raw('user_id')]);

        Schema::table('files', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });

        Schema::table('files', function (Blueprint $table) {
            $table->integer('user_id')->nullable();
        });

        DB::table('files')->update([
            'user_id' => DB::raw('CASE WHEN temp_user_id IS NULL THEN 0 ELSE temp_user_id END')
        ]);

        Schema::table('files', function (Blueprint $table) {
            $table->dropColumn('temp_user_id');
        });
    }
}
