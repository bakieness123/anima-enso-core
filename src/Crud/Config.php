<?php

namespace Yadda\Enso\Crud;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config as ConfigFacade;
use Illuminate\Support\Str;
use TorMorten\Eventy\Facades\Eventy;
use Yadda\Enso\Crud\Contracts\CrudFilterContract;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Traits\ConfiguresNestedModels;
use Yadda\Enso\Crud\Traits\HasCrudName;
use Yadda\Enso\Crud\Traits\HasIndexActions;
use Yadda\Enso\Crud\Traits\HasValidationRules;
use Yadda\Enso\Utilities\Traits\HasAutonomousTraits;

/**
 * Holds configuration for a CRUD section
 */
abstract class Config
{
    use ConfiguresNestedModels,
        HasAutonomousTraits,
        HasCrudName,
        HasIndexActions,
        HasValidationRules;

    /**
     * Whether/how to automatically update Unique validation rules.
     *
     * @var array|bool
     */
    protected $auto_unique_rules = true;

    /**
     * Model for this crud item
     *
     * @var string
     */
    private $model;

    /**
     * Directory that views are stored in. E.g. 'admin.model'
     *
     * @var string
     */
    protected $views_dir = '';

    /**
     * The name of the model handled by this controller.
     *
     * @var string
     */
    protected $name_singular = 'Base Model';

    /**
     * The plural name of the model handled by this controller.
     *
     * If this isn't set in configure() using setNamePlural it will
     * automatically be generated from name_singular.
     *
     * @var string
     */
    protected $name_plural = null;

    /**
     * The base route for this controller.
     *
     * @var string
     */
    protected $route = 'admin.base';

    /**
     * String to prefix all classes discovered by the RouteNameHelper with
     *
     * @var string
     */
    protected $route_class_prefix = 'route-';

    /**
     * Set this to true to enable pagination on index pages.
     *
     * @var bool
     */
    protected $paginate = true;

    /**
     * number of items to show per page
     *
     * @var integer
     */
    protected $per_page = 20;

    /**
     * column to order index by
     *
     * @var string
     */
    protected $orderby = 'id';

    /**
     * order of sorting - ASC or DESC
     *
     * @var string
     */
    protected $order = 'desc';

    /**
     * classes to be added to the body
     *
     * @var array
     */
    protected $body_classes = [];

    /**
     * index page table columns.
     * to display a column without escaping it (e.g. an model-generated
     * image tag), wrap the value in an array
     *
     * @var array
     */
    protected $table_columns = [
        'ID'   => 'id',
        'Name' => 'name',
        'Slug' => 'slug',
    ];

    /**
     * Set to true to enable file uploading in create/edit forms
     *
     * @var bool
     */
    protected $has_files = false;

    /**
     * Array of Relationship names to automatically load on the index
     * view for this controller. Will apply a 'with('relationship')'
     * query modifier to the index query.
     *
     * @var array
     */
    protected $preload_relationships = [];

    /**
     * Separator between entities in the route name. Laravel defaults to
     * '.' and should mostly be left as such, but can be overridden.
     *
     * @var string
     */
    protected $route_name_separator = '.';

    /**
     * Basic validation rules. You may want to alter this in custom
     * doUpdate or doStore methods if they aren't the same.
     *
     * @var array
     */
    protected $rules = [];

    /**
     * Custom validation error messages
     *
     * @var array
     */
    protected $messages = [];

    /**
     * Column to use when ordering items
     *
     * @var string
     */
    protected $order_column;

    /**
     * Custom query scopes for index ordering
     *
     * If an index query's 'orderby' value is a key in this array, use
     * the corresponding value as a query scope. This allows you to
     * e.g. join other tables to allow ordering by relationship values
     *
     * @var array
     */
    protected $order_query_scopes = [];

    /**
     * Columns to use when searching for an item.
     *
     * Used for relationship field lookups.
     *
     * @var array
     */
    protected $search_columns = ['name'];

    /**
     * Instance of the crud form
     *
     * @var \Yadda\Enso\Crud\Forms\Form
     */
    protected $form_instance;

    /**
     * Whether or not the crud type should be searchable on the index page
     *
     * @var bool
     */
    protected $searchable = false;

    protected $search_joins_callback;

    /**
     * Query scope callback for applying joins
     */
    protected $joins_callback;

    /**
     * Show interface bits for addings translations
     *
     * @var bool
     */
    protected $use_translations = false;

    /**
     * Routes to redirect to after the given actions
     *
     * @var array
     */
    protected $redirect_routes = [
        'store'   => 'index',
        'update'  => 'index',
        'destroy' => 'index',
    ];

    /**
     * Either the name of a function to call per-item or a callable that accepts the item
     * to determine a list of classes to add to the row.
     *
     * @var null|string|callable
     */
    protected $row_classes_callback = null;

    /**
     * Actions that can be committed against a selection of rows in an index table.
     *
     * Structure:
     *
     * [
     *   'actions' => [
     *     'name' => [ action ]...
     *   ],
     *   'selections' => [
     *     'name' => [ selection ]...
     *   ]
     * ]
     *
     * @var array
     */
    protected $bulk_actions = [
        'actions' => [],
        'selections' => [],
    ];

    /**
     * Filters that can be committed against a selection of rows in an index table.
     *
     * Structure:
     *
     * [
     *   'name' => [ filter ],
     * ]
     *
     * @var array
     */
    protected $item_filters = [];

    /**
     * Actions to display against items on the index page
     *
     * @var array
     */
    protected $index_actions = [];

    /**
     * Route to use when generating slugs
     *
     * @var string|null
     */
    protected $slug_route = null;

    /**
     * Create a new Config
     */
    public function __construct()
    {
        $this->bootIfNotBooted();

        $this->addDefaultIndexActions();

        $this->configure();

        $this->setupRedirectRoutes();

        if (is_null($this->name_plural)) {
            $this->name_plural = Str::plural($this->name_singular);
        }

        $this->initializeTraits();
    }

    /**
     * Defines configuration for this CRUD item
     *
     * @return void
     */
    abstract public function configure();

    /**
     * Default form configuration.
     *
     * @return \Yadda\Enso\Crud\Forms\Form
     */
    abstract public function create(Form $form);

    /**
     * Return config for an edit form.
     * Returns the result of create() by default.
     *
     * @return \Yadda\Enso\Crud\Forms\Form
     */
    public function edit(Form $form)
    {
        return $this->create($form);
    }

    /**
     * Whether to allow cloning for this Crud. Accepts the model instance in
     * case the implementation requires access to item specific logic
     *
     * @param Model $item
     *
     * @return bool
     */
    public function allowCloning(Model $item = null): bool
    {
        return ConfigFacade::get('enso.crud.' . $this->getCrudName() . '.enable_cloning', false);
    }

    /**
     * Get the model for this crud item
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set the model for this crud item
     *
     * @param string model
     *
     * @return self
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Helper method for getting/setting model name
     *
     * @param null|string $model
     *
     * @return self|string
     */
    public function model($model = null)
    {
        if (is_null($model)) {
            return $this->getModel();
        } else {
            return $this->setModel($model);
        }
    }

    /**
     * Creates a new instance of this Config's model
     *
     * @return \Illuminate\Database\Eloquent\Model instantiated model
     */
    public function newModelInstance()
    {
        $model = $this->getModel();

        return new $model;
    }

    /**
     * Get the directory that views are stored in. E.g. 'admin.model'. If the
     * setting is empty, it will attempt to generate a sensible default from
     * the plural name of this config.
     *
     * @return string
     */
    public function getViewsDir()
    {
        if (empty($this->views_dir)) {
            return strtolower($this->getNamePlural());
        }

        return $this->views_dir;
    }

    /**
     * Set the directory that views are stored in. E.g. 'admin.model'.
     * Not setting this will attempt to generate a sensible default from
     * the plural name of this config.
     *
     * @param string views_dir
     *
     * @return self
     */
    public function setViewsDir($views_dir)
    {
        $this->views_dir = $views_dir;

        return $this;
    }

    /**
     * Helper method for getting/setting views directory
     *
     * @param null|string $views_dir
     *
     * @return self|string
     */
    public function views($views_dir = null)
    {
        if (is_null($views_dir)) {
            return $this->getViewsDir();
        } else {
            return $this->setViewsDir($views_dir);
        }
    }

    /**
     * Checks to see if an override view exists in the views folder else
     * returns the default view based on the passed parameter.
     *
     * @param string $view_name View to get
     *
     * @return string Full view name
     */
    public function getCrudView($view_name)
    {
        if (view()->exists('enso-crud::' . $this->getViewsDir() . '.' . $view_name)) {
            return 'enso-crud::' . $this->getViewsDir() . '.' . $view_name;
        }

        return $this->getEnsoView($view_name);
    }

    /**
     * Gets the View template name to render for a specific view type
     *
     * This should be overridden in cases where you have non-standard
     * templates to render as part of your config that should stay as
     * part of enso and not be directly published, such as modules.
     *
     * @param string $view_name
     *
     * @return string
     */
    protected function getEnsoView($view_name)
    {
        return 'enso-crud::' . $view_name;
    }

    /**
     * Get the name of the model handled by this controller.
     *
     * @return string
     */
    public function getNameSingular()
    {
        return $this->name_singular;
    }

    /**
     * Helper method for setting/getting model name
     *
     * @param null|string $name_singular
     *
     * @return self|string
     */
    public function name($name_singular = null)
    {
        if (is_null($name_singular)) {
            return $this->getNameSingular();
        } else {
            $this->setNamePlural(Str::plural($name_singular));
            return $this->setNameSingular($name_singular);
        }
    }

    /**
     * Set the name of the model handled by this controller.
     *
     * @param string name_singular
     *
     * @return self
     */
    public function setNameSingular($name_singular)
    {
        $this->name_singular = $name_singular;

        return $this;
    }

    /**
     * Get the plural name of the model handled by this controller.
     *
     * @return string
     */
    public function getNamePlural()
    {
        return $this->name_plural;
    }

    /**
     * Set the plural name of the model handled by this controller.
     *
     * @param string name_plural
     *
     * @return self
     */
    public function setNamePlural($name_plural)
    {
        $this->name_plural = $name_plural;

        return $this;
    }

    /**
     * Helper method for setting/getting the route
     *
     * @param null|string $route
     *
     * @return self|string
     */
    public function route($route = null)
    {
        if (is_null($route)) {
            return $this->getRoute();
        } else {
            return $this->setRoute($route);
        }
    }

    /**
     * Get the base route for this controller.
     *
     * @param string $action Action to append to url, e.g. edit
     *
     * @return string
     */
    public function getRoute($action = null)
    {
        if ($action) {
            return $this->route . '.' . $action;
        }

        return $this->route;
    }

    /**
     * Set the base route for this controller.
     *
     * @param string route
     *
     * @return self
     */
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get or set the route to use for slugs
     *
     * This route should contain the string "%SLUG%"
     *
     * @param string|null $route
     * @return string|null
     */
    public function slugRoute($route = null)
    {
        if ($route) {
            $this->slug_route = $route;

            return $this;
        }

        if ($this->slug_route) {
            return $this->slug_route;
        }

        return url(Str::slug($this->name_plural) . '/%SLUG%');
    }

    /**
     * Get the string to prefix all classes discovered by the RouteNameHelper with
     *
     * @return string
     */
    public function getRouteClassPrefix()
    {
        return $this->route_class_prefix;
    }

    /**
     * Set the string to prefix all classes discovered by the RouteNameHelper with
     *
     * @param string route_class_prefix
     *
     * @return self
     */
    public function setRouteClassPrefix($route_class_prefix)
    {
        $this->route_class_prefix = $route_class_prefix;

        return $this;
    }

    /**
     * Helper method for setting up pagination
     *
     * @param bool|int $paginate
     * @param int|null $per_page
     *
     * @return self
     */
    public function paginate($opt = true)
    {
        if (is_bool($opt)) {
            return $this->setPaginate($opt);
        }

        if (!is_int($opt)) {
            throw new Exception('paginate only accepts a boolean or an integer. ' . gettype($opt) . ' passed.');
        }

        $this->setPaginate(true);
        $this->setPerPage($opt);

        return $this;
    }

    /**
     * Get pagination enable/disabled boolean
     *
     * @return bool
     */
    public function getPaginate()
    {
        return $this->paginate;
    }

    /**
     * Set pagination to enabled/disabled
     *
     * @param bool paginate
     *
     * @return self
     */
    public function setPaginate($paginate)
    {
        $this->paginate = $paginate;

        return $this;
    }

    /**
     * Get the number of items to show per page
     *
     * @return int
     */
    public function getPerPage()
    {
        return $this->per_page;
    }

    /**
     * Set the number of items to show per page
     *
     * @param int per_page
     *
     * @return self
     */
    public function setPerPage($per_page)
    {
        $this->per_page = $per_page;

        return $this;
    }

    /**
     * Get the default column to order index table by
     *
     * @return string
     */
    public function getOrderBy()
    {
        return $this->orderby;
    }

    /**
     * Set the default column to order index table by
     *
     * @param string $orderby
     *
     * @return self
     */
    public function setOrderBy($orderby)
    {
        $this->orderby = $orderby;

        return $this;
    }

    /**
     * Get the default ordering direction of the index table - ASC or DESC
     *
     * @return string
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set the default ordering direction of the index table - ASC or DESC
     *
     * @param string order
     *
     * @return self
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Helper method for setting order settings
     *
     * @param string $orderby
     * @param string $order
     *
     * @return self
     */
    public function order($orderby, $order)
    {
        $this->setOrderBy($orderby);
        $this->setOrder($order);

        return $this;
    }

    /**
     * Helper method for setting/getting body classes
     *
     * @param null|string|array $c
     *
     * @return self|array
     */
    public function bodyClasses($c = null)
    {
        if (is_null($c)) {
            return $this->getBodyClasses();
        }

        return $this->addBodyClass($c);
    }

    /**
     * Get the classes to be added to the body
     *
     * @return array
     */
    public function getBodyClasses()
    {
        return $this->body_classes;
    }

    /**
     * Set the classes to be added to the body
     *
     * @param array body_classes
     *
     * @return self
     */
    public function setBodyClasses(array $body_classes)
    {
        $this->body_classes = $body_classes;

        return $this;
    }

    /**
     * Add a class to the list of body classes
     *
     * @param string|array $body_class the class to add
     */
    public function addBodyClass($body_class)
    {
        foreach ((array) $body_class as $class) {
            $this->body_classes[] = $class;
        }

        $this->body_classes = array_unique($this->body_classes);

        return $this;
    }

    /**
     * Remove a class from the list of body classes if it exists
     *
     * @param  string $body_class the class to remove
     *
     * @return self
     */
    public function removeBodyClass(string $body_class)
    {
        // @todo Removing classes that don't exist will cause an error
        unset($this->body_classes[array_search($body_class, $this->body_classes)]);

        return $this;
    }

    /**
     * Get the index page table columns.
     *
     * @return array
     */
    public function getTableColumns()
    {
        return $this->table_columns;
    }

    /**
     * Set the index page table columns.
     *
     * @param array $table_columns
     *
     * @return self
     */
    public function setTableColumns(array $table_columns)
    {
        $this->table_columns = [];

        foreach ($table_columns as $col) {
            $this->setTableColumn($col, $this);
        }

        return $this;
    }

    /**
     * Set a single page column
     *
     * @param \Yadda\Enso\Crud\Tables\ColumnInterface
     */
    public function setTableColumn($col, $config = null)
    {
        $this->table_columns[$col->getName()] = $col->setConfig($config);

        return $this;
    }

    public function removeTableColumn(string $heading)
    {
        unset($this->table_columns[$heading]);

        return $this;
    }

    public function columns($cols = null)
    {
        if (is_null($cols)) {
            return $this->getTableColumns();
        }

        // @todo this could be refactored to be more similar to the bodyClasses methods
        return $this->setTableColumns($cols);
    }

    /**
     * Get the has_files setting
     *
     * @return bool
     */
    public function getHasFiles()
    {
        return $this->has_files;
    }

    /**
     * Set the file uploading as enabled/disabled
     *
     * @param bool $has_files
     *
     * @return self
     */
    public function setHasFiles($has_files = true)
    {
        $this->has_files = $has_files;

        return $this;
    }

    /**
     * Get the array of Relationship names to automatically load on the index
     *
     * @return array
     */
    public function getPreloadRelationships()
    {
        return $this->preload_relationships;
    }

    /**
     * Set the array of Relationship names to automatically load on the index
     *
     * @param array preload_relationships
     *
     * @return self
     */
    public function setPreloadRelationships(array $preload_relationships)
    {
        $this->preload_relationships = $preload_relationships;

        return $this;
    }

    /**
     * Get the separator between entities in the route name.
     * Laravel defaults to '.'
     *
     * @return string
     */
    public function getRouteNameSeparator()
    {
        return $this->route_name_separator;
    }

    /**
     * Set the separator between entities in the route name.
     * Laravel defaults to '.'
     *
     * @param string route_name_separator
     *
     * @return self
     */
    public function setRouteNameSeparator($route_name_separator)
    {
        $this->route_name_separator = $route_name_separator;

        return $this;
    }

    /**
     * Alias for setUseTranslations
     *
     * @param bool $use_translations
     *
     * @return self
     */
    public function useTranslations($use_translations = true)
    {
        return $this->setUseTranslations($use_translations);
    }

    /**
     * Set whether or not to show translation interface items
     *
     * @param bool $use_translations
     *
     * @return void
     */
    public function setUseTranslations($use_translations = true)
    {
        $this->use_translations = $use_translations;

        return $this;
    }

    public function getUseTranslations()
    {
        return $this->use_translations;
    }

    /**
     * Get a list of available languages.
     *
     * @todo this is duplicated in Field.php
     *
     * @return array
     */
    public function getLanguages()
    {
        return config('laravellocalization.supportedLocales', []);
    }

    /**
     * Get a list of translatable field names
     *
     * @return array
     */
    public function getTranslatables()
    {
        if (!$this->getUseTranslations()) {
            return [];
        }

        $model_name = $this->getModel();
        $model = new $model_name;
        return $model->getTranslatableAttributes();
    }

    /**
     * Return config in array form for later conversion to JSON
     *
     * @return array
     */
    public function getJsConfig()
    {
        $config = [
            'name_singular'    => $this->getNameSingular(),
            'name_plural'      => $this->getNamePlural(),
            'orderby'          => $this->getOrderBy(),
            'order'            => $this->getOrder(),
            'order_column'     => $this->getOrderable(),
            'route'            => route($this->getRoute() . '.index'),
            'paginate'         => $this->getPaginate(),
            'per_page'         => $this->getPerPage(),
            'columns'          => [],
            'searchable'       => $this->getSearchable(),
            'use_translations' => $this->getUseTranslations(),
            'languages'        => $this->getLanguages(),
            'translatables'    => $this->getTranslatables(),
            'index_actions'    => $this->getFinalIndexActions(),
            'bulk_actions'     => $this->getBulkActions(),
            'item_filters'     => array_filter(
                array_map(function ($filter) {
                    if (is_object($filter) && $filter instanceof CrudFilterContract) {
                        return $filter->toArray();
                    } else {
                        return $filter;
                    }
                }, $this->getItemFilters())
            ),
            'nested'           => $this->isNested(),
            'max_depth'        => $this->maxDepth(),
        ];

        foreach ($this->getTableColumns() as $col) {
            $config['columns'][] = $col->getJsConfig();
        }

        $config = Eventy::filter('enso.' . $this->getCrudName() . '.config.js-config', $config);

        return $config;
    }

    /**
     * Get a configuration array for passing to the create form as JS
     *
     * @return array
     */
    public function getJsCreateForm()
    {
        return $this->getCreateForm()->toArray();
    }

    /**
     * Get a configuration array for passing to the edit form as JS
     *
     * @param bool $force_reset If true the form will be recreated
     *
     * @return array
     */
    public function getJsEditForm($force_reset = false)
    {
        return $this->getEditForm($force_reset)->toArray();
    }

    /**
     * Get an list of column names
     *
     * @return array
     */
    public function getColumnNames()
    {
        $cols = [];

        foreach ($this->getTableColumns() as $col) {
            $cols[] = $col->getName();
        }

        return $cols;
    }

    /**
     * Prepends the routes in redirect_routes with the base route. You might
     * want to override this function in your own controller.
     *
     * @return void
     */
    protected function setupRedirectRoutes()
    {
        $this->redirect_routes = [
            'store'   => $this->route . $this->route_name_separator . $this->redirect_routes['store'],
            'update'  => $this->route . $this->route_name_separator . $this->redirect_routes['update'],
            'destroy' => $this->route . $this->route_name_separator . $this->redirect_routes['destroy'],
        ];
    }

    /**
     * Get a redirect for after agiven action
     *
     * @param  atring $action The action after which you are redirecting
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getRedirect($action)
    {
        return redirect()->route($this->redirect_routes[$action]);
    }

    /**
     * Get the name of the current route method.
     *
     * @param \Illuminate\Http\Request The current request
     *
     * @return string E.g. index, create, update, etc.
     */
    protected function getMethod()
    {
        $route = request()->route();

        if (is_null($route)) {
            $method = 'non-crud-route';
        } else {
            list($class, $method) = explode('@', $route->getActionName());
        }

        return $method;
    }

    /**
     * Helper method for setting/getting validation messages
     *
     * @param null|array $messages
     *
     * @return self|array
     */
    public function messages($messages = null)
    {
        if (is_null($messages)) {
            return $this->getMessages();
        } else {
            return $this->setMessages($messages);
        }
    }

    /**
     * Set custom validation messages
     *
     * @param $messages Array
     *
     * @return self
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;

        return $this;
    }

    /**
     * Get custom validation messages
     *
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Get the form for the create page
     *
     * @return \Yadda\Enso\Crud\Forms\Form
     */
    public function getCreateForm($force_reset = false)
    {
        if (is_null($this->form_instance) || $force_reset) {
            $this->form_instance = $this->create(new Form());
            $this->form_instance->setConfig($this);
        }

        $this->form_instance->setAction('create');

        return $this->form_instance;
    }

    /**
     * Get the form for the edit page
     *
     * If the edit() method has not been overridden, the create() method
     * will be used instead.
     *
     * @return \Yadda\Enso\Crud\Forms\Form
     */
    public function getEditForm($force_reset = false)
    {
        if (is_null($this->form_instance) || $force_reset) {
            $this->form_instance = $this->edit(new Form());
            $this->form_instance->setConfig($this);
        }

        $this->form_instance->setAction('edit');

        return $this->form_instance;
    }

    /**
     * Helper method for setting/getting which column the data is ordered by
     * for the purposes of manual, drag-and-drop ordering.
     *
     * @param null|string $column
     *
     * @return self|string
     */
    public function orderable($column = null)
    {
        if (is_null($column)) {
            return $this->getOrderable();
        } else {
            return $this->setOrderable($column);
        }
    }

    /**
     * Sets the name of the column which will be used for the purposes of manual
     * item reordering via the crud index. This should therefore be a numeric
     * column only.
     *
     * @param string $column
     *
     * @return self
     */
    public function setOrderable($column = 'order')
    {
        $this->order_column = $column;

        return $this;
    }

    /**
     * Gets the currently selected column that is being used for manual item
     * reordering, or null if manual ordering has not been assigned.
     *
     * @return string
     */
    public function getOrderable()
    {
        if ($this->isNested()) {
            return '_lft';
        }

        return $this->order_column;
    }

    /**
     * Performs a check to see whether this CRUD item should provide manual
     * reordering via it's CRUD index page
     *
     * @return bool
     */
    public function isOrderable()
    {
        if ($this->isNested()) {
            return true;
        }

        return !is_null($this->order_column);
    }

    /**
     * Helper method for setting/getting search columns
     *
     * @param null|array $columns
     *
     * @return self|array
     */
    public function searchColumns($columns = null)
    {
        if (is_null($columns)) {
            return $this->getSearchColumns();
        } else {
            return $this->setSearchColumns($columns);
        }
    }

    /**
     * Set search columns
     *
     * @param array $columns
     */
    public function setSearchColumns($columns)
    {
        $this->search_columns = $columns;

        return $this;
    }

    /**
     * Get search columns
     *
     * @return array
     */
    public function getSearchColumns()
    {
        return $this->search_columns;
    }

    public function searchable($opt = null)
    {
        if (is_null($opt)) {
            return $this->getSearchable();
        } elseif (is_bool($opt)) {
            return $this->setSearchable($opt);
        } elseif (is_array($opt)) {
            $this->setSearchable(true);
            return $this->setSearchColumns($opt);
        } else {
            throw new Exception('Config::searchable takes a bool, a string or an array.');
        }
    }

    /**
     * Set whether the crud type should be searchable on the index page
     *
     * @deprecated v0.2.7
     *
     * @param boolean $searchable
     *
     * @return self
     */
    public function setSearchable($searchable)
    {
        $this->searchable = $searchable;

        return $this;
    }

    /**
     * Whether the crud type should be searchable on the index page
     *
     * @deprecated v0.2.7
     *
     * @return boolean
     */
    public function getSearchable()
    {
        return $this->searchable;
    }

    /**
     * Set a query scope callback function to load joins when searching
     *
     * @param callable $callback
     *
     * @return self|string
     */
    public function setSearchJoinsCallback(callable $callback)
    {
        $this->search_joins_callback = $callback;

        return $this;
    }

    /**
     * Get the query scope callback for loading joins when searching
     */
    public function getSearchJoinsCallback()
    {
        return $this->search_joins_callback;
    }

    /**
     * Set a query scope callback to load joins.
     *
     * This is a temporary fix to solve the problem of searching AND ordering
     * at the same time. If we find a better way of doing it, this may go away.
     *
     * @param callable $callback
     *
     * @return self
     */
    public function setJoinsCallback(callable $callback)
    {
        $this->joins_callback = $callback;

        return $this;
    }

    /**
     * Get the query scope callback for loading joins
     *
     * @return callable
     */
    public function getJoinsCallback()
    {
        return $this->joins_callback;
    }

    /**
     * Get a list of all the relationship names on this model
     *
     * @param string $type Current action name, e.g. create, edit
     *
     * @return array
     */
    public function getRelationshipNames($type = 'create')
    {
        switch ($type) {
            case 'create':
                $form = $this->getCreateForm();
                break;
            case 'edit':
                $form = $this->getEditForm();
                break;
            default:
                break;
        }

        $relation_names = [];

        foreach ($form->getSections() as $section) {
            $relation_names = array_merge($relation_names, $section->getRelationNames());
        }

        return $relation_names;
    }

    /**
     * Set the custom order query scopes.
     *
     * @param array $scopes An associative array of callables
     *
     * @return void
     */
    public function setOrderColumnScopes($scopes)
    {
        $this->order_query_scopes = $scopes;

        return $this;
    }

    /**
     * Get the custom order query scopes
     *
     * @return array
     */
    public function getOrderColumnScopes()
    {
        return $this->order_query_scopes;
    }

    /**
     * Set whether this Crud type can be deleted
     *
     * @param bool $is_deletable
     *
     * @return self
     */
    public function setDeletable($is_deletable = true)
    {
        /**
         * This really shouldn't happen, but just in case the delete action gets removed
         * and then added back in again, here is the original index button.
         */
        if ($is_deletable && !$this->hasIndexAction('delete')) {
            $this->addDeleteIndexAction();
        }

        // More typically, when not deleteable, remove the index action if present.
        if (!$is_deletable && $this->hasIndexAction('delete')) {
            $this->removeIndexAction('delete');
        }

        return $this;
    }

    /**
     * Get whether this Crud type can be deleted
     *
     * @return bool
     */
    public function getDeletable()
    {
        return $this->hasIndexAction('delete');
    }

    /**
     * Checks to see whether a Row classes callback has been defined,
     * for adding classes to Crud index rows on a per-item basis.
     *
     * @return bool
     */
    public function hasRowClassesCallback()
    {
        return !is_null($this->row_classes_callback);
    }

    /**
     * Get the Row classes callback from this Crud.
     *
     * @return string|callable
     */
    public function getRowClassesCallback()
    {
        return $this->row_classes_callback;
    }

    /**
     * Set the Row classes callback from this Crud. This should be either an
     * callable that accepts the item as an argument, or the name of a function
     * to call ON the item.
     *
     * @param string|callable
     *
     * @return self
     */
    public function setRowClassesCallback($row_classes_callback)
    {
        $this->row_classes_callback = $row_classes_callback;

        return $this;
    }

    /**
     * Boolean check to see whether and bulk actions are available for this item
     *
     * @return bool
     */
    public function hasBulkActions()
    {
        return (bool) count($this->bulk_actions['actions']);
    }

    /**
     * Returns the bulk actions list
     *
     * @return array
     */
    public function getBulkActions()
    {
        return $this->bulk_actions;
    }

    /**
     * Set the entire bulk_actions array.
     *
     * @param array $bulk_actions
     *
     * @return self
     */
    public function setBulkActions($bulk_actions)
    {
        $this->bulk_actions = $bulk_actions;

        return $this;
    }

    /**
     * Adds a named bulk action to the list of actions.
     *
     * @param string $name
     * @param array  $bulk_action
     *
     * @return self
     */
    public function addBulkAction($name, $bulk_action)
    {
        if (!empty($this->bulk_actions['actions'][$name])) {
            throw new Exception('Bulk Action named `' . $name . '` already exists');
        }

        $this->bulk_actions['actions'][$name] = $bulk_action;

        return $this;
    }

    /**
     * Defers to addBulkAction for each element of an array.
     *
     * @param array $bulk_actions
     *
     * @return self
     */
    public function addBulkActions($bulk_actions)
    {
        collect($bulk_actions)->each(function ($bulk_action, $key) {
            $this->addBulkAction($key, $bulk_action);
        });

        return $this;
    }

    /**
     * Removes a Bulk Action, by name
     *
     * @param string $name
     *
     * @return self
     */
    public function removeBulkAction($name)
    {
        if (key_exists($name, $this->bulk_actions['actions'])) {
            unset($this->bulk_actions['actions'][$name]);
        }

        return $this;
    }

    /**
     * Adds a selection type to the bulk_actions list.
     *
     * @param string $name
     * @param array  $selection
     *
     * @return self
     */
    public function addBulkActionSelection($name, $selection)
    {
        if (!empty($this->bulk_actions['selections'][$name])) {
            throw new Exception('Bulk Action selection named `' . $name . '` already exists');
        }

        $this->bulk_actions['selections'][$name] = $selection;

        return $this;
    }

    /**
     * Adds an array of selections to the bulk_actions list.
     *
     * @param array $selections
     *
     * @return self
     */
    public function addBulkActionSelections($selections)
    {
        collect($selections)
            ->each(function ($selection, $name) {
                $this->addBulkActionSelection($name, $selection);
            });

        return $this;
    }

    /**
     * Removes a selection from the bulk actions list, by name
     *
     * @param string $name
     *
     * @return self
     */
    public function removeBulkActionSelection($name)
    {
        if (key_exists($name, $this->bulk_actions['selections'])) {
            unset($this->bulk_actions['selections'][$name]);
        }

        return false;
    }

    /**
     * Helper method for setting/getting item filters
     *
     * @param null|array $filters
     *
     * @return self|array
     */
    public function filters($filters = null)
    {
        if (is_null($filters)) {
            return $this->getItemFilters();
        }

        return $this->setItemFilters($filters);
    }

    /**
     * Boolean check to see whether any item filters are available for this config
     *
     * @return bool
     */
    public function hasItemFilters()
    {
        return (bool) count($this->item_filters);
    }

    /**
     * Returns the item filters list
     *
     * @return array
     */
    public function getItemFilters()
    {
        return $this->item_filters;
    }

    /**
     * Set the entire item_filters array.
     *
     * @param array $item_filters
     *
     * @return self
     */
    public function setItemFilters($item_filters)
    {
        $this->item_filters = $item_filters;

        return $this;
    }

    /**
     * Gets a filter by it's name
     *
     * @param string $filter_name
     *
     * @return array
     */
    public function getFilterByName($filter_name)
    {
        if (array_key_exists($filter_name, $this->item_filters)) {
            return $this->item_filters[$filter_name];
        }

        return null;
    }

    /**
     * Adds a named bulk action to the list of actions.
     *
     * @param string $name
     * @param array  $item_filters
     *
     * @return self
     */
    public function addItemFilter($name, $item_filter)
    {
        if (!empty($this->item_filters[$name])) {
            throw new Exception('Filter named `' . $name . '` already exists');
        }

        $this->item_filters[$name] = $item_filter;

        return $this;
    }

    /**
     * Defers to addBulkAction for each element of an array.
     *
     * @param array $bulk_actions
     *
     * @return self
     */
    public function addItemFilters($item_filters)
    {
        collect($item_filters)->each(function ($item_filter, $key) {
            $this->addItemFilter($key, $item_filter);
        });

        return $this;
    }

    /**
     * Removes a selection from the bulk actions list, by name
     *
     * @param string $name
     *
     * @return self
     */
    public function removeItemFilter($name)
    {
        if (key_exists($name, $this->item_filters)) {
            unset($this->item_filters[$name]);
        }

        return false;
    }
}
