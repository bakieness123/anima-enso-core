<?php

namespace Yadda\Enso\Crud\Contracts;

/**
 * Interface to define Sections that Implement an relationship, instead of
 * Sections that save to the Model itself.
 */
interface RelationshipSection
{
    //
}
