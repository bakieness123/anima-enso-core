<?php

namespace Yadda\Enso\Crud\Contracts\Config;

use Illuminate\Database\Eloquent\Builder;

interface IsPublishable
{
    /**
     * Modifes a JS Config data array
     *
     * @param array $config_data
     *
     * @return array
     */
    public static function isPublishableModifyJsConfig(array $config_data): array;
}
