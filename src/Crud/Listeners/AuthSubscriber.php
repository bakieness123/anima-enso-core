<?php

namespace Yadda\Enso\Crud\Listeners;

use Illuminate\Auth\Events\Attempting;
use Illuminate\Auth\Events\Authenticated;
use Illuminate\Auth\Events\Failed;
use Illuminate\Auth\Events\Lockout;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Logout;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class AuthSubscriber
{
    private $monolog;

    /**
     * Creates a new Logger instance so that we can log to a different file.
     */
    public function __construct()
    {
        $file_path = storage_path(Config::get('enso.events.auth.log_file', 'logs/auth.log'));

        $this->monolog = new Logger('auth');

        if (Config::get('enso.events.auth.use_daily_files', false)) {
            $files_to_keep = Config::get('enso.events.auth.files_to_keep', 0);
            $handler = new RotatingFileHandler($file_path, $files_to_keep);
        } else {
            $handler = new StreamHandler($file_path);
        }

        $this->monolog->setHandlers([$handler]);
    }

    /**
     * Handle user login events.
     */
    public function onUserAttempting($event)
    {
        $ip = $this->getClientOriginalIp();
        $event_name = 'Login Attempt';
        $content = serialize(Arr::except($event->credentials, 'password'));

        $message = $ip . ' - ' . $event_name . ': ' . $content;

        $this->monolog->addRecord(Logger::INFO, $message);
    }

    /**
     * Handle user login events.
     */
    public function onUserAuthenticated($event)
    {
        $ip = $this->getClientOriginalIp();
        $event_name = 'Authenticated';
        $user = $event->user;

        $message = "{$ip} - {$event_name}: `{$user->getAuthIdentifier()}`";

        $this->monolog->addRecord(Logger::INFO, $message);
    }

    /**
     * Handle user login events.
     */
    public function onUserFailed($event)
    {
        $ip = $this->getClientOriginalIp();
        $event_name = 'Login Failed';
        $content = serialize(Arr::except($event->credentials, 'password'));

        $message = $ip . ' - ' . $event_name . ': ' . $content;

        $this->monolog->addRecord(Logger::INFO, $message);
    }

    /**
     * Handle user login events.
     */
    public function onUserLockout($event)
    {
        $ip = $this->getClientOriginalIp();
        $event_name = 'Lockout Triggered';

        $message = $ip . ' - ' . $event_name;

        $this->monolog->addRecord(Logger::WARNING, $message);
    }

    /**
     * Handle user login events.
     */
    public function onUserLogin($event)
    {
        $ip = $this->getClientOriginalIp();
        $event_name = 'Login';
        $user = $event->user;

        $message = "{$ip} - {$event_name}: `{$user->getAuthIdentifier()}`";

        $this->monolog->addRecord(Logger::INFO, $message);
    }

    /**
     * Handle user logout events.
     */
    public function onUserLogout($event)
    {
        $ip = $this->getClientOriginalIp();
        $event_name = 'Logout';

        // Sometimes the user isn't set...
        $user_string = $event->user ? ": `{$event->user->getAuthIdentifier()}`" : '';

        $message = $ip . ' - ' . $event_name . $user_string;

        $this->monolog->addRecord(Logger::INFO, $message);
    }

    /**
     * Handle user logout events.
     */
    public function onUserRegistered($event)
    {
        $ip = $this->getClientOriginalIp();
        $event_name = 'Registered';
        $user = $event->user;

        $message = $ip . ' - ' . $event_name . ': ' . $user->getAuthIdentifier();

        $this->monolog->addRecord(Logger::INFO, $message);
    }

    /**
     * Gets the IP address that initiated there request. If it has a different
     * x-forwarded-for header, include that as well.
     *
     * @return string
     */
    protected function getClientOriginalIp()
    {
        if (Request::header('x-forwarded-for')
            && Request::ip() !== Request::header('x-forwarded-for')
        ) {
            return Request::ip() . '(x-forwarded-for ' . Request::header('x-forwarded-for') . ')';
        }

        return Request::ip();
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        if (Config::get('enso.events.auth.log_attempting', false)) {
            $events->listen(Attempting::class, AuthSubscriber::class . '@onUserAttempting');
        }

        if (Config::get('enso.events.auth.log_authenticated', false)) {
            $events->listen(Authenticated::class, AuthSubscriber::class . '@onUserAuthenticated');
        }

        if (Config::get('enso.events.auth.log_failed', false)) {
            $events->listen(Failed::class, AuthSubscriber::class . '@onUserFailed');
        }

        if (Config::get('enso.events.auth.log_lockout', false)) {
            $events->listen(Lockout::class, AuthSubscriber::class . '@onUserLockout');
        }

        if (Config::get('enso.events.auth.log_login', false)) {
            $events->listen(Login::class, AuthSubscriber::class . '@onUserLogin');
        }

        if (Config::get('enso.events.auth.log_logout', false)) {
            $events->listen(Logout::class, AuthSubscriber::class . '@onUserLogout');
        }

        if (Config::get('enso.events.auth.log_registered', false)) {
            $events->listen(Registered::class, AuthSubscriber::class . '@onUserRegistered');
        }
    }
}
