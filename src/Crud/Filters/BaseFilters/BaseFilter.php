<?php

namespace Yadda\Enso\Crud\Filters\BaseFilters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Yadda\Enso\Crud\Contracts\CrudFilterContract;
use Yadda\Enso\Crud\Exceptions\CrudException;
use Yadda\Enso\Users\Contracts\User;

abstract class BaseFilter implements CrudFilterContract
{
    /**
     * Access restrictions for this filter. If this filter is inaccessible, it
     * will not display in the UI, and will apply it's fallback filtering logic.
     *
     * @var callable|string|array
     */
    protected $access;

    /**
     * CSS Classes to apply to the filter. Classes are applied to a wrapper
     * element
     *
     * @var string
     */
    protected $class = 'is-4';

    /**
     * Columns to search in
     *
     * @var array
     */
    protected $columns = [
        'name'
    ];

    /**
     * Initial value to apply to the filter
     *
     * @var mixed
     */
    protected $default = null;

    /**
     * Label to apply to the filter
     *
     * @var string
     */
    protected $label = 'Filter';

    /**
     * Props to apply to the filter
     *
     * @var array
     */
    protected $props = [
        'placeholder' => 'Search...',
        'help-text' => 'Search for records containing the given text',
    ];

    /**
     * Name of a relationship to query
     *
     * Populate to search by relationship. Leave blank to search base query
     *
     * @var string
     */
    protected $relationship_name;

    /**
     * Settings to apply to the filter
     *
     * @var array
     */
    protected $settings = [];

    /**
     * Type of filter
     *
     * @var string
     */
    protected $type = "text";

    /**
     * Allows for dynamics setters and getters
     */
    public function __call($name, $arguments)
    {
        $fragment = substr($name, 0, 3);

        switch ($fragment) {
            case 'get':
                $property_name = Str::snake(substr($name, 3));

                if (property_exists($this, $property_name)) {
                    return $this->$property_name;
                }

                break;
            case 'set':
                $property_name = Str::snake(substr($name, 3));

                if (property_exists($this, $property_name)) {
                    $this->$property_name = Arr::first($arguments);
                }

                break;
            default:
                \Illuminate\Support\Facades\Log::error('Unknown Called property: ' . $name);
        }

        return $this;
    }

    /**
     * Gets or sets the CSS class string that will be used to style the filter
     *
     * @param callable|array|string|null $access
     *
     * @return self|callable|array|string
     */
    public function access($access = null)
    {
        if (is_null($access)) {
            return $this->getAccess();
        }

        if (is_callable($access) || is_array($access) || is_string($access)) {
            return $this->setAccess($access);
        }

        throw new CrudException('Invalid access type: ' . gettype($access));
    }

    /**
     * Apply value as a relationship modifier
     *
     * @param Builder $query
     * @param mixed   $value
     *
     * @return void
     */
    protected function applyAsRelationship(Builder $query, $value): void
    {
        $query->whereHas($this->relationship_name, function ($query) use ($value) {
            $this->applyQueryModifications($query, $value);
        });
    }

    /**
     * Filter query using the provided value.
     *
     * @param Builder $query
     * @param mixed   $value
     *
     * @return void
     */
    protected function applyAsValueQueryModification(Builder $query, $value): void
    {
        if ($this->relationship_name) {
            $this->applyAsRelationship($query, $value);
        } else {
            $this->applyQueryModifications($query, $value);
        }
    }

    /**
     * Applies an alternative query modification if the filter value is empty.
     * By default, this will mean no additional filtering.
     *
     * @param Builder $query
     *
     * @return void
     */
    protected function applyDefaultQueryModifications(Builder $query): void
    {
        // No filtering
    }

    /**
     * Apply columns to the given query as a self-container where statement
     *
     * @param Builder $query
     * @param mixed   $value
     *
     * @return void
     */
    protected function applyQueryModifications(Builder $query, $value): void
    {
        $query->where(function ($query) use ($value) {
            foreach ($this->columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $value . '%');
            }
        });
    }

    /**
     * Applies an alternative query modification if the User doesn't have access
     * to this filter.
     *
     * @param Builder $query
     *
     * @return void
     */
    protected function applyRestrictedQueryModifications(Builder $query): void
    {
        // No filtering
    }

    /**
     * Query modifier
     *
     * @param Builder $query
     * @param mixed   $value
     *
     * @return void
     */
    public function callable(Builder $query, $value): void
    {
        $value = $this->parseValue($value);

        if (count($this->columns) === 0) {
            throw new CrudException('Search Filter incorrectly set up: No columns specified for searching');
        }

        if (!$this->hasAccess()) {
            $this->applyRestrictedQueryModifications($query);
        } elseif ($this->valueIsEmpty($value)) {
            $this->applyDefaultQueryModifications($query);
        } else {
            $this->applyAsValueQueryModification($query, $value);
        }
    }

    /**
     * Gets or sets the CSS class string that will be used to style the filter
     *
     * @param string|null $class
     *
     * @return mixed
     */
    public function class(?string $class)
    {
        if (is_null($class)) {
            return $this->getClass();
        }

        return $this->setClass($class);
    }

    /**
     * Gets or sets the columns
     *
     * @param array|null $columns
     *
     * @return mixed
     */
    public function columns(?array $columns)
    {
        if (is_null($columns)) {
            return $this->getColumns();
        }

        return $this->setColumns($columns);
    }

    /**
     * Gets or sets the default
     *
     * @param mixed|null $default
     *
     * @return mixed
     */
    public function default($default = null)
    {
        if (is_null($default)) {
            return $this->getDefault();
        }

        return $this->setDefault($default);
    }

    /**
     * Gets the props property
     *
     * The ability to get Settings directly has been prevented to mirror the
     * functionality provided by fields. Settings should be set directly.
     *
     * @return array
     */
    public function getProps(): array
    {
        return Arr::except($this->props, 'settings');
    }

    /**
     * Determines whether a user (either given or currently authenticated) has
     * access to this Filter
     *
     * @param User|null $user
     *
     * @return bool
     */
    public function hasAccess(User $user = null): bool
    {
        $user = $user ?? Auth::user();

        if (empty($user)) {
            return false;
        }

        if (empty($this->getAccess())) {
            return true;
        } elseif (is_callable($this->getAccess())) {
            return call_user_func($this->getAccess(), $user);
        } elseif (is_array($this->getAccess())) {
            return $user->hasPermissions($this->getAccess());
        } elseif (is_string($this->getAccess())) {
            return $user->hasPermission($this->getAccess());
        }
    }

    /**
     * Gets or sets the label
     *
     * @param string|null $label
     *
     * @return mixed
     */
    public function label(?string $label)
    {
        if (is_null($label)) {
            return $this->getLabel();
        }

        return $this->setLabel($label);
    }

    /**
     * Return newly instantiated filter
     *
     * @return BaseFilter
     */
    public static function make(): BaseFilter
    {
        return new static(...func_get_args());
    }

    /**
     * Parses the value received by the filter
     *
     * @param mixed $value
     *
     * @return mixed
     */
    public function parseValue($value)
    {
        return $value;
    }

    /**
     * Gets or sets the props
     *
     * @param array|null $props
     *
     * @return mixed
     */
    public function props(?array $props)
    {
        if (is_null($props)) {
            return $this->getProps();
        }

        return $this->setProps($props);
    }

    /**
     * Gets or sets the relationship name
     *
     * @param string|null $relationship_name
     *
     * @return mixed
     */
    public function relationshipName(?string $relationship_name)
    {
        if (is_null($relationship_name)) {
            return $this->getRelationshipName();
        }

        return $this->setRelationshipName($relationship_name);
    }

    /**
     * Sets the props on this
     *
     * The ability to set Settings directly has been prevented to mirror the
     * functionality provided by fields. Settings should be set directly.
     *
     * @param array $props
     * @param bool  $merge
     *
     * @return self
     */
    public function setProps(array $props, bool $merge = false)
    {
        if (array_key_exists('settings', $props)) {
            throw new InvalidArgumentException(
                'Updating settings via props is deprecated. Use settings() '
                . 'instead'
            );
        }

        if ($merge) {
            $this->props = array_merge(
                $this->props,
                $props
            );
        } else {
            $this->props = $props;
        }

        return $this;
    }

    /**
     * Gets or sets the settings
     *
     * @param array|null $settings
     *
     * @return mixed
     */
    public function settings(?array $settings)
    {
        if (is_null($settings)) {
            return $this->getSettings();
        }

        return $this->setSettings($settings);
    }

    /**
     * Outputs this instance into a data array for the Crud.
     *
     * @return array
     */
    public function toArray(): array
    {
        if (!$this->hasAccess()) {
            return [];
        }

        return [
            'type' => $this->type,
            'label' => $this->label,
            'default' => $this->default,
            'callable' => [$this, 'callable'],
            'props' => array_merge(
                $this->props,
                [
                    'settings' => $this->settings,
                ]
            ),
            'classes' => $this->class,
        ];
    }

    /**
     * Gets or sets the type
     *
     * @param string|null $type
     *
     * @return mixed
     */
    public function type(?string $type)
    {
        if (is_null($type)) {
            return $this->getType();
        }

        return $this->setType($type);
    }

    /**
     * Whether the given value is considered empty for this filter
     *
     * @param mixed $value
     *
     * @return boolean
     */
    protected function valueIsEmpty($value = null): bool
    {
        return (
            is_null($value)
            || $value === ''
            || $value === []
        );
    }
}
