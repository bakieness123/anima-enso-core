<?php

namespace Yadda\Enso\Crud\Filters\BaseFilters;

use Yadda\Enso\Crud\Filters\BaseFilters\BaseFilter;

class TextFilter extends BaseFilter
{
    /**
     * Props to apply to the filter
     *
     * @var array
     */
    protected $props = [
        'placeholder' => 'Search...',
        'help-text' => 'Search for records containing the given text',
    ];

    /**
     * Type of filter
     *
     * @var string
     */
    protected $type = 'text';
}
