<?php

namespace Yadda\Enso\Crud\Exceptions;

use Exception;

class FieldIntegrityException extends Exception
{
    //
}
