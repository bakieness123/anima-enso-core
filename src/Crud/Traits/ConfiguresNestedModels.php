<?php

namespace Yadda\Enso\Crud\Traits;

trait ConfiguresNestedModels
{
    /**
     * Whether or not to use a nested set for the model
     */
    public function isNested(): bool
    {
        return !empty($this->nesting_enabled);
    }

    /**
     * Set whether or not to use a nested set
     */
    public function nested(bool $is_nested = true): self
    {
        $this->nesting_enabled = $is_nested;

        return $this;
    }

    /**
     * Get or set the max nesting depth
     */
    public function maxDepth(?int $depth = null)
    {
        if (is_null($depth)) {
            if (isset($this->max_depth)) {
                return $this->max_depth;
            } else {
                return -1;
            }
        }

        $this->max_depth = $depth;

        return $this;
    }
}
