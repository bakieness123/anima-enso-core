<?php

namespace Yadda\Enso\Crud\Traits;

use Log;
use Yadda\Enso\Media\Contracts\ImageFile as ImageContract;

trait HasGalleryFields
{
    /**
     * Get the images from a gallery field
     *
     * @param string $field_name
     *
     * @return \Illuminate\Support\Collection
     */
    public function getGalleryImages(String $field_name)
    {
        $image_class = get_class(resolve(ImageContract::class));

        if (!is_array($this->$field_name)) {
            Log::warning('Tried to gallery images from a field that is not an array');
            return collect([]);
        }

        $ids = array_map(function ($item) {
            return $item['id'];
        }, $this->$field_name);

        $images = $image_class::whereIn('id', $ids)->get();
        $output = [];

        foreach ($ids as $id) {
            $the_image = $images->first(function ($item) use ($id) {
                return $item->id === $id;
            });

            if ($the_image) {
                $output[] = $the_image;
            }
        }

        return collect($output);
    }
}
