<?php

namespace Yadda\Enso\Crud\Traits;

use Yadda\Enso\Crud\Traits\HasCrudName;

/**
 * Trait to apply to an Eloquent Model to ensure it is meets the minimum
 * requirements for use as a CRUD model.
 */
trait IsCrudModel
{
    use HasCrudName;

    /**
     * Database column or attribute used to label the item
     */
    public function ensoLabelColumn(): string
    {
        return 'name';
    }

    /**
     * The value to use at this Model's CRUD label. Name and title are most
     * frequently used, falling back to ID as it's always going to be available.
     *
     * @return string
     */
    public function getCrudLabel(): string
    {
        return $this->name ?? $this->title ?? $this->getKey();
    }

    /**
     * The URL at which this model's can be viewed
     *
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return null;
    }
}
