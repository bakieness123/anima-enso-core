<?php

namespace Yadda\Enso\Crud\Traits\Config;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Yadda\Enso\Utilities\Helpers;

/**
 * This trait can be applied to an Ensō Crud Config to provide templated records
 * functionality.
 */
trait HasTemplates
{
    /**
     * Name of the template field.
     *
     * @return string
     */
    public function hasTemplatesFieldName(): string
    {
        return App::make(Config::get('enso.crud.' . $this->getCrudName() . '.model'))
            ->getTemplatedItemColumnName();
    }

    /**
     * Name of the section that the template field is in.
     *
     * @return string
     */
    public function hasTemplatesSectionName(): string
    {
        return 'main';
    }

    /**
     * Gets a list of template files based on the template directory of the
     * model. If model not provided, try to ascertain which the model based on
     * Crud config.
     *
     * @param string|Model|null
     *
     * @return array
     */
    public function hasTemplatesList($model = null): array
    {
        if (empty($model) && method_exists($this, 'getCrudName')) {
            $model = App::make(
                Config::get(
                    'enso.crud.' . $this->getCrudName() . '.model',
                    []
                )
            );
        } elseif (is_string($model)) {
            $model = App::make($model);
        }

        if (empty($model)) {
            return [];
        }

        $directory = $model->getTemplateDirectory();

        $files = File::allFiles(resource_path("views/{$directory}"));

        $templates = array_map(function ($item) {
            return str_replace('.blade.php', '', $item->getBasename());
        }, $files);

        $templates = array_combine($templates, array_map(function ($template) {
            return ucfirst(str_replace('-', ' ', $template));
        }, $templates));

        return $templates;
    }
}
