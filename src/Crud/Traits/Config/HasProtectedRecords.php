<?php

namespace Yadda\Enso\Crud\Traits\Config;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Yadda\Enso\SiteMenus\Facades\SiteMenu;

/**
 * This trait can be applied to an Ensō Crud Config to provide protected records
 * functionality.
 */
trait HasProtectedRecords
{
    public function initializeHasProtectedRecords(): void
    {
        if (!empty($this->hasProtectedRecordsRecordList($this->getCrudName()))
            && $action = $this->getIndexAction('delete')) {
            $action->mergeCondition([$this, 'hasProtectedRecordsIsDeletable']);
        }
    }

    /**
     * An array of record identifiers for records that should be protected
     * unless permissions allow otherwise.
     *
     * @param string $crud_name
     *
     * @return array
     */
    public function hasProtectedRecordsRecordList(string $crud_name)
    {
        return Config::get('enso.crud.' . $crud_name . '.protected_records', []);
    }

    /**
     * Name of the field that holds the identifiers for protected records on the
     * config.
     *
     * @return string
     */
    public function hasProtectedRecordsFieldName(): string
    {
        return 'slug';
    }

    /**
     * Name of the section that holds the identifiers for protected records on
     * the config.
     *
     * @return string
     */
    public function hasProtectedRecordsSectionName(): string
    {
        return 'main';
    }

    /**
     * Name of the permission that allows Users to delete records irrespective
     * of it's protected status.
     *
     * @return string|null
     */
    public function hasProtectedRecordsDeleteOverridePermission()
    {
        return null;
    }

    /**
     * Name of the permission that allows Users to edit the records identifier
     * field irrespective of it's protected status.
     *
     * @return string|null
     */
    public function hasProtectedRecordsEditOverridePermission()
    {
        return null;
    }

    /**
     * Determines whether the given record is deleteable to the current User
     *
     * @param Model $page
     *
     * @return boolean
     */
    public function hasProtectedRecordsIsDeletable(Model $page): bool
    {
        return (
            Auth::user()->hasPermission(
                $this->hasProtectedRecordsDeleteOverridePermission()
            ) || !$this->hasProtectedRecordsIsProtected($page)
        );
    }

    /**
     * Determines whether the given record is deleteable to the current User
     *
     * @param Model $page
     *
     * @return boolean
     */
    public function hasProtectedRecordsIsEditable(Model $page): bool
    {
        return (
            Auth::user()->hasPermission(
                $this->hasProtectedRecordsEditOverridePermission()
            ) || !$this->hasProtectedRecordsIsProtected($page)
        );
    }

    /**
     * Whether a specific record should be protected
     *
     * @param Model $model
     *
     * @return boolean
     */
    protected function hasProtectedRecordsIsProtected(Model $model): bool
    {
        if (in_array(
            $model->{$this->hasProtectedRecordsFieldName()},
            $this->hasProtectedRecordsRecordList($this->getCrudName())
        )) {
            return true;
        };

        try {
            if ($model->getUrl() && SiteMenu::containsUrl($model->getUrl())) {
                return true;
            }
        } catch (Exception $e) {
            Log::error($e);
        }

        return false;
    }
}
