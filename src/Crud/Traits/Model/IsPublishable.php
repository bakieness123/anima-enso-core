<?php

namespace Yadda\Enso\Crud\Traits\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Yadda\Enso\Crud\Exceptions\PublishableException;
use Yadda\Enso\Users\Contracts\User as UserContract;

/**
 * This trait can be applied to an Ensō Crud Model to provide publishing
 * functionality.
 *
 * @todo - Can we refactor the following reliance out?
 *
 * Models to which you apply this trait should implement the interface
 * Yadda\Enso\Crud\Contracts\Model\IsPublishable
 */
trait IsPublishable
{
    /**
     * Ensure that if a publishable Model implments the 'Publish At' column, it
     * is filled in when the Model is saved, if that Model is publsihed. This
     * ensures that queries for published models can rely on their 'Publish At'
     * value being filled for simpler ordering.
     */
    public static function bootIsPublishable(): void
    {
        static::saving(function ($model) {
            if ($model->getPublishAtColumn()
            && is_null($model->{$model->getPublishAtColumn()})
            && $model->{$model->getPublishedColumn()}) {
                $model->{$model->getPublishAtColumn()} = Carbon::now();
            }
        });
    }

    /**
     * Attribute accessor format for the isPublished call
     *
     * @return boolean
     */
    public function getIsPublishedAttribute(): bool
    {
        return $this->isPublished();
    }

    /**
     * Gets the name of the Published boolean column on this publishable
     *
     * @return string
     */
    public function getPublishedColumn(): string
    {
        return 'published';
    }

    /**
     * Gets the name of the 'Publish At' DateTime column on this publishable
     *
     * @return string|null
     */
    public function getPublishAtColumn()
    {
        return null;
    }

    /**
     * Gets the current Published boolean value from this publishable
     *
     * @return boolean
     */
    public function getPublished(): bool
    {
        return (bool) $this->{$this->getPublishedColumn()};
    }

    /**
     * Gets the current Publish At datetime value from this publishable.
     *
     * @return DateTime|null
     */
    public function getPublishDate()
    {
        return $this->getPublishAtColumn()
            ? $this->{$this->getPublishAtColumn()}
            : null;
    }

    /**
     * Name of the permission that allows users to view a page irrespective of
     * it's publishing state.
     *
     * @return string|null
     */
    public function getPublishViewOverridePermission()
    {
        return null;
    }

    /**
     * Gets the name of the 'Unpublish At' column on this publishable
     *
     * @return string
     */
    public function getUnpublishAtColumn()
    {
        return null;
    }

    /**
     * Gets the 'Unpublish At' datetime from this publishable.
     *
     * @return DateTime
     */
    public function getUnpublishDate()
    {
        return $this->getUnpublishAtColumn()
            ? $this->{$this->getUnpublishAtColumn()}
            : null;
    }

    /**
     * Whether this model can be published.
     *
     * @return boolean
     */
    public function isPublishableCanPublish(): bool
    {
        return !$this->isPublished();
    }

    /**
     * Whether this model can be published.
     *
     * @return boolean
     */
    public function isPublishableCanUnpublish(): bool
    {
        return $this->isPublished();
    }

    /**
     * Checks to see whether this Publishable is published
     *
     * @return boolean
     */
    public function isPublished(): bool
    {
        return (
            $this->getPublished()
            && (
                is_null($this->getPublishDate())
                || $this->getPublishDate()->lt(Carbon::now())
            )
            && (
                is_null($this->getUnpublishDate())
                || $this->getUnpublishDate()->gt(Carbon::now())
            )
        );
    }

    /**
     * Publish this model
     *
     * @param Carbon|null $publish_at
     *
     * @return void
     */
    public function publish(?Carbon $publish_at = null): void
    {
        if ($this->getPublishAtColumn()) {
            // If this model supports publishing at a specific datetime
            if ($publish_at) {
                // If new publishing date provided, then override existing
                $this->{$this->getPublishAtColumn()} = $publish_at;
            } elseif (is_null($this->{$this->getPublishAtColumn()})
                || $this->{$this->getPublishAtColumn()}->gt(Carbon::now())
            ) {
                /**
                 * Otherwise, set the publish_at date to `now` if the current
                 * publishing date is either empty or in the future to ensure
                 * that it is published and accessible once saved.
                 */
                $this->{$this->getPublishAtColumn()} = Carbon::now();
            }
        }

        /**
         * If this model supports unpublishing at a specific datetime and the
         * currently set datetime is historic, clear the current unpublish_at
         * date. Otherwise leave unpublishing state as-is
         */
        if ($this->getUnpublishAtColumn()
            && !is_null($this->{$this->getUnpublishAtColumn()})
            && $this->{$this->getUnpublishAtColumn()}->lt(Carbon::now())
        ) {
            $this->{$this->getUnpublishAtColumn()} = null;
        }

        $this->{$this->getPublishedColumn()} = true;
        $this->save();
    }

    /**
     * Limits a publishable query to only those that should be visible to either
     * a passed in user, the current user if authenticated or a guest.
     *
     * @param Builder $query
     * @param User    $user
     *
     * @return Builder
     */
    public function scopeAccessibleToUser(Builder $query, UserContract $user = null): Builder
    {
        if (Auth::check()) {
            return $this->limitByUser($query, $user ?? Auth::user());
        }

        return $this->limitByGuest($query);
    }

    /**
     * Limits queries to only return item that are considered as published.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopePublished(Builder $query): Builder
    {
        return $query->where(function ($sub_query) {
            return $sub_query->where($this->getPublishedColumn(), true)
                ->when($this->getPublishAtColumn(), function ($query) {
                    $query->where(function ($query) {
                        return $query->whereNull($this->getPublishAtColumn())
                            ->orWhere($this->getPublishAtColumn(), '<', Carbon::now());
                    });
                })
                ->when($this->getUnpublishAtColumn(), function ($query) {
                    $query->where(function ($query) {
                        return $query->whereNull($this->getUnpublishAtColumn())
                            ->orWhere($this->getUnpublishAtColumn(), '>', Carbon::now());
                    });
                });
        });
    }

    /**
     * Limits queries to only return item that are not labelled as published,
     * or which either have a publish_at date set in the future.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeNotPublished(Builder $query): Builder
    {
        return $query->where(function ($sub_query) {
            $now = Carbon::now();

            return $sub_query
                ->where($this->getPublishedColumn(), false)
                ->when($this->getPublishAtColumn(), function ($query) use ($now) {
                    $query->orWhere(function ($query) use ($now) {
                        return $query->whereNotNull($this->getPublishAtColumn())
                            ->where($this->getPublishAtColumn(), '>', $now);
                    });
                })
                ->when($this->getUnpublishAtColumn(), function ($query) use ($now) {
                    $query->orWhere(function ($query) use ($now) {
                        return $query->whereNotNull($this->getUnpublishAtColumn())
                            ->where($this->getUnpublishAtColumn(), '<', $now);
                    });
                });
        });
    }

    /**
     * Limits queries to only return items that are set to publish in the
     * future but are not yet published now
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeWillPublishLater(Builder $query): Builder
    {
        if (!$this->getPublishAtColumn()) {
            throw new PublishableException(
                'Can not use willPublishLater query scope without a publishing '
                    . 'date'
            );
        }

        return $query
            ->whereNotNull($this->getPublishAtColumn())
            ->where($this->getPublishAtColumn(), '>', Carbon::now())
            ->where($this->getPublishedColumn(), true);
    }

    /**
     * Unpublish this model
     */
    public function unpublish(): void
    {
        $this->{$this->getPublishedColumn()} = false;
        $this->save();
    }

    /**
     * Function to be overriden to change the logic being accessibility to
     * guests
     *
     * @param Builder $query
     *
     * @return Builder
     */
    protected function limitByGuest(Builder $query): Builder
    {
        return $query->published();
    }

    /**
     * Function to be overriden to change the logic being accessibility to
     * users
     *
     * @param Builder      $query
     * @param UserContract $user
     *
     * @return Builder
     */
    protected function limitByUser(Builder $query, UserContract $user): Builder
    {
        if ($user->hasPermission($this->getPublishViewOverridePermission())) {
            return $query;
        }

        return $query->published();
    }
}
