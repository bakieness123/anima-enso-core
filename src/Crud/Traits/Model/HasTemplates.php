<?php

namespace Yadda\Enso\Crud\Traits\Model;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

/**
 * Adds a trait for eloquent models that should have a selectable template.
 * Templates should be stored in the folder (or a child-folder of) that which is
 * pointed to by the template directory.
 */
trait HasTemplates
{
    /**
     * The directory to search for custom, single-use templates in.
     *
     * @return string
     */
    protected function getCustomTemplateDirectory(): string
    {
        $crud_name = Str::slug($this->getCrudName());

        return Str::plural($crud_name) . '/' . $crud_name . '-specific-templates';
    }

    /**
     * The name of the default template.
     *
     * @return string
     */
    protected function getDefaultTemplateName(): string
    {
        return 'default';
    }

    /**
     * Gets the template directory to search for templates in.
     *
     * @return string
     */
    public function getTemplateDirectory()
    {
        if ($this->hasCustomTemplate()) {
            return $this->getCustomTemplateDirectory();
        } else {
            return Str::plural(Str::slug($this->getCrudName())) . '/templates';
        }
    }

    /**
     * Name of the column that holds the template value for the implementing
     * model
     *
     * @return string
     */
    public function getTemplatedItemColumnName(): string
    {
        return 'template';
    }

    /**
     * Name of the column that holds the identifier.
     *
     * @return mixed
     */
    public function getTemplatedItemIdentifier()
    {
        return $this->{$this->getTemplatedItemIdentifierName()};
    }

    /**
     * Name of the column that holds the identifier.
     *
     * @return string
     */
    protected function getTemplatedItemIdentifierName(): string
    {
        return 'slug';
    }

    /**
     * Name of the template to use, based on the database column entry.
     *
     * @return string
     */
    protected function getTemlpatedItemTemplate(): string
    {
        return (string) $this->{$this->getTemplatedItemColumnName()};
    }

    /**
     * Gets the name of the template file
     *
     * @return string
     */
    public function getTemplateName()
    {
        return !empty($this->getTemlpatedItemTemplate())
            ? $this->getTemlpatedItemTemplate()
            : $this->getDefaultTemplateName();
    }

    /**
     * Returns the name of the view to use for this templated item. This handles
     * both un-keyed array elements (where the element is the slug) and keyed
     * array elements (where the key is the slug and the element is the name of
     * the template to use).
     *
     * @return string
     */
    public function getViewName()
    {
        $item_identifier = $this->getTemplatedItemIdentifier();
        $templated_records = Config::get('enso.crud.' . $this->getCrudName() . '.templated_records', []);

        if (array_key_exists($item_identifier, $templated_records)) {
            return $templated_records[$item_identifier];
        }

        if (in_array($item_identifier, $templated_records)) {
            $template_name = $item_identifier;
        } else {
            $template_name = $this->getTemplateName();
        }

        return str_replace(
            '/',
            '.',
            trim($this->getTemplateDirectory() . '/' . $template_name, '/')
        );
    }

    /**
     * Whether this templated Model has a custom template.
     *
     * @return boolean
     */
    public function hasCustomTemplate(): bool
    {
        $crud_name = $this->getCrudName();

        if (array_key_exists(
            $this->getTemplatedItemIdentifier(),
            Config::get('enso.crud.' . $crud_name . '.templated_records', [])
        )) {
            return true;
        }

        if (in_array(
            $this->getTemplatedItemIdentifier(),
            Config::get('enso.crud.' . $crud_name . '.templated_records', [])
        )) {
            return true;
        }

        return false;
    }
}
