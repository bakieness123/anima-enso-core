<?php

namespace Yadda\Enso\Crud\Traits;

use Illuminate\Support\Facades\App;
use Ramsey\Uuid\UuidFactory;

/**
 * Trait for models that use Uuids.
 * Adds a uuid when a model is created.
 *
 * Note: when applying this to a model, also set incrementing=false
 * to prevent auto-casting to integer
 */
trait HasUuids
{
    protected static function bootHasUuids()
    {
        static::creating(function ($model) {
            if (!empty($model->uuid_column) && empty($model->{$model->uuid_column})) {
                $model->{$model->uuid_column} = App::make(UuidFactory::class)->uuid1()->toString();
            }
        });
    }
}
