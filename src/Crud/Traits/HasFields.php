<?php

namespace Yadda\Enso\Crud\Traits;

use Yadda\Enso\Crud\Exceptions\FieldIntegrityException;
use Yadda\Enso\Crud\Forms\FieldCollection;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\SectionInterface;

/**
 * When using this trait, you should ensure that there is a property on the the
 * class that matches the $attribute_name you are be using, and that it is an
 * instance of \Yadda\Enso\Crud\Forms\FieldCollection
 */
trait HasFields
{
    /**
     * Check whether this a field with a given name already exists within
     * an internal collection.
     *
     * @param string $attribute_name
     * @param string $field_name
     *
     * @return boolean
     */
    public function baseHasField(string $attribute_name, string $field_name): bool
    {
        return $this->baseGetFieldIndex($attribute_name, $field_name, false) !== false;
    }

    /**
     * Gets a field from an internal collection, by name
     *
     * @param string $attribute_name
     * @param string $field_name
     *
     * @return \Yadda\Enso\Crud\Forms\FieldInterface
     */
    public function baseGetField(string $attribute_name, string $field_name): FieldInterface
    {
        return $this->baseGetFields($attribute_name)->get(
            $this->baseGetFieldIndex($attribute_name, $field_name)
        );
    }

    /**
     * Get an interal field collection
     *
     * @param string $attribute_name
     *
     * @return \Yadda\Enso\Crud\Forms\FieldCollection
     */
    public function baseGetFields(string $attribute_name): FieldCollection
    {
        return $this->$attribute_name;
    }

    /**
     * Extracts and returns a field from an internal collection.
     *
     * @param string $attribute_name
     * @param string $name
     *
     * @return \Yadda\Enso\Crud\Forms\FieldInterface
     */
    public function baseExtractField(string $attribute_name, string $name): FieldInterface
    {
        return $this->baseGetFields($attribute_name)
            ->splice($this->baseGetFieldIndex($attribute_name, $name), 1)->first();
    }

    /**
     * Remove a field from an internal collection, by name
     *
     * @param string $attribute_name
     * @param string $name
     *
     * @return self
     */
    public function baseRemoveField(string $attribute_name, string $name)
    {
        if ($this->baseHasField($attribute_name, $name)) {
            $this->baseExtractField($attribute_name, $name);
        }

        return $this;
    }

    /**
     * Iteratively calls baseAddField for each element in an array of fields
     *
     * @param string $attribute_name
     * @param array  $fields
     *
     * @return self
     */
    public function baseAddFields(string $attribute_name, array $fields)
    {
        foreach ($fields as $field) {
            $this->baseAddField($attribute_name, $field);
        }

        return $this;
    }

    /**
     * Adds a given field to an internal fields collection
     *
     * @param string                                $attribute_name
     * @param \Yadda\Enso\Crud\Forms\FieldInterface $field
     *
     * @throws \Yadda\Enso\Crud\Exceptions\FieldIntegrityException
     *
     * @return self
     */
    public function baseAddField(string $attribute_name, FieldInterface $field)
    {
        if ($this->baseHasField($attribute_name, $field->getName())) {
            throw new FieldIntegrityException(
                'Cannot add field with duplicate name: '
                    . '\''. $field->getName() . '\''
                    . ' to section \''. $this->getName() .'\''
                    . ' in collection \'' . $attribute_name . '\''
            );
        }

        if ($this instanceof SectionInterface) {
            $field->setSection($this);
        }

        $this->baseGetFields($attribute_name)->push($field);

        return $this;
    }

    /**
     * Adds a Field after a field with the given name.
     *
     * @param string                                $attribute_name
     * @param string                                $field_name
     * @param \Yadda\Enso\Crud\Forms\FieldInterface $field
     *
     * @return self
     */
    public function baseAddFieldAfter(string $attribute_name, string $field_name, FieldInterface $field)
    {
        return $this->baseAddFieldsAfter($attribute_name, $field_name, [$field]);
    }

    /**
     * Adds a set of Fields after a field with the given name.
     *
     * @param string $attribute_name
     * @param string $field_name
     * @param array  $fields
     *
     * @return self
     */
    public function baseAddFieldsAfter(string $attribute_name, string $field_name, array $fields)
    {
        if ($this instanceof SectionInterface) {
            foreach ($fields as $field) {
                $field->setSection($this);
            }
        }

        $this->baseGetFields($attribute_name)->splice(
            $this->baseGetFieldIndex($attribute_name, $field_name) + 1,
            0,
            $fields
        );

        return $this;
    }

    /**
     * Adds a Field before a field with the given name.
     *
     * @param string         $attribute_name
     * @param string         $field_name
     * @param FieldInterface $field
     *
     * @return self
     */
    public function baseAddFieldBefore(string $attribute_name, string $field_name, FieldInterface $field)
    {
        return $this->baseAddFieldsBefore($attribute_name, $field_name, [$field]);
    }

    /**
     * Adds a set of Fields before a field with the given name.
     *
     * @param string $attribute_name
     * @param string $field_name
     * @param array  $fields
     *
     * @return self
     */
    public function baseAddFieldsBefore(string $attribute_name, string $field_name, array $fields)
    {
        if ($this instanceof SectionInterface) {
            foreach ($fields as $field) {
                $field->setSection($this);
            }
        }

        $this->baseGetFields($attribute_name)->splice(
            $this->baseGetFieldIndex($attribute_name, $field_name),
            0,
            $fields
        );

        return $this;
    }

    /**
     * Adds a field at the beginning of an internal collection.
     *
     * @param string         $attribute_name
     * @param FieldInterface $field
     *
     * @return self
     */
    public function basePrependField(string $attribute_name, FieldInterface $field)
    {
        if ($this instanceof SectionInterface) {
            $field->setSection($this);
        }

        $this->baseGetFields($attribute_name)->prepend($field);

        return $this;
    }

    /**
     * Adds a field at the beginning of an internal collection.
     *
     * @param string         $attribute_name
     * @param FieldInterface $fields
     *
     * @return self
     */
    public function basePrependFields(string $attribute_name, array $fields)
    {
        // Reverse array to ensure they're in the correct order once prepended
        $fields = array_reverse($fields);

        foreach ($fields as $field) {
            $this->basePrependField($attribute_name, $field);
        }

        return $this;
    }

    /**
     * Adds a field at the end of an internal collection.
     *
     * @param string         $attribute_name
     * @param FieldInterface $field
     *
     * @return self
     */
    public function baseAppendField(string $attribute_name, FieldInterface $field)
    {
        if ($this instanceof SectionInterface) {
            $field->setSection($this);
        }

        $this->baseGetFields($attribute_name)->push($field);


        return $this;
    }

    /**
     * Adds a field at the end of an internal collection.
     *
     * @param string $attribute_name
     * @param array  $fields
     *
     * @return self
     */
    public function baseAppendFields(string $attribute_name, array $fields)
    {
        foreach ($fields as $field) {
            $this->baseAppendField($attribute_name, $field);
        }

        return $this;
    }

    /**
     * Moves a named field to the position just before another named field
     *
     * @param string $attribute_name
     * @param string $source_name
     * @param string $destination_name
     *
     * @return self
     */
    public function baseMoveFieldAfter(string $attribute_name, string $source_name, string $destination_name)
    {
        $transfer = $this->baseExtractField($attribute_name, $source_name);

        $this->baseGetFields($attribute_name)->splice(
            $this->baseGetFieldIndex($attribute_name, $destination_name) + 1,
            0,
            [$transfer]
        );

        return $this;
    }

    /**
     * Moves a named field to the position just before another named field
     *
     * @param string $attribute_name
     * @param string $source_name
     * @param string $destination_name
     *
     * @return self
     */
    public function baseMoveFieldBefore(string $attribute_name, string $source_name, string $destination_name)
    {
        $transfer = $this->baseExtractField($attribute_name, $source_name);

        $this->baseGetFields($attribute_name)->splice(
            $this->baseGetFieldIndex($attribute_name, $destination_name),
            0,
            [$transfer]
        );

        return $this;
    }

    /**
     * Gets the current index of a field, base on it's name.
     *
     * @param string $attribute_name
     * @param string $field_name
     * @param bool   $require_existence
     *
     * @return integer
     */
    protected function baseGetFieldIndex(string $attribute_name, string $field_name, $require_existence = true)
    {
        $result = $this->baseGetFields($attribute_name)
            ->search(function ($field) use ($field_name) {
                return $field->getName() === $field_name;
            });

        if ($require_existence === true && $result === false) {
            throw new FieldIntegrityException('Cannot find field \'' . $field_name . '\'');
        }

        return $result;
    }
}
