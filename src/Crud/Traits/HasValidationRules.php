<?php

namespace Yadda\Enso\Crud\Traits;

use Illuminate\Support\Arr;
use Illuminate\Validation\Rules\Unique;

/**
 * Trait for managing Validation rules on an object. Validation rules are stored
 * as an array key => value pair, where the key is the name of the validation
 * rule and the value is any value which is a valid validation rules under
 * laravel. This means a string of `min:2|max:3` and an array of ['min:2', 'max:3']
 * are both valid values.
 */
trait HasValidationRules
{
    /**
     * Applies exceptions for the current model to any unique rules provided.
     * This applies to string rules, such as `unique:table,column` as well as
     * Rule::unique() instances.
     *
     * @param array $rules
     *
     * @return $rules
     */
    public function applyAutoUniqueRules(array $rules): array
    {
        $item = $this->getEditForm()->getModelInstance();

        foreach ($rules as $rule_key => $rule_set) {
            $exploded = false;

            if (is_string($rule_set)) {
                $rule_set = explode('|', $rule_set);
                $exploded = true;
            }

            foreach ($rule_set as $index => $rule) {
                if (is_object($rule) && $rule instanceof Unique) {
                    $rule->ignore($item);
                } elseif (is_string($rule) && (substr($rule, 0, 6) === 'unique')) {
                    $rule_set[$index] = $rule . ',' . $item->getKey() . ',' . $item->getKeyName();
                } else {
                    continue;
                }
            }

            $rules[$rule_key] = $exploded
                ? implode('|', $rule_set)
                : $rule_set;
        }

        return $rules;
    }

    /**
     * Name of the property that contains the validation rules
     *
     * @return string
     */
    protected function getAutoUniqueRulesProperty(): string
    {
        return 'auto_unique_rules';
    }

    /**
     * Get the rules specific for the create process
     *
     * @return array
     */
    public function getCreateRules(): array
    {
        return $this->getRules();
    }

    /**
     * Get the rules specific for the edit process. This will automatically
     * updates any unique rules to add an exclusion for the current model.
     *
     * @return array
     */
    public function getEditRules(): array
    {
        $rules = $this->getRules();

        if (is_array($this->{$this->getAutoUniqueRulesProperty()})) {
            return array_merge(
                $rules,
                $this->applyAutoUniqueRules(Arr::only($rules, $this->{$this->getAutoUniqueRulesProperty()}))
            );
        } elseif ($this->{$this->getAutoUniqueRulesProperty()}) {
            return $this->applyAutoUniqueRules($rules);
        } else {
            return $rules;
        }
    }

    /**
     * Gets the validation rule, by name, if it exists. Otherwise returns null.
     *
     * @param string $name
     *
     * @return array|string|null
     */
    public function getRule(string $name)
    {
        return Arr::get($this->{$this->getRulesProperty()}, $name);
    }

    /**
     * Get the validation rules for this crud object
     *
     * @return array
     */
    public function getRules(): array
    {
        return $this->{$this->getRulesProperty()};
    }

    /**
     * Name of the property that contains the validation rules
     *
     * @return string
     */
    protected function getRulesProperty(): string
    {
        return 'rules';
    }

    /**
     * Merge a set of rules into the current rules.
     *
     * NOTE: This does not perform a deep merge, it replaces full values from one
     * set of rules with those in another.
     *
     * @param array   $rules
     * @param boolean $new_rules_take_precedence
     *
     * @return self
     */
    public function mergeRules(array $rules, bool $new_rules_take_precedence = true): self
    {
        if ($new_rules_take_precedence) {
            $this->{$this->getRulesProperty()} = array_merge(
                $this->{$this->getRulesProperty()},
                $rules
            );
        } else {
            $this->{$this->getRulesProperty()} = array_merge(
                $rules,
                $this->{$this->getRulesProperty()}
            );
        }

        return $this;
    }

    /**
     * Removes a single rule, by name
     *
     * @param string $name
     *
     * @return self
     */
    public function removeRule(string $name): self
    {
        $this->removeRules([$name]);

        return $this;
    }

    /**
     * Removes multiple rules at once, by name
     *
     * @param array $names
     *
     * @return self
     */
    public function removeRules(array $names): self
    {
        $this->{$this->getRulesProperty()} = Arr::except($this->{$this->getRulesProperty()}, $names);

        return $this;
    }

    /**
     * Helper method for getting/setting a single rule.
     *
     * @param string $name
     * @param mixed  $rule
     *
     * @return self|array|string
     */
    public function rule(string $name, $rule = null)
    {
        if (is_null($rule)) {
            return $this->getRule($name);
        }

        return $this->setRule($name, $rule);
    }

    /**
     * Helper method for setting/getting rules
     *
     * @param null|array $rules
     * @param bool       $merge
     *
     * @return self|array
     */
    public function rules($rules = null, bool $merge = false)
    {
        if (is_null($rules)) {
            return $this->getRules();
        }

        return $merge
            ? $this->mergeRules($rules)
            : $this->setRules($rules);
    }

    /**
     * Set a validation rule for this object
     *
     * @param array
     *
     * @return self
     */
    public function setRule(string $name, $rule): self
    {
        $this->{$this->getRulesProperty()}[$name] = $rule;

        return $this;
    }

    /**
     * Set the validation rules for this object
     *
     * @param array
     *
     * @return self
     */
    public function setRules($rules): self
    {
        $this->{$this->getRulesProperty()} = $rules;

        return $this;
    }
}
