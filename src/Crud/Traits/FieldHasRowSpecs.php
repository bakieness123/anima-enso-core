<?php

namespace Yadda\Enso\Crud\Traits;

use Illuminate\Support\Facades\App;
use Yadda\Enso\Crud\Contracts\FlexibleFieldHandler;
use Yadda\Enso\Crud\Handlers\FlexibleRow;
use Yadda\Enso\Utilities\Helpers;

/**
 * Can be applied to FlexibleContentFields to provide an generic child item
 * getter.
 */
trait FieldHasRowSpecs
{
    /**
     * Parses the buttons block content using the ButtonSection::unpack
     * function
     *
     * @param FlexibleRow $row
     * @param string|null $field_name
     *
     * @return Collection
     */
    public function getSubRowsData(FlexibleRow $row, string $field_name = null)
    {
        $field_content = $row->getBlocks()->get($field_name ?? $this->getName())->getContent();
        $row_specs = $this->getRowSpecs();

        $handler = App::make(FlexibleFieldHandler::class);

        $handler->loadData($field_content, '');

        return $handler->getRows()->map(function ($flexible_row) use ($row_specs) {
            return Helpers::getConcreteClass(
                get_class($row_specs->keyBy->getName()->get($flexible_row->getType()))
            )::unpack($flexible_row);
        });
    }
}
