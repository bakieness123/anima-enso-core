<?php

namespace Yadda\Enso\Crud\Traits;

use Illuminate\Support\Facades\Config;
use Purifier;

trait PurifiesHtml
{
    /**
     * Set whether or not this CRUD should use HTML Purifier
     *
     * @param Boolean $purify
     * @return self
     */
    public function setPurifyHTML($purify = true)
    {
        $this->purify_html = $purify;

        return $this;
    }

    /**
     * Get whether or not this CRUD should use HTML Purifier
     *
     * @return Boolean
     */
    public function getPurifyHTML()
    {
        if (!isset($this->purify_html)) {
            return true;
        }

        return $this->purify_html;
    }

    /**
     * Clean the data before inserting in the database
     *
     * @param String $data
     * @return String
     */
    protected function sanitizeData($data)
    {
        if (!$this->getPurifyHTML()) {
            return $data;
        }

        // Fix to prevent fields using this trait from becoming unable to set a
        // value to null.
        if (is_null($data)) {
            return $data;
        }

        $settings = array_merge(
            // Purifier defaults
            Config::get('purifier.settings.default', []),
            // Enso forced override
            [
                'Attr.AllowedFrameTargets' => ['_blank'],
                'AutoFormat.AutoParagraph' => false,
            ],
            // Implementation specific
            $this->getSanitizationSettings() ?? []
        );

        return Purifier::clean($data, $settings);
    }
}
