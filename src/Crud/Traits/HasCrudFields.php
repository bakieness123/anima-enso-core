<?php

namespace Yadda\Enso\Crud\Traits;

use Yadda\Enso\Crud\Forms\FieldCollection;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Traits\HasFields;

/**
 * When using this trait, you should ensure that there is a property on the the
 * class that matches the property name returned by getFieldAttributeName, and
 * that it is an instance of \Yadda\Enso\Crud\Forms\FieldCollection
 */
trait HasCrudFields
{
    use HasFields;

    /**
     * Gets the proprety name of the fields collection
     *
     * @return string
     */
    protected function getFieldAttributeName(): string
    {
        return 'fields';
    }

    /**
     * Check whether this a field with a given name already exists within
     * the fields collection.
     *
     * @param string $field_name
     *
     * @return boolean
     */
    public function hasField(string $field_name): bool
    {
        return !!$this->baseHasField($this->getFieldAttributeName(), $field_name);
    }

    /**
     * Gets a field from the fields collection, by name
     *
     * @param string $field_name
     *
     * @return \Yadda\Enso\Crud\Forms\FieldInterface
     */
    public function getField(string $field_name): FieldInterface
    {
        return $this->baseGetField($this->getFieldAttributeName(), $field_name);
    }

    /**
     * Get the field collection
     *
     * @return FieldCollection
     */
    public function getFields(): FieldCollection
    {
        return $this->baseGetFields($this->getFieldAttributeName());
    }

    /**
     * Extracts and returns a field from the field collection.
     *
     * @param string $field_name
     *
     * @return FieldInterface
     */
    public function extractField(string $field_name): FieldInterface
    {
        return $this->baseExtractField($this->getFieldAttributeName(), $field_name);
    }

    /**
     * Remove a field from the field collection, by name
     *
     * @param string $field_name
     *
     * @return self
     */
    public function removeField(string $field_name)
    {
        return $this->baseRemoveField($this->getFieldAttributeName(), $field_name);
    }

    /**
     * Iteratively calls addField for each element in an array of fields
     *
     * @param Array  $fields
     *
     * @return self
     */
    public function addFields(array $fields)
    {
        return $this->baseAddFields($this->getFieldAttributeName(), $fields);
    }

    /**
     * Adds a given field to the field collection
     *
     * @param FieldInterface $field
     *
     * @return self
     */
    public function addField(FieldInterface $field)
    {
        return $this->baseAddField($this->getFieldAttributeName(), $field);
    }

    /**
     * Adds a Field after a field with the given name.
     *
     * @param string         $field_name
     * @param FieldInterface $field
     *
     * @return self
     */
    public function addFieldAfter(string $field_name, FieldInterface $field)
    {
        return $this->baseAddFieldAfter(
            $this->getFieldAttributeName(),
            $field_name,
            $field
        );
    }

    /**
     * Adds a set of Fields after a field with the given name.
     *
     * @param string $field_name
     * @param array  $fields
     *
     * @return self
     */
    public function addFieldsAfter(string $field_name, array $fields)
    {
        return $this->baseAddFieldsAfter(
            $this->getFieldAttributeName(),
            $field_name,
            $fields
        );
    }

    /**
     * Adds a Field before a field with the given name.
     *
     * @param string         $field_name
     * @param FieldInterface $field
     *
     * @return self
     */
    public function addFieldBefore(string $field_name, FieldInterface $field)
    {
        return $this->baseAddFieldBefore(
            $this->getFieldAttributeName(),
            $field_name,
            $field
        );
    }

    /**
     * Adds a set of Fields before a field with the given name.
     *
     * @param string $field_name
     * @param array  $fields
     *
     * @return self
     */
    public function addFieldsBefore(string $field_name, array $fields)
    {
        return $this->baseAddFieldsBefore(
            $this->getFieldAttributeName(),
            $field_name,
            $fields
        );
    }

    /**
     * Adds a field at the beginning of the field collection.
     *
     * @param FieldInterface $field
     *
     * @return self
     */
    public function prependField(FieldInterface $field)
    {
        return $this->basePrependField($this->getFieldAttributeName(), $field);
    }

    /**
     * Adds a field at the beginning of the field collection.
     *
     * @param array $fields
     *
     * @return self
     */
    public function prependFields(array $fields)
    {
        return $this->basePrependFields($this->getFieldAttributeName(), $fields);
    }

    /**
     * Adds a field at the end of the field collection.
     *
     * @param FieldInterface $field
     *
     * @return self
     */
    public function appendField(FieldInterface $field)
    {
        return $this->baseAppendField($this->getFieldAttributeName(), $field);
    }

    /**
     * Adds a field at the end of the field collection.
     *
     * @param array $fields
     *
     * @return self
     */
    public function appendFields(array $fields)
    {
        return $this->baseAppendFields($this->getFieldAttributeName(), $fields);
    }

    /**
     * Moves a named field to the position just before another named field
     *
     * @param string $source_name
     * @param string $destination_name
     *
     * @return self
     */
    public function moveFieldAfter(string $source_name, string $destination_name)
    {
        return $this->baseMoveFieldAfter(
            $this->getFieldAttributeName(),
            $source_name,
            $destination_name
        );
    }

    /**
     * Moves a named field to the position just before another named field
     *
     * @param string $source_name
     * @param string $destination_name
     *
     * @return self
     */
    public function moveFieldBefore(string $source_name, string $destination_name)
    {
        return $this->baseMoveFieldBefore(
            $this->getFieldAttributeName(),
            $source_name,
            $destination_name
        );
    }
}
