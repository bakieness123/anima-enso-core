<?php

namespace Yadda\Enso\Crud\Traits\Controller;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use TorMorten\Eventy\Facades\Eventy;
use Yadda\Enso\Crud\Config;

/**
 * This trait can be applied to an Ensō Crud Controller to provide templated
 * records functionality.
 */
trait HasTemplates
{
    /**
     * Boot the trait
     *
     * @return void
     */
    public static function bootHasTemplates(): void
    {
        Eventy::addAction('crud.edit', [static::class, 'hasTemplatesRemoveTemplateField'], 20, 2);
        Eventy::addFilter('crud.update.rules', [static::class, 'hasTemplatesRemoveTemplateRules'], 20, 3);
        Eventy::addAction('crud.update.before', [static::class, 'hasTemplatesRemoveTemplateField'], 20, 2);
    }

    /**
     * Removes a record's template field when the record has a custom template
     *
     * @param Config  $config
     * @param Request $request
     *
     * @return void
     */
    public static function hasTemplatesRemoveTemplateField(Config $config, Request $request)
    {
        $model_instance = $config->getEditForm()->getModelInstance();

        if (empty($model_instance->getTemplatedItemIdentifier())) {
            return;
        }

        if (static::hasTemplatesShouldHideField($model_instance)) {
            $config->getEditForm()
                ->getSection($config->hasTemplatesSectionName())
                ->removeField($config->hasTemplatesFieldName());
        }
    }

    /**
     * Removes rules for Template field when template field will be removed from the Crud.
     *
     * @param array   $rules
     * @param Config  $config
     * @param Request $request
     *
     * @return array
     */
    public static function hasTemplatesRemoveTemplateRules(
        array $rules,
        Config $config,
        Request $request
    ): array {
        $item = $config->getEditForm()->getModelInstance();

        if (static::hasTemplatesShouldHideField($item)) {
            $field_name = $config->hasTemplatesFieldName();
            $section_name = $config->hasTemplatesSectionName();

            unset($rules[$section_name . '.' . $field_name]);
        }

        return $rules;
    }

    /**
     * Whether to remove the template field for a given record.
     *
     * @param Model $model
     *
     * @return boolean
     */
    protected static function hasTemplatesShouldHideField(Model $model): bool
    {
        return method_exists($model, 'hasCustomTemplate')
            && $model->hasCustomTemplate();
    }
}
