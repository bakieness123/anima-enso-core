<?php

namespace Yadda\Enso\Crud\Traits\Controller;

use Enso;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use throwable;
use TorMorten\Eventy\Facades\Eventy;
use Yadda\Enso\Crud\Config;

/**
 * This trait can be applied to an Ensō Crud Controller to provide publishing
 * functionality.
 */
trait IsPublishable
{
    /**
     * Boot the trait
     *
     * @return void
     */
    public static function bootIsPublishable(): void
    {
        Eventy::addAction('crud.create', [static::class, 'isPublishableModifyJsCreateData'], 20, 2);
        Eventy::addAction('crud.store.after', [static::class, 'isPublishableDoStorePublishActions'], 20, 2);
        Eventy::addFilter('crud.store.xhr-success', [static::class, 'isPublishableModifyStoreXhrSuccess'], 20, 3);
        Eventy::addAction('crud.edit', [static::class, 'isPublishableModifyJsEditData'], 20, 2);
        Eventy::addAction('crud.update.after', [static::class, 'isPublishableDoUpdatePublishActions'], 20, 2);
        Eventy::addFilter('crud.update.xhr-success', [static::class, 'isPublishableModifyUpdateXhrSuccess'], 20, 3);
        Eventy::addAction('crud.clone.replciate-item', [static::class, 'isPublishableModifyCloneItem'], 20, 3);
    }

    /**
     * Perform additional actions based on "publish_actions" request data.
     *
     * @param Model $item
     *
     * @return void
     */
    protected static function isPublishableDoCommonPublishActions(Request $request, Model $item): void
    {
        $options = $request->input('publish_actions');

        if (Arr::get($options, 'publish') === true) {
            $item->publish();
        } elseif (Arr::get($options, 'unpublish') === true) {
            $item->unpublish();
        };
    }

    /**
     * Perform additional actions based on "publish_actions" request data.
     *
     * @param Config  $config
     * @param Request $request
     *
     * @return void
     */
    public static function isPublishableDoStorePublishActions(Config $config, Request $request)
    {
        $item = $config->getCreateForm()->getModelInstance();

        static::isPublishableDoCommonPublishActions($request, $item);
    }

    /**
     * Perform additional actions based on "publish_actions" request data.
     *
     * @param Config  $config
     * @param Request $request
     *
     * @return Model
     */
    public static function isPublishableDoUpdatePublishActions(Config $config, Request $request)
    {
        $item = $config->getEditForm()->getModelInstance();

        static::isPublishableDoCommonPublishActions($request, $item);
    }

    /**
     * Perform additional modifications to an unsaved Model instance during the
     * clone item process
     *
     * @param Config  $config
     * @param Request $requests
     * @param Model   $model
     *
     * @return void
     */
    public static function isPublishableModifyCloneItem(Config $config, Request $request, Model $model)
    {
        $model->{$model->getPublishedColumn()} = false;
    }

    /**
     * Add extra js data on the create record route.
     *
     * @return void
     */
    public static function isPublishableModifyJsCreateData(Config $config, Request $request): void
    {
        Enso::setJSData('item_is_published', null);
    }

    /**
     * Add extra js data on the edit record route.
     *
     * @param Config  $config
     * @param Request $request
     *
     * @return void
     */
    public static function isPublishableModifyJsEditData(Config $config, Request $request): void
    {
        $item = $config->getEditForm()->getModelInstance();

        Enso::setJSData('item_is_published', $item->isPublished());
    }

    /**
     * Add additional data to xhr success responses that is common to both store
     * and update actions
     *
     * @param array $response_data
     * @param Model $item
     *
     * @return array
     */
    protected static function isPublishableModifyCommonXhrSuccess(array $response_data, Model $item): array
    {
        if (isset($response_data['data'])) {
            $response_data['data']['published'] = $item->isPublished();
        }

        return $response_data;
    }

    /**
     * Add additional data to xhr success responses for store actions
     *
     * @param array   $response_data
     * @param Config  $config
     * @param Request $request
     *
     * @return array
     */
    public static function isPublishableModifyStoreXhrSuccess(
        array $response_data,
        Config $config,
        Request $request
    ): array {
        return static::isPublishableModifyCommonXhrSuccess(
            $response_data,
            $config->getCreateForm()->getModelInstance()
        );
    }

    /**
     * Add additional data to xhr success responses for update actions
     *
     * @param array   $response_data
     * @param Config  $config
     * @param Request $request
     *
     * @return array
     */
    public static function isPublishableModifyUpdateXhrSuccess(
        array $response_data,
        Config $config,
        Request $request
    ): array {
        return static::isPublishableModifyCommonXhrSuccess(
            $response_data,
            $config->getEditForm()->getModelInstance()
        );
    }

    /**
     * Publish one or more models.
     *
     * NOTE: This route should only be accessed via xhr as it does not provide
     * appropriate responses for anythign else.
     *
     * @param Request $request
     *
     * @return array
     */
    public function publish(Request $request)
    {
        $config = $this->getConfig();
        $model = $config->getModel();

        try {
            $models = $model::whereIn('id', $request->get('values', []))->get();

            if ($models->count() === 0) {
                return [
                    'status' => 'error',
                    'message' => 'No valid ' . $config->getNamePlural() . ' selected to publish.',
                ];
            }

            if (!$this->isPublishableValidatePublishing($models)) {
                return [
                    'status' => 'error',
                    'message' => 'Some of the selected ' . $config->getNamePlural() . ' are not publishable.',
                ];
            }

            $models->each(function ($model) {
                $model->publish();
            });

            DB::beginTransaction();

            $models->each->save();
        } catch (throwable $e) {
            DB::rollback();

            Log::error($e);

            return [
                'status' => 'error',
                'message' => 'Failed publishing these items',
            ];
        }

        DB::commit();

        return [
            'status' => 'success',
        ];
    }

    /**
     * Unpublish one or more models.
     *
     * NOTE: This route should only be accessed via xhr as it does not provide
     * appropriate responses for anythign else.
     *
     * @param Request $request
     *
     * @return array
     */

    public function unpublish(Request $request)
    {
        $config = $this->getConfig();
        $model = $config->getModel();

        try {
            $models = $model::whereIn('id', $request->get('values', []))->get();

            if ($models->count() === 0) {
                return [
                    'status' => 'error',
                    'message' => 'No valid ' . $config->getNamePlural() . ' selected to unpublish.',
                ];
            }

            if (!$this->isPublishableValidateUnpublishing($models)) {
                return [
                    'status' => 'error',
                    'message' => 'Some of the selected ' . $config->getNamePlural() . ' are not unpublishable.',
                ];
            }

            $models->each(function ($model) {
                $model->unpublish();
            });

            DB::beginTransaction();

            $models->each->save();
        } catch (throwable $e) {
            DB::rollback();

            Log::error($e);

            return [
                'status' => 'error',
                'message' => 'Failed unpublishing these items',
            ];
        }

        DB::commit();

        return [
            'status' => 'success',
        ];
    }

    /**
     * Checks whether the given Items collection is valid for publishing.
     * Invalid Collections are ones with no items, or ones where the contained
     * items have a 'canPublish' method, and one or more items do not pass
     * this test.
     *
     * @param Collection $items
     *
     * @return array|bool
     */
    protected function isPublishableValidatePublishing(Collection $items)
    {
        if ($items->count() === 0) {
            return false;
        }

        if (method_exists($items->first(), 'isPublishableCanPublish')) {
            $failures = $items->reject(function ($item) {
                return $item->isPublishableCanPublish();
            });

            return !$failures->count();
        }

        return true;
    }

    /**
     * Checks whether the given Items collection is valid for publishing.
     * Invalid Collections are ones with no items, or ones where the contained
     * items have a 'canPublish' method, and one or more items do not pass
     * this test.
     *
     * @param Collection $items
     *
     * @return array|bool
     */
    protected function isPublishableValidateUnpublishing(Collection $items)
    {
        if ($items->count() === 0) {
            return false;
        }

        if (method_exists($items->first(), 'isPublishableCanUnpublish')) {
            $failures = $items->reject(function ($item) {
                return $item->isPublishableCanUnpublish();
            });

            return !$failures->count();
        }

        return true;
    }
}
