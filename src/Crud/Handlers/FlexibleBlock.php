<?php

namespace Yadda\Enso\Crud\Handlers;

use Illuminate\Support\Collection;

class FlexibleBlock
{
    /**
     * A CSS class to use as the basis for BEM classes
     *
     * @var string
     */
    protected $base_class;

    /**
     * CSS classes to apply to the row
     *
     * @var array
     */
    protected $classes;

    /**
     * Presumably an array of style options and values.
     *
     * @todo Doesn't appear to be used anywhere.
     *
     * @var array
     */
    protected $styles = [];


    /**
     * The string identifier of this block. E.g. 'image' or 'wysiwyg'
     *
     * This value will be used to associate data in the database with row specs
     *
     * @var string
     */
    protected $type;

    /**
     * The name of the block
     *
     * @var string
     */
    protected $name;

    /**
     * The content of this block
     *
     * @var mixed
     */
    protected $content;

    /**
     * Create a new FlexibleBlock
     *
     * @param array  $block_data
     * @param string $field_name
     * @param string $base_class
     */
    public function __construct($block_data, $field_name, $base_class = 'flexible-content')
    {
        $this
            ->setBaseClass($base_class)
            ->setName($field_name)
            ->setType($block_data['component'])
            ->setContent($block_data['content'])
            ->setDefaultClass()
            ->addClasses(explode(' ', $block_data['class']), true);
    }

    /**
     * Set the block type
     *
     * @param string $block_type new block type
     *
     * @return self
     */
    public function setType($block_type)
    {
        $this->type = $block_type;

        return $this;
    }

    /**
     * Get the type of this block
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the block field name
     *
     * @param string $field_name new block field name
     *
     * @return self
     */
    public function setName($field_name)
    {
        $this->name = $field_name;

        return $this;
    }

    /**
     * Get the type of this block
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the base class for bem classes on this block
     *
     * @param string $class base class to use
     *
     * @return self
     */
    protected function setBaseClass($class)
    {
        $this->base_class = $class;

        return $this;
    }

    /**
     * Get the current base class for this block
     *
     * @return string
     */
    public function getBaseClass()
    {
        return $this->base_class;
    }

    /**
     * Set the content value of this block
     *
     * @param mixed $content
     *
     * @return self
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Returns the content of this block
     *
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Check whether the block is empty or not
     *
     * @return boolean
     */
    public function notEmpty()
    {
        $content = $this->getContent();

        if ($content instanceof Collection) {
            return $content->count() > 0;
        }

        return !empty($content);
    }

    /**
     * Set the default class array based on the 'base_class' property and the
     * types of block present
     *
     * @return self
     */
    protected function setDefaultClass()
    {
        $this->classes = [
            $this->getBaseClass() . '__block',
            $this->makeBemModifierClass($this->getType()),
        ];

        return $this;
    }

    /**
     * Prefixes the given class with the BEM base class for this block
     *
     * @param string $class original class
     *
     * @return string BEM class
     */
    protected function makeBemModifierClass($class)
    {
        return $this->getBaseClass() . '__block--' . $class;
    }

    /**
     * Sets the current row classes array
     *
     * @param array $classes classes to set
     *
     * @return self
     */
    public function setClasses(array $classes)
    {
        $this->classes = $classes;

        return $this;
    }

    /**
     * Adds an array of classes to the row, if they don't already exist
     *
     * @param array   $classes classes to add
     * @param boolean $as_bem  whether to BEM the class
     *
     * @return self
     */
    public function addClasses(array $classes, $as_bem = false)
    {
        if ($as_bem) {
            foreach ($classes as $index => $class) {
                $classes[$index] = $this->makeBemModifierClass($class);
            }
        }

        $this->classes = array_unique(array_merge($this->classes, $classes));

        return $this;
    }

    /**
     * Adds a single class to the row, if it isn't already present
     *
     * @param string  $class  Class to add
     * @param boolean $as_bem Whether to BEM the class
     *
     * @return self
     */
    public function addClass(string $class, $as_bem = false)
    {
        if ($as_bem) {
            $class = $this->makeBemModifierClass($class);
        }

        if (!in_array($class, $this->classes)) {
            $this->classes[] = $class;
        }

        return $this;
    }

    /**
     * Removes an array of classes from the current list, if they are present
     *
     * @param array $classes Classes to remove
     *
     * @return self
     */
    public function removeClasses(array $classes)
    {
        $this->array_classes = array_diff($this->classes, $classes);

        return $this;
    }

    /**
     * Removes a single class from the current list, if it is present
     *
     * @param string $class Class to remove
     *
     * @return self
     */
    public function removeClass(string $class)
    {
        if (($class_index = array_search($class, $this->array_classes)) !== false) {
            unset($this->array_classes[$class_index]);
        }
    }

    /**
     * Gets the current classes, by either as a string (by default) or as an
     * array
     *
     * @param boolean $as_array Whether to return as array
     *
     * @return mixed Formatted class list
     */
    public function getClasses($as_array = false)
    {
        if ($as_array) {
            return $this->classes;
        }

        return implode(' ', $this->classes);
    }
}
