<?php

namespace Yadda\Enso\Crud\Handlers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Slugs
{
    /**
     * Name of the slug field
     *
     * @var string
     */
    protected $field_name = 'slug';

    /**
     * Max length of the generated slug
     *
     * @var integer
     */
    protected $max_length = 255;

    /**
     * Model to generate Unique slugs for
     *
     * @var Model
     */
    protected $model;

    /**
     * Slug separator
     *
     * @var string
     */
    protected $separator = '-';

    /**
     * Constructor
     *
     * @param Model|null $model
     */
    public function __construct(Model $model = null)
    {
        $this->model = $model;
    }

    /**
     * Get or set the field name
     *
     * @param string $field_name
     *
     * @return string|self
     */
    public function fieldName(string $field_name = null)
    {
        if (is_null($field_name)) {
            return $this->field_name;
        }

        $this->field_name = $field_name;

        return $this;
    }

    /**
     * Generates a Slug
     *
     * @param string  $source
     *
     * @return string
     */
    public function generateFrom(string $source): string
    {
        return trim(substr(Str::slug($source, $this->separator), 0, $this->max_length));
    }

    /**
     * Generate a Unique slug value based on an initial slug
     *
     * @param string $base_slug
     *
     * @return string
     */
    public function generateUniqueValue(string $base_slug): string
    {
        if ($this->modelQuery($base_slug)->count() === 0) {
            return $base_slug;
        }

        $original_slug = $base_slug;
        $suffix_length = 0;

        while (strlen($base_slug) > 0) {
            $matches = $this->modelQuery($base_slug . (($suffix_length > 0) ? $this->separator : ''))
                ->pluck($this->field_name);

            $start = max(pow(10, ($suffix_length - 1)), 1);
            $end = pow(10, $suffix_length);

            for ($i = $start; $i < $end; $i++) {
                $potential_slug = $base_slug . (($suffix_length > 0) ? $this->separator . (string)$i : '');

                if (!$matches->contains($potential_slug)) {
                    return $potential_slug;
                }
            }

            $suffix_length++;
            $base_slug = substr($original_slug, 0, $this->max_length - (($suffix_length > 0) ? $suffix_length + 1 : 0));
        }

        return '';
    }

    /**
     * Generates a Unique slug
     *
     * @param string $base_slug
     *
     * @return string
     */
    public function generateUniqueSlug(string $base_slug): string
    {
        return $this->generateUniqueValue(
            $this->generateFrom($base_slug)
        );
    }

    /**
     * Static constructor
     *
     * @param Model $model
     *
     * @return self
     */
    public static function make(Model $model = null)
    {
        return (new static($model));
    }

    /**
     * Get or Set the max length
     *
     * @param integer $max_length
     *
     * @return integer|self
     */
    public function maxLength(int $max_length = null)
    {
        if (is_null($max_length)) {
            return $this->max_length;
        }

        $this->max_length = $max_length;

        return $this;
    }

    /**
     * Get or Set the model class
     *
     * @param Model $model
     *
     * @return string|self
     */
    public function model(Model $model = null)
    {
        if (is_null($model)) {
            return $this->model;
        }

        $this->model = $model;

        return $this;
    }

    /**
     * Generates a query to find all records where the slug field begins with
     * the provided slug
     *
     * @param string $slug
     *
     * @return Builder
     */
    protected function modelQuery(string $slug): Builder
    {
        $query = $this->model::query()
            ->withoutGlobalScopes()
            ->where($this->field_name, 'LIKE', $slug . '%');

        if ($this->model->exists) {
            $query->where($this->model->getKeyName(), '!=', $this->model->getKey());
        }

        if ($this->usingSoftDeletes()) {
            $query->withTrashed();
        }

        return $query;
    }

    /**
     * Whether the model uses soft deletes
     *
     * @return boolean
     */
    protected function usingSoftDeletes(): bool
    {
        return in_array(\Illuminate\Database\Eloquent\SoftDeletes::class, class_uses($this->model), true);
    }
}
