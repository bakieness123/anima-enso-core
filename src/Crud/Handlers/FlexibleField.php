<?php

namespace Yadda\Enso\Crud\Handlers;

use Illuminate\Support\Collection;
use Yadda\Enso\Crud\Contracts\FlexibleFieldHandler as FlexibleFieldHandlerContract;
use Yadda\Enso\Crud\Contracts\FlexibleFieldParser as FlexibleFieldParserContract;
use Yadda\Enso\Crud\Handlers\FlexibleRow;

class FlexibleField implements FlexibleFieldHandlerContract
{
    /**
     * Parser for parsing database content into useable data
     *
     * @var \Yadda\Enso\Crud\Contracts\FlexibleFieldParser
     */
    protected $parser;

    /**
     * All of the rows in this field
     *
     * @var \Illuminate\Support\Collection
     */
    protected $rows;

    /**
     * Reference to the original item in case it's required to
     * access or compute additional data on the row
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $instance;

    /**
     * Blade template to render this field with
     *
     * @var string
     */
    protected static $view_template = 'enso-crud::flex-partials.flexible-content';

    /**
     * Create a new FlexibleField
     *
     * @param \Yadda\Enso\Crud\Contracts\FlexibleFieldParser $parser
     */
    public function __construct(FlexibleFieldParserContract $parser)
    {
        $this->parser = $parser;
    }

    /**
     * Set a reference to the original instance for this flexible field
     *
     * @param \Illuminate\Database\Eloquent\Model $instance Instance of the model that this field is on
     *
     * @return self
     */
    public function setInstance(&$instance)
    {
        $this->instance = $instance;

        return $this;
    }

    /**
     * Returns the original instance for this flexible field.
     *
     * @return \Illuminate\Database\Eloquent\Model Instance of the model that this field is on
     */
    public function getInstance()
    {
        return $this->instance;
    }

    /**
     * Loads the data into an interal array, passing it through a helper class
     * in order to expand the data
     *
     * @param array  $data       data to expand
     * @param string $base_class base class for bem
     *
     * @return self
     */
    public function loadData(array $data, $base_class)
    {
        $parsed_data = $this->parser->expand($data);

        $this->rows = collect($parsed_data)->reduce(function ($carry, $item) use ($base_class) {
            $row = new FlexibleRow($item, $base_class);

            // Set this row's previous row.
            $row->previous_row = $carry->last();

            // Set the previous row's next row
            if ($carry->last()) {
                $carry->last()->next_row = $row;
            }

            $carry->push($row);

            return $carry;
        }, new Collection);

        return $this;
    }

    /**
     * Extract an array of id-title pairs from content rows for populating
     * a navbar
     *
     * @param \Illuminate\Database\Eloquent\Model $item
     * @param string                              $field_name
     *
     * @return \Illuminate\Support\Collection
     */
    public function extractNavItems($item, $field_name)
    {
        return $item->flexibleFieldContent($field_name)
            ->getRows()
            ->map(function ($row) {
                $id = $row->setting('row_id') ? $row->setting('row_id')->getContent() : null;
                $title = $row->setting('row_label') ? $row->setting('row_label')->getContent() : null;

                if ($title && $id) {
                    return [
                        'row_id' => $id,
                        'row_label' => $title,
                    ];
                } else {
                    return null;
                }
            })
            ->filter();
    }

    /**
     * Returns a collection of all the rows
     *
     * @return \Illuminate\Support\Collection
     */
    public function getRows()
    {
        return $this->rows;
    }

    /**
     * Static function wrapper call (so we can use multiple arguments cleanly in
     * the blade temlate)
     *
     * @param object $item            item to fetch flex content from
     * @param string $field_name      name of field to get
     * @param string $container_class class to add to container
     * @param string $template        non-standard template name
     *
     * @return string rendered Flexible content
     */
    public static function getRowContent(
        $item,
        $field_name,
        $container_class = 'flexible-content',
        $template = null,
        $id_prefix = null
    ) {
        if (empty($item->$field_name)) {
            return '';
        }

        if (is_null($template) || $template === 'null') {
            $template = static::$view_template;
        }

        return view($template, [
            'container_class' => $container_class,
            'flexible_field' => $item->flexibleFieldContent($field_name, $container_class),
            'item' => $item,
            'id_prefix' => $id_prefix ?? $field_name,
        ])->render();
    }
}
