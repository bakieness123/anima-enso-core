<?php

namespace Yadda\Enso\Crud\Forms;

use Yadda\Enso\Crud\Forms\FieldCollection;
use Yadda\Enso\Crud\Forms\FieldInterface;

interface SectionInterface
{
    /**
     * Create a new section
     *
     * @param string $name The name of the section.
     *                     Should be sluggified (no spaces etc.)
     */
    public function __construct(string $name = 'main');

    public static function make($name = null);

    public function setName(string $name);

    public function getName();

    public function setLabel(string $label);

    public function getLabel();

    /**
     * Add this tab to a given form
     *
     * @param CrudForm $form
     */
    public function addTo($form);

    /**
     * Check whether this a field with a given name already exists within
     * the fields collection.
     *
     * @param string $field_name
     *
     * @return boolean
     */
    public function hasField(string $field_name): bool;


    /**
     * Get the field collection
     *
     * @return FieldCollection
     */
    public function getFields(): FieldCollection;

    /**
     * Gets a field from the fields collection, by name
     *
     * @param string $field_name
     *
     * @return \Yadda\Enso\Crud\Forms\FieldInterface
     */
    public function getField(string $field_name): FieldInterface;

    /**
     * Iteratively calls addField for each element in an array of fields
     *
     * @param Array  $fields
     *
     * @return self
     */
    public function addFields(array $fields);

    /**
     * Adds a given field to the field collection
     *
     * @param FieldInterface $field
     *
     * @return self
     */
    public function addField(FieldInterface $field);

    /**
     * Adds a Field after a field with the given name.
     *
     * @param string         $field_name
     * @param FieldInterface $field
     *
     * @return self
     */
    public function addFieldAfter(string $field_name, FieldInterface $field);

    /**
     * Adds a set of Fields after a field with the given name.
     *
     * @param string $field_name
     * @param array  $fields
     *
     * @return self
     */
    public function addFieldsAfter(string $field_name, array $fields);

    /**
     * Adds a Field before a field with the given name.
     *
     * @param string         $field_name
     * @param FieldInterface $field
     *
     * @return self
     */
    public function addFieldBefore(string $field_name, FieldInterface $field);

    /**
     * Adds a set of Fields before a field with the given name.
     *
     * @param string $field_name
     * @param array  $fields
     *
     * @return self
     */
    public function addFieldsBefore(string $field_name, array $fields);

    /**
     * Adds a field at the beginning of the field collection.
     *
     * @param FieldInterface $field
     *
     * @return self
     */
    public function prependField(FieldInterface $field);

    /**
     * Adds a field at the end of the field collection.
     *
     * @param FieldInterface $field
     *
     * @return self
     */
    public function appendField(FieldInterface $field);

    /**
     * Moves a named field to the position just before another named field
     *
     * @param string $source_name
     * @param string $destination_name
     *
     * @return self
     */
    public function moveFieldAfter(string $source_name, string $destination_name);

    /**
     * Moves a named field to the position just before another named field
     *
     * @param string $source_name
     * @param string $destination_name
     *
     * @return self
     */
    public function moveFieldBefore(string $source_name, string $destination_name);

    /**
     * Extracts and returns a field from the field collection.
     *
     * @param string $field_name
     *
     * @return FieldInterface
     */
    public function extractField(string $field_name): FieldInterface;

    /**
     * Remove a field from the field collection, by name
     *
     * @param string $field_name
     *
     * @return self
     */
    public function removeField(string $field_name);

    /**
     * Add a class to be applied to the section element
     *
     * @param string $class
     */
    public function addClass($class);

    /**
     * Get classes to be applied to the section element
     *
     * @return array
     */
    public function getClasses();
}
