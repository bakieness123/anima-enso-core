<?php

namespace Yadda\Enso\Crud\Forms\Sections;

use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Fields\WysiwygField;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Handlers\FlexibleRow;

class WysiwygSection extends FlexibleContentSection
{
    /**
     * Default name for this section
     *
     * @param string
     */
    const DEFAULT_NAME = 'wysiwyg';

    public function __construct(string $name = 'wysiwyg')
    {
        parent::__construct($name);

        $this->setLabel('Text')
            ->addFields([
                TextField::make('title'),
                WysiwygField::make('content'),
            ])
            ->addSettingsFields([
                TextField::make('section_title')
                    ->setHelptext('This will be used in menus, lists of page sections, etc.')
                    ->addFieldsetClass('is-half'),
                TextField::make('id')
                    ->setLabel('Id Attribute')
                    ->addFieldsetClass('is-half'),
            ]);
    }

    /**
     * Unpacks a row of parsed data, mergin common and row-specific fields
     * into a single object
     */
    public static function unpack(FlexibleRow $row): ?object
    {
        return (object) [
            'id' => $row->settingContent('row_id'),
            'section_title' => $row->blockContent('section_title'),
            'title' => $row->blockContent('title'),
            'content' => static::getWysiwygHtml($row->getBlocks(), 'content'),
        ];
    }
}
