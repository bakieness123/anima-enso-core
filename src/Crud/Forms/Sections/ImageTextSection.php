<?php

namespace Yadda\Enso\Crud\Forms\Sections;

use Exception;
use Yadda\Enso\Crud\Forms\Fields\ButtonsField;
use Yadda\Enso\Crud\Forms\Fields\FileUploadFieldResumable;
use Yadda\Enso\Crud\Forms\Fields\SelectField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Fields\WysiwygField;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Handlers\FlexibleRow;

class ImageTextSection extends FlexibleContentSection
{
    /**
     * Default name for this section
     *
     * @param string
     */
    const DEFAULT_NAME = 'imagetext';

    protected $wysiwyg_modules = [
        'toolbar' => [
            [['header' => [1, 2, 3, 4, 5, 6, false]]],
            ['bold', 'italic', 'underline', 'strike', 'link'],
            [['align' => []]],
            ['clean'],
        ],
    ];

    public function __construct(string $name = 'imagetext')
    {
        parent::__construct($name);

        $this->setLabel('Image/Text')
            ->addFields([
                FileUploadFieldResumable::make('image')
                    ->setUploadPath('general/flexible-content/image-text')
                    ->addFieldsetClass('is-half'),
                SelectField::make('alignment')
                    ->setLabel('Image Alignment')
                    ->addFieldsetClass('is-half')
                    ->setOptions([
                        'auto'  => 'Automatic',
                        'left'  => 'Left',
                        'right' => 'Right',
                    ])
                    ->setDefaultValue(['id' => 'auto', 'name' => 'Automatic'])
                    ->setSettings(['allow_empty' => false]),
                TextField::make('title'),
                WysiwygField::make('content')
                    ->setModules($this->wysiwyg_modules),
                ButtonsField::make('buttons')->setLabel('')->setMaxRows(1),
            ])
            ->addSettingsFields([
                TextField::make('section_title')
                    ->setHelptext('This will be used in menus, lists of page sections, etc.')
                    ->addFieldsetClass('is-half'),
                TextField::make('id')
                    ->setLabel('Id Attribute')
                    ->addFieldsetClass('is-half'),
            ]);
    }

    /**
     * Unpacks a row of parsed data, mergin common and row-specific fields
     * into a single object
     */
    public static function unpack(FlexibleRow $row): ?object
    {
        $blocks = $row->getBlocks();
        $settings = $row->getSettingsBlocks();

        return (object) [
            'id' => $settings->has('row_id')
                ? $settings->get('row_id')->getContent()
                : null,
            'image' => $blocks->has('image')
                ? $blocks->get('image')->getContent()->first()
                : null,
            'alignment' => self::calculateAlignment($row),
            'section_title' => $blocks->has('section_title')
                ? $blocks->get('section_title')->getContent()
                : null,
            'title' => $blocks->has('title')
                ? $blocks->get('title')->getContent()
                : null,
            'content' => static::getWysiwygHtml($blocks, 'content'),
            'buttons' => static::flexibleFieldContent('buttons', $row),
        ];
    }

    /**
     * Calculates whether this should be a right or left aligned Image Text rows
     *
     * @param FlexibleRow $row
     *
     * @return string
     */
    protected static function calculateAlignment(FlexibleRow $row)
    {
        try {
            $requested_alignment = $row->getBlocks()->get('alignment')->getContent()['id'] ?? 'auto';
        } catch (Exception $e) {
            return 'left';
        }

        if ($requested_alignment !== 'auto') {
            return $requested_alignment;
        }

        $state = [];
        $working_row = $row;

        // Pluck out all previous ImageText rows
        do {
            if ($working_row->getType() === $row->getType()) {
                array_unshift($state, ($working_row->getBlocks()->get('alignment')->getContent()['id'] ?? 'auto'));
            }

            $working_row = $working_row->previous_row;
        } while ($working_row);

        // Work down the list, calculating the final left or right value.
        return array_reduce($state, function ($carry, $item) {
            if ($item !== 'auto') {
                return $item;
            }

            if (is_null($carry)) {
                return 'left';
            }

            if ($carry === 'left') {
                return 'right';
            } else {
                return 'left';
            }
        });
    }
}
