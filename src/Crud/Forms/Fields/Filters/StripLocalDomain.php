<?php

namespace Yadda\Enso\Crud\Forms\Fields\Filters;

use Yadda\Enso\Crud\Exceptions\CrudException;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\Fields\Filters\FieldFilterInterface;
use Yadda\Enso\Utilities\Filters\FilterException;
use Yadda\Enso\Utilities\Filters\StripLocalDomain as BaseFilter;

/**
 * Strips the domain, subdomain and protocol from any url
 * present in the data given where the domain of the url
 * matches the current domain
 */
class StripLocalDomain extends BaseFilter implements FieldFilterInterface
{
    /**
     * Applies this FieldFilter to the provided data, making
     * and necessary alterations based on the type of
     * field the data belongs to.
     *
     * @param mixed $data
     * @param FieldInterface $field
     *
     * @return mixed
     */
    public function applyFilterViaField($data, FieldInterface $field)
    {
        if ($field->getTranslatable()) {
            throw new CrudException('`' . __CLASS__ . '` does not yet support translatable fields');
        }

        try {
            return $field->setTextData($this->applyFilter($field->getTextData($data)), $data);
        } catch (FilterException $e) {
            throw new CrudException($e);
        }
    }
}
