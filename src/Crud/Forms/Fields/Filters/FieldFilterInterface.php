<?php

namespace Yadda\Enso\Crud\Forms\Fields\Filters;

use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Utilities\Filters\FilterInterface;

interface FieldFilterInterface extends FilterInterface
{
    /**
     * Applies this FieldFilter to the provided data, making
     * and necessary alterations based on the type of
     * field the data belongs to.
     *
     * @param mixed $data
     * @param FieldInterface $field
     *
     * @return mixed
     */
    public function applyFilterViaField($data, FieldInterface $field);
}
