<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use DB;
use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Crud\Contracts\HasCorrectTyping;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\Fields\GeoField;

class LocationField extends GeoField implements
    FieldInterface,
    HasCorrectTyping
{
    /**
     * Can this field be used inside a flexible content field
     *
     * @var boolean
     */
    protected static $flexible_field = true;

    protected $show_coord_fields = false;

    protected $use_location = true;

    /**
     * Returns the default value of this Field type
     *
     * @return mixed          Default value
     */
    public function getDefaultValue()
    {
        return [
            'location' => null,
        ];
    }

    /**
     * Set which database POINT column to store location data in
     *
     * @param String $column
     * @return void
     */
    public function setLocationColumn($column)
    {
        $this->location_column = $column;

        return $this;
    }

    /**
     * Get the name of the database POINT column that location will be stored in
     *
     * @return String
     */
    public function getLocationColumn()
    {
        return $this->location_column;
    }

    /**
     * Choose whether or not to show the lat and lng fields as well as the map
     *
     * @param Boolean $show
     * @return self
     */
    public function setShowCoordFields($show = true)
    {
        $this->show_coord_fields = $show;

        return $this;
    }

    public function getShowCoordFields()
    {
        return $this->show_coord_fields;
    }

    /**
     * Applies data to a given item. Can be overridden to provide functionality
     * for non-simple data.
     *
     * @todo this doesn't allow setting a value to 0 - it just sets it to default instead
     * @param  Model    $item           Item to set data on
     * @param  mixed    $value          data to set
     */
    public function applyRequestData(Model &$item, $data)
    {
        $field_name = $this->getName();
        $value = $this->getRequestData($data);

        $item->fill([
            $this->getLocationColumn() => DB::raw(
                'POINT(' . $value['location']['lng'] . ', ' . $value['location']['lat'] . ')'
            )
        ]);

        $this->setDirtiness($item->isDirty($field_name));
    }

    /**
     * Hook for modifying the form data, if required
     *
     * @param  mixed    $data           Original data
     * @return mixed                    Modified data
     */
    protected function modifyFormData($data)
    {
        if (!$data) {
            $data = $this->getDefaultValue()['location'];
        }

        if (is_null($data)) {
            return [
                'location' => null,
            ];
        }

        return [
            'location' => [
                'lng' => $data->getLng(),
                'lat' => $data->getLat(),
            ],
        ];
    }

    /**
     * Convert the field to an array
     *
     * @return Array
     */
    public function toArray()
    {
        $array = parent::toArray();
        $array['props']['use_location']      = $this->use_location;
        $array['props']['use_boundary_box']  = $this->use_boundary_box;
        $array['props']['use_zoom']          = $this->use_zoom;
        $array['props']['use_polygon']       = $this->use_polygon;
        $array['props']['use_circle_radius'] = $this->use_circle_radius;
        $array['props']['show_coord_fields'] = $this->show_coord_fields;
        return $array;
    }
}
