<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Yadda\Enso\Crud\Contracts\HasCorrectTyping;
use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;

class DividerField extends Field implements FieldInterface, HasCorrectTyping
{
    /**
     * Whether this field can be used inside Flexible Content
     *
     * @var boolean
     */
    protected static $flexible_field = true;

    /**
     * Readonly status of the field
     *
     * @var boolean
     */
    protected $readonly = true;

    /**
     * Vue component to use to display this field
     *
     * @var string
     */
    protected $tag_name = 'enso-field-divider';

    /**
     * Set a divider title
     *
     * @param string $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->props['title'] = $title;

        return $this;
    }

    /**
     * Get the divider title
     *
     * @return string
     */
    public function getContent()
    {
        return $this->props['title'];
    }

    /**
     * Set a vue component to use to render $content.
     *
     * @param string $component name of component
     *
     * @return self
     */
    public function setComponent($component)
    {
        $this->props['component'] = $component;

        return $this;
    }

    /**
     * Get the name the of the vue component to display $content
     *
     * @return void
     */
    public function getComponent()
    {
        return $this->props['component'];
    }
}
