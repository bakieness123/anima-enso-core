<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Crud\Exceptions\FieldIntegrityException;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\Fields\SelectField;

/**
 * An actual relationship field. Allows selecting other item[s] from a dropdown.
 */
abstract class RelationshipField extends SelectField implements FieldInterface
{
    /**
     * Can this field be used inside a flexible content field
     *
     * @var boolean
     */
    protected static $flexible_field = false;

    /**
     * The class of the Model whose CRUD form this field appears on
     *
     * @var string
     */
    protected $relationship_model_class;

    /**
     * A blank instance of getRelatioshipClass
     *
     * @var Model
     */
    protected $relationship_model_instance;

    /**
     * An instance of the relationship that this field represents
     *
     * @var \Illuminate\Database\Eloquent\Relations\Relation
     */
    protected $relationship_relation;

    /**
     * The class of the Model whose CRUD form this field appears on
     *
     * @return string
     */
    protected function getRelationshipClass($reset = false): string
    {
        if ($reset || is_null($this->relationship_model_class)) {
            if (!class_exists($this->relationship_model_class = $this->getSection()->getModel())) {
                throw new FieldIntegrityException('Class `' . $this->relationship_model_class . '` does not exist.');
            }
        }

        return $this->relationship_model_class;
    }

    /**
     * A blank instance of getRelatioshipClass
     *
     * Used to get relationship specific information.
     *
     * @param boolean $reset whether to force new instance
     *
     * @return Model Model instance
     */
    protected function getOwnerModelInstance($reset = false)
    {
        $this->doFieldIntegrityCheck();

        if (is_null($this->relationship_model_instance) || $reset) {
            $relationship_class = $this->getRelationshipClass();
            $this->relationship_model_instance = new $relationship_class;
        }

        return $this->relationship_model_instance;
    }

    /**
     * An instance of the relationship that this field represents
     *
     * @param boolean $reset Whether to force new instance
     *
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    protected function getRelationshipRelation($reset = false)
    {
        if (is_null($this->relationship_relation) || $reset) {
            $model_instance = $this->getOwnerModelInstance();
            $property_name = $this->getName();
            $this->relationship_relation = $model_instance->$property_name();
        }

        return $this->relationship_relation;
    }

    /**
     * Field integrity check
     *
     * All fields should extend this, call parent versions and then add their
     * own checks if they have their own checks to make
     *
     * @return void
     */
    protected function fieldIntegrityCheck(): void
    {
        parent::fieldIntegrityCheck();

        if (!$this->hasSection()) {
            throw new FieldIntegrityException('Unable to get Model instance from a Field with no section set');
        }
    }
}
