<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\View;
use Yadda\Enso\Crud\Contracts\HasCorrectTyping;
use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;

/**
 * A field for Enso forms which uses a given data getter function to get data
 * and then uses a given blade view to render it. This is then passed to the
 * frontend and rendered statically.
 *
 * Note: due to a limitation of Vue, Vue components may NOT be included in the
 * view.
 */
class CustomHTMLField extends Field implements FieldInterface, HasCorrectTyping
{
    /**
     * Vue component to use to display this field
     *
     * @var string
     */
    protected $tag_name = 'enso-field-custom-html';

    /**
     * Name of the blade view to use to render the content
     *
     * @var string
     */
    protected $view;

    /**
     * Whether this field can be used inside Flexible Content
     *
     * @var boolean
     */
    protected static $flexible_field = false;

    /**
     * Callable to use to get data
     *
     * @var callable
     */
    protected $data_getter;

    /**
     * Set the data getter to use
     */
    public function dataGetter($callback)
    {
        $this->data_getter = $callback;

        return $this;
    }

    /**
     * Set the name of the view to use
     */
    public function setView(string $view): self
    {
        $this->view = $view;

        return $this;
    }

    /**
     * Get the name of the view
     */
    public function getView(): string
    {
        return $this->view;
    }

    /**
     * Helper method to set or get the view
     */
    public function view($view = null)
    {
        if (is_null($view)) {
            return $this->view;
        }

        $this->view = $view;

        return $this;
    }

    /**
     * Gets the correct value for this field from the passed data
     *
     * @param object $item Data source
     * @param string $property_name Override property name
     *
     * @return mixed Matched data
     */
    public function getFormData($item, $property_name = null)
    {
        if (is_null($this->data_getter)) {
            $data = null;
        }

        $data = call_user_func($this->data_getter, $item);

        return View::make($this->view, $data)->render();
    }

    /**
     * Applies data to a given item. Can be overridden to provide functionality
     * for non-simple data.
     *
     * @param  Model    $item           Item to set data on
     * @param  mixed    $value          data to set
     */
    public function applyRequestData(Model &$item, $data)
    {
        //
    }
}
