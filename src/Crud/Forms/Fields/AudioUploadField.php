<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\Fields\FileUploadField;
use Yadda\Enso\Crud\Forms\SectionInterface;
use Yadda\Enso\Media\Models\AudioFile;

class AudioUploadField extends FileUploadField implements FieldInterface
{
    protected $accepted_file_types = [
        'audio/*'
    ];

    /**
     * Name of the custom vue component
     *
     * @var string
     */
    protected $tag_name = 'enso-field-audio-upload';

    // model that this upload field type instantiated
    protected $file_model = AudioFile::class;

    /**
     * Laravel validation rules which can be converted to html validation rules
     * for the form input
     *
     * @var array
     */
    protected $applicable_validation_rules = [
        'required'
    ];
}
