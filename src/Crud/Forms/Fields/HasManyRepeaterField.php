<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Exception;

use Yadda\Enso\Crud\Forms\Fields\RelationshipRepeaterField;

class HasManyRepeaterField extends RelationshipRepeaterField
{
    protected $inverse_relationship;

    /**
     * Create a new field
     *
     * @param String $name
     */
    public function __construct($name)
    {
        parent::__construct($name);

        $this->setRelationApplicationCallback(function ($item, $new_current_relations, $removed_relations) {

            if (is_null($field_name = $this->getInverseRelationshipName())) {
                throw new Exception('You must specify the name of the inverse relationship for this field type.');
            }

            $new_current_relations->each(function ($related_item) use ($item, $field_name) {
                $related_item->{$field_name}()->associate($item)->save();
            });

            if ($this->getDeleteRemoved()) {
                $removed_relations->each(function ($relation) {
                    $relation->delete();
                });
            } else {
                $removed_relations->each(function ($relation) use ($field_name) {
                    $relation->{$field_name}()->dissociate()->save();
                });
            }
        });
    }

    public function setInverseRelationshipName($name)
    {
        $this->inverse_relationship = $name;
        return $this;
    }

    public function getInverseRelationshipName()
    {
        return $this->inverse_relationship;
    }
}
