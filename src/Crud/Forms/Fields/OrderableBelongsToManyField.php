<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\Fields\BelongsToManyField as BaseRelationshipField;
use Yadda\Enso\Crud\Traits\IsOrderable;

/**
 * An actual relationship field. Allows selecting other item[s] from a dropdown.
 */
class OrderableBelongsToManyField extends BaseRelationshipField implements FieldInterface
{
    use IsOrderable;

    /**
     * Name of the custom vue component
     *
     * @var string
     */
    protected $tag_name = 'enso-field-orderable-relationship';

    /**
     * Convert the field to an array
     *
     * @return Array
     */
    public function toArray()
    {
        $array = parent::toArray();

        $array['props']['pivotFields'] = $this->getFields()->toArray();
        $array['props']['settings'] = $this->settings;
        $array['props']['useAjax'] = $this->isAjax();
        $array['props']['settings']['options'] = $this->getFinalOptions();

        return $array;
    }

    /**
     * Hook for modifying the request data, if required
     *
     * @param  mixed    $data           Original data
     * @return mixed                    Modified data
     */
    protected function modifyRequestData($data)
    {
        if (empty($data) || !is_array($data)) {
            return $this->getDefaultValue();
        }

        if ($this->isOrderableAscending()) {
            $count = 1;
        } else {
            $count = count($data);
        }

        $keys = [];
        foreach ($data as $single) {
            $keys[$single[$this->getSetting('track_by')]] =
                array_merge(
                    $this->getPivotData($single),
                    [
                        $this->getOrderableOrderBy() => $this->isOrderableAscending()
                            ? $count++
                            : $count--
                    ]
                );
        }

        return $keys;
    }
}
