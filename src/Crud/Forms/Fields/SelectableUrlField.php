<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use Yadda\Enso\Crud\Contracts\HasCorrectTyping;
use Yadda\Enso\Crud\Exceptions\CrudException;
use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;

class SelectableUrlField extends SelectField implements FieldInterface, HasCorrectTyping
{
    protected $applicable_validation_rules = ['required'];

    protected $default = '';

    /**
     * Names of Crud data types that this SelectableUrlField is able to query
     *
     * @var array
     */
    protected $crud_names = [];

    protected $tag_name = 'enso-field-selectable-url';

    protected static $flexible_field = true;

    /**
     * Create a new SelectableUrlField
     */
    public function __construct($name)
    {
        parent::__construct($name);

        $this->settings['options'] = [
            'track_by' => 'id',
            'label' => 'label',
        ];
    }

    /**
     * Adds a single crud name to the existing list
     *
     * @param string $crud_name
     *
     * @return void
     */
    public function addCrudName(string $crud_name): self
    {
        $this->addCrudNames([$crud_name]);

        return $this;
    }

    /**
     * Adds a set of crud names to the existing list
     *
     * @param array $crud_names
     *
     * @return void
     */
    public function addCrudNames(array $crud_names): self
    {
        $this->crud_names = array_unique(
            array_merge(
                $this->crud_names,
                $crud_names
            )
        );

        return $this;
    }

    /**
     * Helper function for quick getting and setting of crud names
     *
     * @param null|array $crud_names
     *
     * @return void|array
     */
    public function crudNames($crud_names = null)
    {
        if (is_null($crud_names)) {
            return $this->getCrudNames();
        } else {
            return $this->setCrudNames($crud_names);
        }
    }

    /**
     * Current list of crud names
     *
     * @return array
     */
    public function getCrudNames(): array
    {
        return $this->crud_names;
    }

    /**
     * Parses the list of crud names to provide the URL for their index route.
     *
     * @return array
     */
    protected function parseCrudNames(): array
    {
        try {
            return array_map(function ($item) {
                $config = App::make(Config::get('enso.crud.' . $item . '.config'));

                return [
                    'id' => $item,
                    'label' => $config->getNameSingular(),
                    'url' => URL::route($config->getRoute('index')),
                ];
            }, $this->crud_names);
        } catch (Exception $e) {
            throw new CrudException('SelectableUrlField unable to extrapolate urls from crud names.');
        }
    }

    /**
     * Remove a single crud name from the exising list
     */
    public function removeCrudName(string $crud_name): self
    {
        $this->removeCrudNames([$crud_name]);

        return $this;
    }

    /**
     * Remove a set of crud names from the existing list
     */
    public function removeCrudNames(array $crud_names): self
    {
        $this->crud_names = array_diff($this->crud_names, $crud_names);

        return $this;
    }

    /**
     * Overwrites the current list of crud names
     */
    public function setCrudNames(array $crud_names): self
    {
        $this->crud_names = $crud_names;

        return $this;
    }

    /**
     * Get an array of props to apply to the vue component.
     *
     * These can be override by using setProp
     *
     * @return array
     */
    public function getProps(): array
    {
        return array_merge(
            parent::getProps(),
            [
                'cruds' => $this->parseCrudNames(),
                'options' => Arr::get($this->settings, 'options'),
            ]
        );
    }

    /**
     * Hook for modifying the request data, if required
     *
     * @param  mixed    $data           Original data
     * @return mixed                    Modified data
     */
    protected function modifyRequestData($data)
    {
        return $data;
    }

    /**
     * Applies data to a given item. Can be overridden to provide functionality
     * for non-simple data.
     *
     * @todo this doesn't allow setting a value to 0 - it just sets it to default instead
     * @param  Model    $item           Item to set data on
     * @param  mixed    $value          data to set
     */
    public function applyRequestData(Model &$item, $data)
    {
        return Field::applyRequestData($item, $data);
    }

    /**
     * Gets the correct value for this field from the passed data
     *
     *
     * @param  object   $item           Data source
     * @param  string   $property_name  Override property name
     * @return mixed                    matched data
     */
    public function getFormData($item, $property_name = null)
    {
        return Field::getFormData($item, $property_name);
    }
}
