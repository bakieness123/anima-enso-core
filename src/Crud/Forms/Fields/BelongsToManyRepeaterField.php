<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Exception;

use Yadda\Enso\Crud\Forms\Fields\RelationshipRepeaterField;

class BelongsToManyRepeaterField extends RelationshipRepeaterField
{
    /**
     * Create a new field
     *
     * @param String $name
     */
    public function __construct($name)
    {
        parent::__construct($name);

        $this->setRelationApplicationCallback(function ($item, $new_current_relations, $removed_relations) {
            $field_name = $this->getName();

            $relation_ids = $new_current_relations->pluck($this->getRelationPrimaryKeyName());

            $item->{$field_name}()->sync($relation_ids);

            if ($this->getDeleteRemoved()) {
                $removed_relations->each(function ($relation) {
                    $relation->delete();
                });
            }
        });
    }
}
