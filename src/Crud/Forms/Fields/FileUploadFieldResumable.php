<?php

namespace Yadda\Enso\Crud\Forms\Fields;

class FileUploadFieldResumable extends FileUploadField
{
    // route to post uploads to
    protected $upload_route = 'admin.media.upload-resumable';

    protected $tag_name = 'enso-field-file-upload-resumable';

    // Maximum size of a single upload
    protected $max_file_size_mb = null;
}
