<?php

namespace Yadda\Enso\Crud\Forms\Fields;

use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
use Yadda\Enso\Crud\Forms\FieldCollection;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\Fields\RelationshipField;
use Yadda\Enso\Crud\Traits\HasCrudFields;

/**
 * An actual relationship field. Allows selecting other item[s] from a dropdown.
 */
class BelongsToManyField extends RelationshipField implements FieldInterface
{
    use HasCrudFields;

    /**
     * The default value to return if no other value is available
     *
     * @param mixed
     */
    protected $default = [];

    /**
     * Collection of fields to use for Pivot Data handling.
     *
     * @var FieldCollection
     */
    protected $pivot_fields;

    public function __construct($name)
    {
        parent::__construct($name);

        $this->pivot_fields = new FieldCollection;

        $this->data_callback = function ($item) {
            return array_merge(
                $this->resource_class::make($item)->resolve(),
                array_filter([
                    'pivot' => $this->getFields()->keyBy(function ($field) {
                        return $field->getName();
                    })->getFormData($item->pivot)
                ])
            );
        };

        // Set relevant settings for ths field type (ignoreing deprecation
        // warnings)
        $this->setSettings([
            'multiple' => true,
            'hide_selected' => true,
        ], true);
    }

    /**
     * Gets the proprety name of the fields collection
     *
     * @return string
     */
    protected function getFieldAttributeName(): string
    {
        return 'pivot_fields';
    }

    /**
     * Applies data to a given item. Can be overridden to provide functionality
     * for non-simple data.
     *
     * @param  Model    $item           Item to set data on
     * @param  mixed    $value          data to set
     */

    public function applyRequestData(Model &$item, $data)
    {
        // This field affects on relationships, and so can only reliably apply
        // data after a save.
    }

    /**
     * This relationship requires that the id of the current item be set, and
     * as such must happen after a save for the create route
     *
     * @param  Model    $item           Item to set data on
     * @param  mixed    $value          data to set
     *
     * @return self
     */
    public function applyRequestDataAfterSave(Model &$item, $data)
    {
        $data = $this->getRequestData($data);

        $property_name = $this->getName();

        $item->$property_name()->sync($data);
    }

    /**
     * Check whether the relationship was setup withTimestamps()
     *
     * @return boolean
     */
    protected function hasTimestamps()
    {
        $relationship = $this->getRelationshipRelation();
        $table = $relationship->getTable();
        $column = $relationship->updatedAt();
        $cols = DB::select(
            'SELECT * FROM information_schema.COLUMNS ' .
            'WHERE TABLE_NAME = :table ' .
            'AND COLUMN_NAME = :column',
            [
                'table' => $table,
                'column' => $column
            ]
        );

        return count($cols) > 0;
    }

    /**
     * Hook for modifying the request data, if required
     *
     * @param  mixed    $data           Original data
     * @return mixed                    Modified data
     */
    protected function modifyRequestData($data)
    {
        if (empty($data) || !is_array($data)) {
            return $this->getDefaultValue();
        }

        $keys = [];

        foreach ($data as $single) {
            $keys[$single[$this->getSetting('track_by')]] = $this->getPivotData($single);
        }

        return $keys;
    }

    /**
     * Gets the correct value for this field from the passed data
     *
     * @param  object       $item               Data source
     * @param  string       $property_name      Override property name
     *
     * @return mixed                            matched data
     */
    public function getFormData($item, $property_name = null)
    {
        $this->doFieldIntegrityCheck();

        $property_name = $property_name ?? $this->getName();

        if (!$item->relationLoaded($property_name)) {
            $item->load($property_name);
        }

        $relation = $item->$property_name;

        if ($relation->count() === 0) {
            return $this->getDefaultValue();
        } else {
            if (!is_null($this->getAlterFormDataCallback())) {
                $relation_data = call_user_func($this->getAlterFormDataCallback(), $relation);
            } else {
                $relation_data = $relation->map($this->data_callback);
            }
        }

        // dd($relation_data);

        return $relation_data;
    }

    /**
     * Converts the data to an array for use with ->sync() to set pivot data
     * for a relationship.
     *
     * @param array $data
     *
     * @return array
     */
    protected function getPivotData(array $data)
    {
        $pivot_data =  $this->getFields()->keyBy(function ($field) {
            return $field->getName();
        })->map(function ($field) use ($data) {
            return $field->getRequestData($data, 'pivot');
        })->all();

        // According to the Docs, You should just be able to pass an array
        // of ids to the sync method, and provided the relationship has
        // withTimestamps() set, it should update the updated_at column. In
        // practice, this only seems to work when also setting additional
        // pivot data -> see OrderableBelongsToManyField;
        return array_merge(
            $pivot_data,
            $this->hasTimestamps() ? ['updated_at' => Carbon::now()] : []
        );
    }
}
