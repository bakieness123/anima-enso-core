<?php

namespace Yadda\Enso\Crud\Forms;

use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use stdClass;
use Yadda\Enso\Crud\Exceptions\FieldIntegrityException;
use Yadda\Enso\Crud\Forms\FieldCollection;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\RelationshipField;
use Yadda\Enso\Crud\Forms\SectionInterface;
use Yadda\Enso\Crud\Traits\HasCrudFields;

class Section implements SectionInterface
{
    use HasCrudFields;

    /**
     * Fields in this section
     *
     * @var \Yadda\Enso\Crud\Forms\FieldCollection
     */
    protected $fields;

    /**
     * Name of the section
     *
     * Used to refer to the section in code - no spaces, etc
     *
     * @var string
     */
    protected $name;

    /**
     * Text to display on sections tab
     *
     * @var string
     */
    protected $label;

    /**
     * The name of the vue component to use to display this section
     * e.g. <this-text-here :foo="bar">
     *
     * @var string
     */
    protected $tag_name = 'enso-crud-section';

    /**
     * Permission required to edit any fields within this section
     *
     * @param string
     */
    protected $edit_permission;

    /**
     * Classes to be applied to the section element
     *
     * @var array
     */
    protected $classes = [
        'enso-crud-section',
        'columns',
        'is-multiline',
    ];

    /**
     * Props to apply to the sections Vue component
     *
     * @var array
     */
    protected $props = [];

    /**
     * Reference to the Form that this section is in
     *
     * @param \Yadda\Enso\Crud\Forms\Form
     */
    protected $form_reference;

    /**
     * Default name for this section
     *
     * @param string
     */
    const DEFAULT_NAME = 'main';

    /**
     * Optionally set a callback to restrict whether a section should show in
     * the CMS.
     *
     * @param callable
     */
    protected $restriction_callback;

    public function __construct(string $name = 'main')
    {
        $this->name = $name;

        $this->fields = (new FieldCollection)->setSection($this);
    }

    /**
     * Create a new instance
     *
     * @param string name
     *
     * @return SectionInterface
     */
    public static function make($name = null): SectionInterface
    {
        return App::make(static::class, ['name' => $name ?? static::DEFAULT_NAME]);
    }

    /**
     * Get the value of Name
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param mixed name
     *
     * @return self
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of Label
     *
     * @return mixed
     */
    public function getLabel()
    {
        if (is_null($this->label)) {
            $this->setLabel(Str::title(preg_replace('/[_-]/', ' ', $this->getName())));
        }

        return $this->label;
    }

    /**
     * Set the value of Label
     *
     * @param mixed label
     *
     * @return self
     */
    public function setLabel(string $label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Sets the form reference
     *
     * @param \Yadda\Enso\Crud\Forms\Form &$form reference to containing crud form
     */
    public function setForm(&$form)
    {
        $this->form_reference = $form;

        foreach ($this->getFields() as $field) {
            $field->setSection($this);
        }

        return $this;
    }

    /**
     * Returns the form reference
     *
     * @return \Yadda\Enso\Crud\Forms\Form reference to containing crud form
     */
    public function getForm()
    {
        return $this->form_reference;
    }

    /**
     * Whether this form has a form set
     *
     * @return boolean
     */
    public function hasForm()
    {
        return !empty($this->form_reference);
    }

    /**
     * Gets the model from the containing Form
     *
     * @return string Model class
     */
    public function getModel()
    {
        if (!$this->hasForm()) {
            throw new FieldIntegrityException('Cannot get a Model class from a section with no form');
        }

        return $this->getForm()->getModel();
    }

    /**
     * Gets the local data instance of the form's model
     *
     * @return \Illuminate\Database\Eloquent\Model Model instance
     */
    public function getModelInstance()
    {
        if ($this->hasForm()) {
            return $this->getForm()->getModelInstance();
        }

        return null;
    }

    /**
     * Add this section to a given form
     *
     * @param CrudForm $form
     *
     * @return self
     */
    public function addTo($form, $position = null)
    {
        $form->addSection($this, $position);

        return $this;
    }

    /**
     * Sets the Sections edit permission. If this requirement is not met, all
     * fields within this section will be disabled
     *
     * @param string $permission_name Permission slug
     */
    public function setEditPermission($permission_name)
    {
        $this->edit_permission = $permission_name;

        return $this;
    }

    /**
     * Gets the current edit_permission set on this Section
     *
     * @return string Current Edit permission
     */
    public function getEditPermission()
    {
        return $this->edit_permission;
    }

    /**
     * Helper function for getting/setting the collection value column name
     *
     * @param string $permission_name Name of column to use for value
     *
     * @return mixed Current value column name or self, depending on arguments
     */
    public function editPermission($permission_name = null)
    {
        if (is_null($permission_name)) {
            return $this->getEditPermission();
        } else {
            return $this->setEditPermission($permission_name);
        }
    }

    /**
     * Gets an array of the relationship names of any relationship fields
     * present in this section
     *
     * @return array Relationship names
     */
    public function getRelationNames()
    {
        $relation_names = [];

        foreach ($this->getFields() as $field) {
            if ($field instanceof RelationshipField) {
                $relation_names[] = $field->getRelationshipName();
            }
        }

        return $relation_names;
    }

    /**
     * Applies data to the given item
     *
     * @param \Illuminate\Database\Eloquent\Model $item Item to apply data to
     * @param array                               $data All data
     */
    public function applyRequestData(&$item, array $data)
    {
        foreach ($this->getFields() as $field) {
            if (!$field->shouldWriteData()) {
                continue;
            }

            $result = true;

            if ($field->getCallback()) {
                $result = $field->getCallback()($item, $data, $field);
            }

            if ($result === true) {
                $field->applyRequestData($item, $data);
            }

            $field->runPostSaveHooks($item, $data);
        }
    }

    /**
     * Applies data to the given item AFTER a save has been completed, in order
     * to provide the scope to make pivot_table relationships and other updates
     * that required the item to have an id set.
     *
     * @param \Illuminate\Database\Eloquent\Model $item Item to apply data to
     * @param array                               $data All data
     */
    public function applyRequestDataAfterSave(&$item, array $data)
    {
        foreach ($this->getFields() as $field) {
            if (!$field->shouldWriteData()) {
                continue;
            }

            $result = true;

            if ($field->getCallbackAfterSave()) {
                $result = $field->getCallbackAfterSave()($item, $data, $field);
            }

            if ($result === true) {
                $field->applyRequestDataAfterSave($item, $data);
            }
        }
    }

    /**
     * Add a class to be applied to the section element, if it isn't already
     * present in the list
     *
     * @param string $class
     */
    public function addClass($class)
    {
        if (!in_array($class, $this->classes)) {
            $this->classes[] = $class;
        }

        return $this;
    }

    /**
     * Adds an array of classes to the current class list if they are not
     * already present
     *
     * @param array Array of classes to add
     */
    public function addClasses(array $classes)
    {
        try {
            $diffs = array_diff($classes, $this->classes);
        } catch (Exception $e) {
            throw new Exception('Invalid argument provided for Section->addClasses. ' . $e->getMessage());
        }

        if (!empty($diffs)) {
            $this->classes = array_merge($this->classes, $diffs);
        }

        return $this;
    }

    /**
     * Get classes to be applied to the section element
     *
     * @return array
     */
    public function getClasses()
    {
        return $this->classes;
    }

    /**
     * Removes a class to be applied to the section element
     *
     * @param string $class
     */
    public function removeClass($class)
    {
        if (false !== $key = array_search($class, $this->classes)) {
            unset($this->classes[$key]);
        }

        return $this;
    }

    /**
     * Removes an array of classes from the current class list
     *
     * @param array $classes Array of classes to remove
     */
    public function removeClasses(array $classes)
    {
        try {
            $matches = array_intersect($classes, $this->classes);
        } catch (Exception $e) {
            throw new Exception('Invalid argument provided for Section->removeClasses. ' . $e->getMessage());
        }

        if (!empty($matches)) {
            $this->classes = array_diff($this->classes, $matches);
        }

        return $this;
    }

    /**
     * Gets the data associated with each of the fields and stores in a
     * formatted structure for passing to the vue components
     *
     * @param object $item data source
     *
     * @return mixed found data values
     */
    public function getFormData($item)
    {
        if (!is_object($item)) {
            return new stdClass;
        }

        $form_data = [];

        foreach ($this->getFields() as $field) {
            $form_data[$field->getName()] = $field->getFormData($item);
        }

        if (empty($form_data)) {
            return new stdClass;
        }

        return $form_data;
    }

    /**
     * Converts this section into a an array
     *
     * @return array
     */
    public function toArray()
    {
        $output = [
            'label'     => $this->getLabel(),
            'class'     => $this->getClasses(),
            'name'      => $this->getName(),
            'component' => $this->tag_name,
            'props'     => $this->props,
            'fields'    => $this->getFields()->toArray(),
        ];

        return $output;
    }

    /**
     * Set the value of a prop
     *
     * @param string $key   name of the prop
     * @param string $value value to apply to the prop
     *
     * @return self
     */
    public function setProp($key, $value)
    {
        $this->props[$key] = $value;

        return $this;
    }

    /**
     * Get the value of a prop
     *
     * @param string $key name of the prop
     *
     * @return void
     */
    public function getProp($key)
    {
        return isset($this->props[$key]) ? $this->props[$key] : null;
    }


    /**
     * Check to see whether this section is restricted. Returning True denotes
     * that the field should not show on the CMS.
     *
     * @return boolean
     */
    public function isRestricted()
    {
        $restrictions_callable = $this->getSectionRestrictions();

        if (is_null($restrictions_callable)) {
            return false;
        }

        return call_user_func($restrictions_callable, $this);
    }

    /**
     * Sets the Field Restrictions callback. This should be a callable that
     * returns `true` when the field should not show, and false when it should.
     *
     * @param callable
     *
     * @return self
     */
    public function setSectionRestrictions($callable)
    {
        $this->restriction_callback = $callable;
        return $this;
    }

    /**
     * Gets the Fields Restrictions callback
     *
     * @return null\callable
     */
    public function getSectionRestrictions()
    {
        return $this->restriction_callback;
    }

    /**
     * Gets a field by it's name property from this section
     *
     * @deprecated v0.2.277
     *
     * @param string $name Name of field to get
     *
     * @return FieldInterface|null
     */
    public function getFieldByName($name)
    {
        Log::warning('`getFieldByName` has been Deprecated. Use `getField` instead');

        return $this->getField($name);
    }

    /**
     * Remove a field from the section by its name
     *
     * @deprecated v0.2.277
     *
     * @param string $name
     *
     * @return self
     */
    public function removeFieldByName($name)
    {
        Log::warning('`removeFieldByName` has been Deprecated. Use `removeField` instead');

        $this->removeField($name);

        return $this;
    }
}
