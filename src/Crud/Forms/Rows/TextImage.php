<?php

namespace Yadda\Enso\Crud\Forms\Rows;

use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Forms\Rows\Image;
use Yadda\Enso\Crud\Forms\Rows\Text;
use Yadda\Enso\Crud\Handlers\FlexibleRow;
use Yadda\Enso\Crud\Traits\RowHasAlignment;
use Yadda\Enso\Utilities\Helpers;

/**
 * Represents a purely text row withing a flexible content collection.
 */
class TextImage extends FlexibleContentSection
{
    use RowHasAlignment;

    /**
     * Default name for this section
     *
     * @param string
     */
    const DEFAULT_NAME = 'text-image';

    public function __construct(string $name = 'text-image')
    {
        parent::__construct($name);

        $this
            ->setLabel('Text / Image')
            ->addFields(array_merge(
                Helpers::getConcreteClass(Text::class)::textFields(),
                Helpers::getConcreteClass(Image::class)::imageFields()
            ));

        $this->getField('title')->addFieldsetClass('is-9');
        $this->addFieldAfter(
            'title',
            static::alignmentField()
                ->setLabel('Image alignment')
                ->addFieldsetClass('is-3')
        );

        $this->getField('content')
            ->setModules(
                Config::get('enso.flexible-content.rows.text-image.modules', [])
            );
    }

    /**
     * Unpack Row-specific fields. Should be overriden in Rowspecs that extend
     * this class.
     *
     * If style is not selected, default to the first style in the config array
     *
     * @param FlexibleRow $row
     *
     * @return array
     */
    protected static function getRowContent(FlexibleRow $row): array
    {
        return array_merge(
            Helpers::getConcreteClass(Text::class)::textContent($row),
            Helpers::getConcreteClass(Image::class)::imageContent($row),
            [
                'alignment' => static::calculateAlignment($row),
            ]
        );
    }
}
