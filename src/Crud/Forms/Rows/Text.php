<?php

namespace Yadda\Enso\Crud\Forms\Rows;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Forms\Fields\ButtonsField;
use Yadda\Enso\Crud\Forms\Fields\SelectField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Fields\WysiwygField;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Handlers\FlexibleRow;

/**
 * Represents a purely text row withing a flexible content collection.
 */
class Text extends FlexibleContentSection
{
    /**
     * Default name for this section
     *
     * @param string
     */
    public const DEFAULT_NAME = 'text';

    /**
     * Array of style options
     *
     * @var array
     */
    protected $styles;

    public function __construct(string $name = 'text')
    {
        parent::__construct($name);

        $this->addFields(static::textFields());

        $this->getField('title')->addFieldsetClass('is-9');
        $this->addFieldAfter(
            'title',
            SelectField::make('style')
                ->setOptions(Config::get('enso.flexible-content.rows.text.styles'))
                ->setDefaultValue($this->getDefaultStyleValue())
                ->setSettings([
                    'allow_empty' => false,
                    'hide_labels' => true,
                ])
                ->addFieldsetClass('is-3'),
        );
    }

    /**
     * Modules for text rows
     *
     * @return array
     */
    public static function modules(): array
    {
        return Config::get('enso.flexible-content.rows.text.modules', []);
    }

    /**
     * Content for default text fields
     *
     * @param FlexibleRow $row
     * @param string      $suffix
     *
     * @return array
     */
    public static function textContent(FlexibleRow $row, string $suffix = ''): array
    {
        return [
            'buttons' . $suffix => static::flexibleFieldContent('buttons', $row),
            'content' . $suffix => static::getWysiwygHtml($row->getBlocks(), 'content'),
            'title' . $suffix => $row->blockContent('title'),
        ];
    }

    /**
     * Fields to use in text sections
     *
     * @param string $suffix
     *
     * @return array
     */
    public static function textFields(string $suffix = '')
    {
        return [
            TextField::make('title'),
            WysiwygField::make('content' . $suffix)
                ->setModules(static::modules())
                ->isInline(true),
            ButtonsField::make('buttons' . $suffix)
                ->singleButton(),
        ];
    }

    /**
     * Default Style
     *
     * @return string
     */
    protected static function getDefaultStyle(): string
    {
        return Config::get('enso.flexible-content.rows.text.default-style', 'wide');
    }

    /**
     * Default values of the Style dropdown box
     *
     * @return array
     */
    protected static function getDefaultStyleValue(): array
    {
        $default_style = static::getDefaultStyle();

        return SelectField::makeOption(
            $default_style,
            Config::get('enso.flexible-content.rows.text.styles.' . $default_style, 'Wide')
        );
    }

    /**
     * Unpack Row-specific fields. Should be overriden in Rowspecs that extend
     * this class.
     *
     * If style is not selected, default to the first style in the config array
     *
     * @param FlexibleRow $row
     *
     * @return array
     */
    protected static function getRowContent(FlexibleRow $row): array
    {
        $style_track_by = static::make()->getField('style')->getSetting('track_by');

        return array_merge(
            static::textContent($row),
            [

                'style' => Arr::get(
                    $row->blockContent('style'),
                    $style_track_by,
                    Config::get('enso.flexible-content.rows.text.default-style')
                ),
            ]
        );
    }
}
