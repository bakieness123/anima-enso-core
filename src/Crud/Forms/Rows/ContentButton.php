<?php

namespace Yadda\Enso\Crud\Forms\Rows;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Forms\Fields\SelectableUrlField;
use Yadda\Enso\Crud\Forms\Fields\SelectField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\FlexibleContentSection;
use Yadda\Enso\Crud\Handlers\FlexibleRow;

class ContentButton extends FlexibleContentSection
{
    /**
     * Default name for this section
     *
     * @param string
     */
    const DEFAULT_NAME = 'button';

    public function __construct(string $name = 'button')
    {
        parent::__construct($name);

        $this->setLabel('Button')
            ->addFields([
                TextField::make('label')
                    ->setLabel('Text')
                    ->addFieldsetClass('is-6'),
                TextField::make('hover')
                    ->setLabel('Hover Text')
                    ->addFieldsetClass('is-6'),
                SelectableUrlField::make('link')
                    ->setCrudNames(
                        Config::get('enso.flexible-content.rows.content-button.cruds', ['page'])
                    )
                    ->setLabel('URL')
                    ->addFieldsetClass('is-9'),
                SelectField::make('target')
                    ->setLabel('Open in')
                    ->setOptions([
                        'auto' => 'Automatic',
                        '_self' => 'This window',
                        '_blank' => 'A new window',
                    ])
                    ->setSettings([
                        'allow_empty' => false,
                        'show_labels' => false,
                    ])
                    ->setDefaultValue(SelectField::makeOption('auto', 'Automatic'))
                    ->addFieldsetClass('is-3'),
            ]);
    }

    /**
     * Unpack Row-specific fields.
     *
     * @param FlexibleRow $row
     *
     * @return array
     */
    protected static function getRowContent(FlexibleRow $row): array
    {
        $track_by = static::make()->getField('target')->getSetting('track_by');

        $target = Arr::get($row->blockContent('target'), $track_by, 'auto') !== 'auto'
            ? Arr::get($row->blockContent('target'), $track_by, '_self')
            : '_self';

        return [
            'label' => $row->blockContent('label'),
            'hover' => $row->blockContent('hover'),
            'link' => $row->blockContent('link'),
            'target' => $target,
            'rel' => $target === '_blank'
                ? 'noopener noreferrer'
                : '',
        ];
    }
}
