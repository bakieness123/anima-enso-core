<?php

namespace Yadda\Enso\Crud\Forms;

use Illuminate\Database\Eloquent\Model;
use stdClass;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Localisation\JsonTranslatable;

class JsonDataSection extends Section
{
    /**
     * Gets the data associated with each of the fields and stores in a
     * formatted structure for passing to the vue components
     *
     * @param  object   $item       data source
     * @return mixed                found data values
     */
    public function getFormData($item)
    {
        if (!is_object($item)) {
            return new stdClass;
        }

        // Override the normal function to get data from a json field instead OF
        // the given object, instead of the object itself
        $section_name = $this->getName();
        $form_data = [];

        $item = $item->$section_name;

        if (empty($item)) {
            $item = [];
        }

        $translatables = [];

        foreach ($this->getFields() as $field) {
            // @todo don't use data that doesn't have a corresponding CRUD
            if ($field->getTranslatable()) {
                $translatables[] = $field->getName();
            }

            $fillables[] = $field->getName();
        }


        foreach ($this->getFields() as $field) {
            $fake_item = new JsonTranslatable((array) $item, $translatables, $fillables);
            $form_data[$field->getName()] = $field->getFormData($fake_item, $field->getName());
        }

        if (empty($form_data)) {
            return new stdClass;
        }

        return $form_data;
    }

    /**
     * Applies data to the given item
     *
     * @param  Model    $item       Item to apply data to
     *
     * @param  array    $data       All data
     */
    public function applyRequestData(&$item, array $data)
    {
        $section_data = [];
        $section_name = $this->getName();

        foreach ($this->getFields() as $field) {
            if (!$field->shouldWriteData()) {
                continue;
            }

            $field_name = $field->getName();
            $field_value = $field->getRequestData($data);
            if ($field->getTranslatable()) {
                $langs = config('laravellocalization.supportedLocales', []);

                foreach ($langs as $lang_code => $lang) {
                    if (isset($field_value[$lang_code])) {
                        $section_data[$field_name][$lang_code] = $field_value[$lang_code];
                    }
                }
            } else {
                $section_data[$field_name] = $field_value;
            }
        }

        $item->fill([$section_name => $section_data]);
    }

    /**
     * Applies data to the given item AFTER a save has been completed, in order
     * to provide the scope to make pivot_table relationships and other updates
     * that required the item to have an id set.
     *
     * @param  Model    $item       Item to apply data to
     * @param  array    $data       All data
     */
    public function applyRequestDataAfterSave(&$item, array $data)
    {
        /**
         * Everything should happen during the initial save as json data
         * sections cannot hold data representing actual relationships to the
         * parent model, and so can always be saved before the model has an id
         */
    }
}
