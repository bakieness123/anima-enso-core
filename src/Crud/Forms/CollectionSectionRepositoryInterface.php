<?php

namespace Yadda\Enso\Crud\Forms;

interface CollectionSectionRepositoryInterface
{
    /**
     * Returns a Collection with all the items for the Section
     *
     * @return \Illuminate\Support\Collection Collection of items
     */
    public function all();

    /**
     * Gets a new blank instance of the appropriate class
     *
     * @param string $name Identifier to item
     *
     * @return \Yadda\Enso\Settings\Contracts\Model New instance of correct class
     */
    public function newItem($name);
}
