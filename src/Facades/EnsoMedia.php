<?php

namespace Yadda\Enso\Facades;

use Illuminate\Support\Facades\Facade;

class EnsoMedia extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ensomedia';
    }
}
