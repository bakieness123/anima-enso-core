<?php

namespace Yadda\Enso;

use Illuminate\Foundation\Support\Providers\EventServiceProvider;
use Yadda\Enso\Crud\Listeners\AuthSubscriber;
use Yadda\Enso\Crud\Listeners\CrudSubscriber;

class EnsoEventServiceProvider extends EventServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        //
    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        AuthSubscriber::class,
        CrudSubscriber::class,
    ];
}
