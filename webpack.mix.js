const mix = require('laravel-mix');
const path = require('path');
const webpack = require('webpack');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
require('laravel-mix-svg-sprite');

mix.webpackConfig({
  resolve: {
    modules: [
      path.resolve(__dirname, 'vendor/yadda/enso-core/resources/js'),
      path.resolve(__dirname, 'node_modules'),
    ],
  },
  plugins: [
    new webpack.NormalModuleReplacementPlugin(/^\.\/package$/, function (result) {
      if (/cheerio/.test(result.context)) {
        result.request = './package.json';
      }
    }),
    new ImageminPlugin({
      //disable: process.env.NODE_ENV !== 'production', // Disable during development
      pngquant: {
        quality: '95-100',
      },
      test: /\.(jpe?g|png|gif|svg)$/i,
    }),
    // Hacky way to exclude moment.js localisation files to reduce file size
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en/),
  ],
});

mix
  .setPublicPath(path.normalize('installable/public/enso'))
  .js('installable/resources/js/enso.js', 'js/enso-admin.js')
  .sourceMaps(false, 'source-map')
  .sass('installable/resources/sass/enso/enso-admin.scss', 'css/enso-admin.css')
  .version();
