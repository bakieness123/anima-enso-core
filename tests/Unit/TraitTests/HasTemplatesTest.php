<?php

namespace Yadda\Enso\Tests\Unit\TraitTests;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Pages\Models\Page;
use Yadda\Enso\Tests\TestCase;

/**
 * Tests the HasTemplates trait. User the Page model as a basis for tests, as
 * this already model uses the trait.
 */
class HasTemplatesTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();

        Config::set('enso.crud.page.model', Page::class);
    }

    /** @test */
    public function has_templates_models_determines_template_as_default_when_db_column_is_empty()
    {
        $p = new Page(['template' => null]);

        $this->assertNull($p->template);
        $this->assertEquals('default', $p->getTemplateName());
    }

    /** @test */
    public function has_templates_models_determines_template_from_db_column()
    {
        $p = new Page(['template' => 'test-template']);

        $this->assertEquals('test-template', $p->template);
        $this->assertEquals('test-template', $p->getTemplateName());
    }

    /** @test */
    public function has_templates_models_determines_template_directory_when_no_custom_templating_logic_applied()
    {
        $p = new Page;

        $this->assertEquals('pages/templates', $p->getTemplateDirectory());
    }

    /** @test */
    public function has_templates_models_determines_template_directory_when_simple_custom_templating_logic_applied()
    {
        Config::set('enso.crud.page.templated_records', ['about']);

        $p = new Page(['slug' => 'about']);

        $this->assertEquals('pages/page-specific-templates', $p->getTemplateDirectory());
    }

    /** @test */
    public function has_templates_models_determines_view_name_when_no_custom_templating_logic_applied_using_default_template()
    {
        $p = new Page(['template' => null]);

        $this->assertEquals('pages.templates.default', $p->getViewName());
    }

    /** @test */
    public function has_templates_models_determines_view_name_when_no_custom_templating_logic_applied_using_selected_template()
    {
        $p = new Page(['template' => 'no-nav']);

        $this->assertEquals('pages.templates.no-nav', $p->getViewName());
    }

    /** @test */
    public function has_templates_models_determines_view_name_when_simple_custom_templating_logic_applied()
    {
        Config::set('enso.crud.page.templated_records', ['about']);

        $p = new Page(['slug' => 'about']);

        $this->assertEquals('pages.page-specific-templates.about', $p->getViewName());
    }

    /** @test */
    public function has_templates_models_determines_view_name_when_simple_templating_logic_specified_template()
    {
        Config::set('enso.crud.page.templated_records', ['about' => 'custom-folder.template_name']);

        $p = new Page(['slug' => 'about']);

        $this->assertEquals('custom-folder.template_name', $p->getViewName());
    }
}
