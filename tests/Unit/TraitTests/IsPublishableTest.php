<?php

namespace Yadda\Enso\Tests\Unit\TraitTests;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Yadda\Enso\Tests\Concerns\DatePublishable;
use Yadda\Enso\Tests\TestCase;

class IsPublishableTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();

        $this->loadMigrationsFrom(__DIR__ . '/../../Concerns/Database/Migrations');

        $this->artisan('migrate');
    }

    /** @test */
    public function a_publishable_with_a_publish_at_date_is_publishable()
    {
        $publishable = DatePublishable::create([
            'published' => true,
            'publish_at' => null,
        ]);

        $this->assertTrue($publishable->isPublished());
    }

    /** @test */
    public function a_publishable_with_a_publish_at_date_and_publish_at_in_the_past_is_published()
    {
        $publishable = DatePublishable::create([
            'published' => true,
            'publish_at' => Carbon::now()->subDays(1),
        ]);

        $this->assertTrue($publishable->isPublished());
    }

    /** @test */
    public function published_query_scope_only_finds_publishables_that_are_published_now()
    {
        $published = DatePublishable::create([
            'published' => true,
            'publish_at' => Carbon::now()->subDays(1),
        ]);

        $unpublish = DatePublishable::create([
            'published' => false,
            'publish_at' => null,
        ]);

        $publish_later = DatePublishable::create([
            'published' => true,
            'publish_at' => Carbon::now()->addDays(1),
        ]);

        $unpublished_later = DatePublishable::create([
            'published' => false,
            'publish_at' => Carbon::now()->addDays(1),
        ]);

        $publised_publishables = DatePublishable::published()->get();

        $this->assertEquals(1, $publised_publishables->count());
        $this->assertTrue($publised_publishables->contains($published));
    }

    /** @test */
    public function will_published_later_query_scope_finds_the_correct_publishables()
    {
        $published = DatePublishable::create([
            'published' => true,
            'publish_at' => Carbon::now()->subDays(1),
        ]);

        $unpublish = DatePublishable::create([
            'published' => false,
            'publish_at' => null,
        ]);

        $publish_later = DatePublishable::create([
            'published' => true,
            'publish_at' => Carbon::now()->addDays(1),
        ]);

        $unpublished_later = DatePublishable::create([
            'published' => false,
            'publish_at' => Carbon::now()->addDays(1),
        ]);

        $publised_publishables = DatePublishable::willPublishLater()->get();

        $this->assertEquals(1, $publised_publishables->count());

        $this->assertTrue($publised_publishables->contains($publish_later));
    }

    /** @test */
    public function not_published_query_scope_finds_the_correct_publishables()
    {
        $published = DatePublishable::create([
            'published' => true,
            'publish_at' => Carbon::now()->subDays(1),
        ]);

        $unpublish = DatePublishable::create([
            'published' => false,
            'publish_at' => null,
        ]);

        $publish_later = DatePublishable::create([
            'published' => true,
            'publish_at' => Carbon::now()->addDays(1),
        ]);

        $unpublished_later = DatePublishable::create([
            'published' => false,
            'publish_at' => Carbon::now()->addDays(1),
        ]);

        $publised_publishables = DatePublishable::notPublished()->get();

        $this->assertEquals(3, $publised_publishables->count());

        $this->assertTrue($publised_publishables->contains($unpublish));
        $this->assertTrue($publised_publishables->contains($publish_later));
        $this->assertTrue($publised_publishables->contains($unpublished_later));
    }
}
