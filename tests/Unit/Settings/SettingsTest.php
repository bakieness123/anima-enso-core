<?php

namespace Tests\Unit\Settings;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Yadda\Enso\Settings\Facades\EnsoSettings;
use Yadda\Enso\Tests\TestCase;

class SettingsTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function you_can_set_a_setting()
    {
        $this->assertNull(EnsoSettings::get('thing'));

        EnsoSettings::set('thing', 12);
        $this->assertEquals(12, EnsoSettings::get('thing'));

        EnsoSettings::set('thing', 123);
        $this->assertEquals(123, EnsoSettings::get('thing'));
    }
}
