<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms;

use Illuminate\Support\Facades\Log;
use Mockery;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Tests\TestCase;

class FormTest extends TestCase
{
    protected $form;

    public function setUp(): void
    {
        parent::setUp();

        $this->form = new Form();
    }

    /** @test */
    public function a_section_can_be_added_to_a_form()
    {
        $section = new Section('test_section');

        $this->assertEquals(0, $this->form->getSections()->count());

        $this->form->addSection($section);

        $this->assertEquals(1, $this->form->getSections()->count());
        $this->assertEquals($section, $this->form->getSections()[0]);
    }

    /** @test */
    public function multiple_sections_can_be_added_at_once()
    {
        $section_one = new Section('test_section_one');
        $section_two = new Section('test_section_two');

        $this->assertEquals(0, $this->form->getSections()->count());

        $this->form->addSections([
            $section_one,
            $section_two
        ]);

        $this->assertEquals(2, $this->form->getSections()->count());

        $this->assertEquals($section_one, $this->form->getSections()[0]);
        $this->assertEquals($section_two, $this->form->getSections()[1]);
    }

    /** @test */
    public function a_section_can_be_added_before_a_specific_section()
    {
        $section_one = new Section('first_added_section');
        $section_two = new Section('second_added_section');
        $section_three = new Section('third_added_section');

        $this->form->addSection($section_one);
        $this->form->addSection($section_two);

        $this->assertEquals($section_two, $this->form->getSections()[1]);

        $this->form->addSectionBefore('second_added_section', $section_three);

        $this->assertEquals($section_one, $this->form->getSections()[0]);
        $this->assertEquals($section_three, $this->form->getSections()[1]);
        $this->assertEquals($section_two, $this->form->getSections()[2]);
    }

    /** @test */
    public function a_section_can_be_added_after_a_specific_section()
    {
        $section_one = new Section('first_added_section');
        $section_two = new Section('second_added_section');
        $section_three = new Section('third_added_section');

        $this->form->addSection($section_one);
        $this->form->addSection($section_two);

        $this->assertEquals($section_two, $this->form->getSections()[1]);

        $this->form->addSectionAfter('first_added_section', $section_three);

        $this->assertEquals($section_one, $this->form->getSections()[0]);
        $this->assertEquals($section_three, $this->form->getSections()[1]);
        $this->assertEquals($section_two, $this->form->getSections()[2]);
    }

    /** @test */
    public function a_section_can_be_moved_within_the_collection_after_another_section()
    {
        $section_one = new Section('first_added_section');
        $section_two = new Section('second_added_section');
        $section_three = new Section('third_added_section');

        $this->form->addSections([
            $section_one,
            $section_two,
            $section_three
        ]);

        $this->assertEquals($section_one, $this->form->getSections()[0]);
        $this->assertEquals($section_two, $this->form->getSections()[1]);
        $this->assertEquals($section_three, $this->form->getSections()[2]);

        $this->form->moveSectionAfter('first_added_section', 'second_added_section');

        $this->assertEquals($section_two, $this->form->getSections()[0]);
        $this->assertEquals($section_one, $this->form->getSections()[1]);
        $this->assertEquals($section_three, $this->form->getSections()[2]);
    }

    /** @test */
    public function a_section_can_be_moved_within_the_collection_before_another_section()
    {
        $section_one = new Section('first_added_section');
        $section_two = new Section('second_added_section');
        $section_three = new Section('third_added_section');

        $this->form->addSections([
            $section_one,
            $section_two,
            $section_three
        ]);

        $this->assertEquals($section_one, $this->form->getSections()[0]);
        $this->assertEquals($section_two, $this->form->getSections()[1]);
        $this->assertEquals($section_three, $this->form->getSections()[2]);

        $this->form->moveSectionBefore('third_added_section', 'second_added_section');

        $this->assertEquals($section_one, $this->form->getSections()[0]);
        $this->assertEquals($section_three, $this->form->getSections()[1]);
        $this->assertEquals($section_two, $this->form->getSections()[2]);
    }

    /** @test */
    public function a_section_can_be_prepended_to_the_section_list()
    {
        $section_one = new Section('first_added_section');
        $section_two = new Section('second_added_section');

        $this->form->addSection($section_one);

        $this->assertEquals(1, $this->form->getSections()->count());

        $this->form->prependSection($section_two);

        $this->assertEquals($section_two, $this->form->getSections()[0]);
        $this->assertEquals($section_one, $this->form->getSections()[1]);
    }

    /** @test */
    public function a_section_can_be_appended_to_the_section_list()
    {
        $section_one = new Section('first_added_section');
        $section_two = new Section('second_added_section');

        $this->form->addSection($section_one);

        $this->assertEquals(1, $this->form->getSections()->count());

        $this->form->appendSection($section_two);

        $this->assertEquals($section_one, $this->form->getSections()[0]);
        $this->assertEquals($section_two, $this->form->getSections()[1]);
    }

    /**
     * @test
     */
    public function get_section_by_name_is_deprecated()
    {
        Log::shouldReceive('warning')->once();

        $this->form->getSectionByName('test');
    }

    /** @test */
    public function a_section_can_be_found()
    {
        Log::shouldReceive('warning')->never();

        $section_one = new Section('first_added_section');
        $section_two = new Section('second_added_section');
        $section_three = new Section('third_added_section');

        $this->form->addSections([
            $section_one,
            $section_two,
            $section_three,
        ]);

        $this->assertEquals($section_two, $this->form->getSection('second_added_section'));
    }

    /**
     * @test
     */
    public function remove_section_by_name_is_deprecated()
    {
        Log::shouldReceive('warning')->once();

        $this->form->removeSectionByName('test');
    }

    /** @test */
    public function a_section_can_be_removed()
    {
        Log::shouldReceive('warning')->never();

        $section_one = new Section('first_added_section');
        $section_two = new Section('second_added_section');
        $section_three = new Section('third_added_section');

        $this->form->addSections([
            $section_one,
            $section_two,
            $section_three,
        ]);

        $this->assertEquals(3, $this->form->getSections()->count());

        $this->form->removeSection('second_added_section');

        $this->assertEquals(2, $this->form->getSections()->count());

        $this->assertEquals($section_one, $this->form->getSections()[0]);
        $this->assertEquals($section_three, $this->form->getSections()[1]);
    }

    /** @test */
    public function a_section_can_be_extracted()
    {
        Log::shouldReceive('warning')->never();

        $section_one = new Section('first_added_section');
        $section_two = new Section('second_added_section');
        $section_three = new Section('third_added_section');

        $this->form->addSections([
            $section_one,
            $section_two,
            $section_three,
        ]);

        $this->assertEquals(3, $this->form->getSections()->count());

        $extracted = $this->form->extractSection('second_added_section');

        $this->assertEquals(2, $this->form->getSections()->count());

        $this->assertEquals($section_one, $this->form->getSections()[0]);
        $this->assertEquals($section_three, $this->form->getSections()[1]);
    }
}
