<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields\EmailField;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Exceptions\CrudException;
use Yadda\Enso\Crud\Forms\Fields\EmailField;
use Yadda\Enso\Tests\Concerns\Field as BaseField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\Model;
use Yadda\Enso\Tests\Concerns\ModelTrans;
use Yadda\Enso\Tests\Concerns\WordFilter;

class SectionTest extends BaseFieldTest
{
    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->field = (new EmailField('email'))
            ->setDefaultValue('default@email.com');

        $this->setUpConfig();
    }

    /** @test */
    public function has_correct_component()
    {
        $this->assertEquals('enso-field-email', $this->field->getComponent());
    }

    /** @test */
    public function gets_form_data()
    {
        $model = new Model(['email' => 'initial@value.com']);

        $data = $this->field->getFormData($model);

        $this->assertEquals('initial@value.com', $data);
    }

    /** @test */
    public function gets_altered_form_data()
    {
        $model = new Model(['email' => 'initial@value.com']);

        $this->field->setAlterFormDataCallback(function ($value) {
            return 'modified.' . $value;
        });

        $data = $this->field->getFormData($model);

        $this->assertEquals('modified.initial@value.com', $data);
    }

    /** @test */
    public function gets_from_data_default_when_source_not_available()
    {
        $model = new Model();

        $data = $this->field->getFormData($model);

        $this->assertEquals(null, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "default@email.com"');
    }

    /** @test */
    public function gets_form_data_default_when_null()
    {
        $model = new Model(['email' => null]);

        $data = $this->field->getFormData($model);

        $this->assertEquals(null, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "default@email.com"');
    }

    /** @test */
    public function gets_from_data_default_when_empty_string()
    {
        $model = new Model(['email' => '']);

        $data = $this->field->getFormData($model);

        $this->assertEquals('', $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "default@email.com"');
    }

    /** @test */
    public function updates_a_model_with_input_data()
    {
        $model = new Model(['email' => 'initial@email.com']);
        $data = [
            'main' => [
                'email' => 'new@email.com',
            ],
        ];

        $this->assertEquals('initial@email.com', $model->email);

        $this->field->getSection()->applyRequestData($model, $data);

        $this->assertEquals('new@email.com', $model->email);
    }

    /** @test */
    public function disabled_fields_dont_write_data()
    {
        $model = new Model(['email' => 'initial@email.com']);

        $this->field->setDisabled();

        $data = [
            'main' => [
                'email' => 'updated@email.com'
            ]
        ];

        $this->field->applyRequestData($model, $data);

        $this->assertEquals('updated@email.com', $model->email);

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function read_only_fields_dont_write_data()
    {
        $model = new Model(['email' => 'initial@email.com']);

        $this->field->setReadonly();

        $data = [
            'main' => [
                'email' => 'updated@email.com'
            ]
        ];

        $this->field->applyRequestData($model, $data);

        $this->assertEquals('updated@email.com', $model->email);

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function updates_a_model_with_altered_input_data()
    {
        $model = new Model(['email' => 'initial@email.com']);
        $data = [
            'main' => [
                'email' => 'new@email.com',
            ],
        ];

        $this->field->setAlterRequestDataCallback(function ($value) {
            return 'altered.' . $value;
        });

        $this->assertEquals('initial@email.com', $model->email);

        $this->field->getSection()->applyRequestData($model, $data);

        $this->assertEquals('altered.new@email.com', $model->email);
    }

    /** @test */
    public function sanitizion_can_be_used_request_data()
    {
        $model = new Model(['email' => 'initial@email.com']);
        $data = [
            'main' => [
                'email' => 'new@email.com',
            ],
        ];

        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true);

        $this->assertEquals('initial@email.com', $model->email);

        $sanitized = $this->field->getRequestData($data);

        $this->assertEquals('<p>new@email.com</p>', $sanitized);
    }

    /** @test */
    public function applies_filters_to_request_data()
    {
        $model = new Model(['email' => 'initial@email.com']);
        $data = [
            'main' => [
                'email' => 'new@email.com',
            ],
        ];

        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('new'),
        ]);

        $this->assertEquals('initial@email.com', $model->email);

        $this->expectException(CrudException::class);

        $this->field->getSection()->applyRequestData($model, $data);
    }

    /** @test */
    public function updates_a_model_with_default_value_when_given_null_input_data()
    {
        $model = new Model(['email' => 'initial@email.com']);
        $data = [
            'main' => [
                'email' => null,
            ],
        ];

        $this->assertEquals('initial@email.com', $model->email);

        $this->field->getSection()->applyRequestData($model, $data);

        $this->assertEquals('default@email.com', $model->email);

        $this->markTestIncomplete('Not Sure it should be doing this');
    }

    /** @test */
    public function updates_a_model_with_default_value_when_given_empty_string_input_data()
    {
        $model = new Model(['email' => 'initial@email.com']);
        $data = [
            'main' => [
                'email' => '',
            ],
        ];

        $this->assertEquals('initial@email.com', $model->email);

        $this->field->getSection()->applyRequestData($model, $data);

        $this->assertEquals('default@email.com', $model->email);

        $this->markTestIncomplete('Not Sure it should be doing this');
    }

    /**
     * Translatable
     */

    /** @test */
    public function gets_translatable_form_data_correctly()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans([
            'email' => [
                "en" => "en@email.com",
                "es" => "es@email.com",
            ],
        ]);

        $expected_output = [
            "en" => "en@email.com",
            "es" => "es@email.com",
        ];

        $data = $this->field->getFormData($model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function gets_translatable_default_form_data_when_source_not_available()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans;

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = ['en' => 'default@email.com'];

        $data = $this->field->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('It would be useful to be able so set an array of default values.');
    }

    /** @test */
    public function gets_translatable_default_form_data_correctly_when_value_is_null()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans([
            'email' => null,
        ]);

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = ['en' => 'default@email.com'];

        $data = $this->field->getFormData($model);

        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('It would be useful to be able so set an array of default values.');
    }

    /** @test */
    public function gets_translatable_default_form_data_correctly_when_value_is_empty_string()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans([
            'email' => "",
        ]);

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = ['en' => 'default@email.com'];

        /**
         * We should't ever hit this point, but we should code it so that IF we
         * do, we should handle it gracefully (which shouldn't be difficult) in case
         * older or newer versions allowed for empty strings.
         */
        $this->markTestIncomplete('This is broken. It tries to use empty string, which getTranslations cannot use.');

        // $data = $this->field->getFormData($model);

        // $this->assertEquals('', $data);

        // $this->markTestIncomplete('It would be useful to be able so set an array of default values.');
    }

    /** @test */
    public function applies_translatable_request_data_correctly()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans;

        $data = [
            'main' => [
                'email' => [
                    'en' => 'en@email.com',
                    'es' => 'es@email.com',
                ],
            ],
        ];

        $this->field->applyRequestData($model, $data);

        $this->assertEquals('en@email.com', $model->getTranslation('email', 'en', true));
        $this->assertEquals('es@email.com', $model->getTranslation('email', 'es', true));
    }

    /** @test */
    public function disabled_translatable_fields_dont_write_data()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans([
            'main' => [
                'email' => [
                    'en' => 'en@email.com',
                    'es' => 'es@email.com',
                ]
            ]
        ]);

        $this->field->setDisabled();

        $data = [
            'main' => [
                'email' => [
                    'en' => 'new.en@email.com',
                    'es' => 'new.es@email.com',
                ],
            ],
        ];

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('new.en@email.com', $model->getTranslation('email', 'en', true));
        $this->assertEquals('new.es@email.com', $model->getTranslation('email', 'es', true));

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function read_only_translatable_fields_dont_write_data()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans([
            'main' => [
                'email' => [
                    'en' => 'en@email.com',
                    'es' => 'es@email.com',
                ]
            ]
        ]);

        $this->field->setReadonly();

        $data = [
            'main' => [
                'email' => [
                    'en' => 'new.en@email.com',
                    'es' => 'new.es@email.com',
                ],
            ],
        ];

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('new.en@email.com', $model->getTranslation('email', 'en', true));
        $this->assertEquals('new.es@email.com', $model->getTranslation('email', 'es', true));

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is disabled.');
    }

    /** @test */
    public function can_sanitize_translatable_data_when_writing_data()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans([
            'main' => [
                'email' => [
                    'en' => 'en@email.com',
                    'es' => 'es@email.com',
                ]
            ]
        ]);

        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true);

        $data = [
            'main' => [
                'email' => [
                    'en' => 'new.en@email.com',
                    'es' => 'new.es@email.com',
                ],
            ],
        ];

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('<p>new.en@email.com</p>', $model->getTranslation('email', 'en', true));
        $this->assertEquals('<p>new.es@email.com</p>', $model->getTranslation('email', 'es', true));
    }

    /** @test */
    public function can_ultilise_alter_data_callbacks_for_translatable_data_when_writing_data()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans([
            'main' => [
                'email' => [
                    'en' => 'en@email.com',
                    'es' => 'es@email.com',
                ]
            ]
        ]);

        $this->field->setAlterRequestDataCallback(function ($value) {
            return array_map(function ($single) {
                return 'altered.' . $single;
            }, $value);
        });

        $data = [
            'main' => [
                'email' => [
                    'en' => 'new.en@email.com',
                    'es' => 'new.es@email.com',
                ],
            ],
        ];

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('altered.new.en@email.com', $model->getTranslation('email', 'en', true));
        $this->assertEquals('altered.new.es@email.com', $model->getTranslation('email', 'es', true));
    }

    /** @test */
    public function can_ultilise_filters_for_translatable_data_when_writing_data()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans([
            'main' => [
                'email' => [
                    'en' => 'en@email.com',
                    'es' => 'es@email.com',
                ]
            ]
        ]);

        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('New'),
        ]);;

        $data = [
            'main' => [
                'email' => [
                    'en' => 'new.en@email.com',
                    'es' => 'new.es@email.com',
                ],
            ],
        ];

        $this->ExpectException(CrudException::class);

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('***.en@email.com', $model->getTranslation('email', 'en', true));
        $this->assertEquals('***.es@email.com', $model->getTranslation('email', 'es', true));
    }

    /** @test */
    public function applies_translatable_request_data_correctly_when_data_is_null()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans;

        $data = [
            'main' => [
                'email' => null,
            ],
        ];

        $this->field->applyRequestData($model, $data);

        $this->assertEquals('default@email.com', $model->getTranslation('email', 'en', false));
        $this->assertEquals('', $model->getTranslation('email', 'es', false));
    }

    /** @test */
    public function applies_translatable_request_data_correctly_when_data_is_empty_string()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans;

        $data = [
            'main' => [
                'email' => '',
            ],
        ];

        $this->field->applyRequestData($model, $data);

        $this->assertEquals('default@email.com', $model->getTranslation('email', 'en', false));
        $this->assertEquals('', $model->getTranslation('email', 'es', false));
    }
}
