<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields\SlugField;

use Exception;
use Yadda\Enso\Crud\Exceptions\CrudException;
use Yadda\Enso\Crud\Forms\Fields\SlugField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\Model;
use Yadda\Enso\Tests\Concerns\ModelTrans;
use Yadda\Enso\Tests\Concerns\WordFilter;

class SectionTest extends BaseFieldTest
{
    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        // Slug field is a special case, as it contains logic to
        // enforce uniqueness of slug, and so requires being able to
        // check against a database.
        $this->loadMigrationsFrom(__DIR__ . '/../../../../../Concerns/Database/Migrations');

        $this->field = (new SlugField('slug'))
            ->setSource('text')
            ->setRoute('%SLUG%')
            ->setDefaultValue('default-value');

        $this->setUpConfig();
    }

    /** @test */
    public function has_correct_component()
    {
        $this->assertEquals('enso-field-slug', $this->field->getComponent());
    }

    /** @test */
    public function gets_form_data()
    {
        $model = new Model(['slug' => 'initial-value']);

        $data = $this->field->getFormData($model);

        $this->assertEquals('initial-value', $data);
    }

    /** @test */
    public function gets_altered_form_data()
    {
        $model = new Model(['slug' => 'initial-value']);

        $this->field->setAlterFormDataCallback(function ($value) {
            return 'modified-' . $value;
        });

        $data = $this->field->getFormData($model);

        $this->assertEquals('modified-initial-value', $data);
    }

    /** @test */
    public function gets_from_data_default_when_source_not_available()
    {
        $model = new Model();

        $data = $this->field->getFormData($model);

        $this->assertEquals(null, $data);

        $this->markTestIncomplete('This should be fetching a default value');

        $this->assertEquals('default-value', $data);
    }

    /** @test */
    public function gets_form_data_when_data_falsey_but_should_still_be_used()
    {
        $model = new Model(['slug' => '0']);

        $data = $this->field->getFormData($model);

        $this->assertEquals('0', $data);
    }

    /** @test */
    public function gets_form_data_default_when_null()
    {
        $model = new Model(['slug' => null]);

        $data = $this->field->getFormData($model);

        $this->assertEquals(null, $data);

        $this->markTestIncomplete('This should be fetching a default value');

        $this->assertEquals('default-value', $data);
    }

    /** @test */
    public function gets_form_data_default_when_empty_string()
    {
        $model = new Model(['slug' => '']);

        $data = $this->field->getFormData($model);

        $this->assertEquals('', $data);

        $this->markTestIncomplete('This should be fetching a default value');

        $this->assertEquals('default-value', $data);
    }

    /** @test */
    public function updates_a_model_with_input_data()
    {
        $model = new Model(['slug' => 'initial-value']);
        $data = [
            'main' => [
                'slug' => 'new-value',
            ],
        ];

        $this->assertEquals('initial-value', $model->slug);

        $this->field->applyRequestData($model, $data);

        $this->assertEquals('new-value', $model->slug);
    }

    /** @test */
    public function disabled_fields_dont_write_data()
    {
        $model = new Model(['slug' => 'initial-value']);

        $this->field->setDisabled();

        $data = [
            'main' => [
                'slug' => 'updated-value'
            ]
        ];

        $this->field->applyRequestData($model, $data);

        $this->assertEquals('updated-value', $model->slug);

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');

        $this->assertEquals('initial-value', $model->slug);
    }

    /** @test */
    public function read_only_fields_dont_write_data()
    {
        $model = new Model(['slug' => 'initial-value']);

        $this->field->setReadonly();

        $data = [
            'main' => [
                'slug' => 'updated-value'
            ]
        ];

        $this->field->applyRequestData($model, $data);

        $this->assertEquals('updated-value', $model->slug);

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is read only.');

        $this->assertEquals('initial-value', $model->slug);
    }

    /** @test */
    public function exception_thrown_when_not_enough_data_provided()
    {
        $model = new Model;
        $data = [
            'main' => [
                'slug' => '',
                'text' => '',
            ]
        ];

        $this->expectException(Exception::class); // Laravel 5.4 >
        // $this->expectException(Exception::class); // Laravel 5.5 <

        $this->section->applyRequestData($model, $data);

        $this->markTestIncomplete('This should throw a CrudException');

        $this->expectException(CrudException::class);

        $this->section->applyRequestData($model, $data);
    }

    /** @test */
    public function updates_a_model_with_altered_input_data()
    {
        $model = new Model(['slug' => 'initial-value']);
        $data = [
            'main' => [
                'slug' => 'new-value',
            ],
        ];

        $this->field->setAlterRequestDataCallback(function ($value) {
            return 'altered-' . $value;
        });

        $this->assertEquals('initial-value', $model->slug);

        $this->field->applyRequestData($model, $data);

        $this->assertEquals('new-value', $model->slug);

        $this->markTestIncomplete('This is incorrect. It should be running the data through the callback');

        $this->assertEquals('altered-new-value', $model->slug);
    }

    /** @test */
    public function sanitization_can_be_used_request_data()
    {
        $model = new Model(['slug' => 'Initial value']);
        $data = [
            'main' => [
                'slug' => 'new-value',
            ],
        ];

        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true);

        $sanitized = $this->field->getRequestData($data);

        $this->assertEquals('new-value', $sanitized);

        $this->markTestIncomplete('This is incorrect. It should be attempting to sanitize the data');

        $this->assertEquals('<p>new-value</p>', $sanitized);
    }

    /** @test */
    public function applies_filters_to_request_data()
    {
        $model = new Model(['slug' => 'initial-value']);
        $data = [
            'main' => [
                'slug' => 'new-value',
            ],
        ];

        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('new'),
        ]);

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('new-value', $model->slug);

        $this->markTestIncomplete('This is broken. It should be attempting to apply filters');

        $this->assertEquals('***-value', $model->slug);
    }

    /** @test */
    public function does_not_allow_duplicate_slugs()
    {
        Model::create(['slug' => 'new-value']);

        $model = new Model(['slug' => 'initial-value']);
        $data = [
            'main' => [
                'slug' => 'new-value',
            ],
        ];

        $this->assertEquals('initial-value', $model->slug);

        $this->field->applyRequestData($model, $data);

        $this->assertNotEquals('new-value', $model->slug);
    }

    /** @test */
    public function does_not_create_duplicate_slugs_when_generating_from_source_field()
    {
        Model::create(['slug' => 'new-value']);

        $model = new Model(['slug' => 'initial-value']);
        $data = [
            'main' => [
                'slug' => '',
                'text' => 'New Value',
            ],
        ];

        $this->assertEquals('initial-value', $model->slug);

        $this->field->applyRequestData($model, $data);

        $this->assertNotEquals('new-value', $model->slug);
    }

    /** @test */
    public function updates_a_model_with_0_input_data()
    {
        $model = new Model(['slug' => 'Initial value']);
        $data = [
            'main' => [
                'slug' => '0',
                'text' => 'New Value'
            ],
        ];

        $this->assertEquals('Initial value', $model->slug);

        $this->field->applyRequestData($model, $data);

        $this->assertEquals('new-value', $model->slug);

        $this->markTestIncomplete('This functionality is incorrect. It should return "0" instead of treating it as if empty');

        $this->assertEquals('0', $model->slug);
    }

    /** @test */
    public function updates_a_model_with_slug_generated_from_source_field_value_when_given_null_input_data()
    {
        $model = new Model(['slug' => 'Initial value']);
        $data = [
            'main' => [
                'slug' => null,
                'text' => 'New Value',
            ],
        ];

        $this->assertEquals('Initial value', $model->slug);

        $this->field->applyRequestData($model, $data);

        $this->assertEquals('new-value', $model->slug);
    }

    /** @test */
    public function updates_a_model_with_slug_generated_from_source_field_value_when_given_empty_string_input_data()
    {
        $model = new Model(['slug' => 'Initial value']);
        $data = [
            'main' => [
                'slug' => '',
                'text' => 'New Value'
            ],
        ];

        $this->assertEquals('Initial value', $model->slug);

        $this->field->applyRequestData($model, $data);

        $this->assertEquals('new-value', $model->slug);
    }

    /**
     * Translatable
     */

    /** @test */
    public function gets_translatable_form_data_correctly()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans([
            'slug' => [
                "en" => "en-value",
                "es" => "es-value",
            ],
        ]);

        $expected_output = [
            "en" => "en-value",
            "es" => "es-value",
        ];

        $data = $this->field->getFormData($model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function gets_translatable_default_form_data_when_source_not_available()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans;

        $data = $this->field->getFormData($model);

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = ['en' => 'default-value'];

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function gets_translatable_default_form_data_correctly_when_value_is_null()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans([
            'slug' => null,
        ]);

        $data = $this->field->getFormData($model);

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = ['en' => 'default-value'];

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function gets_translatable_default_form_data_correctly_when_value_is_empty_string()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans([
            'slug' => "",
        ]);

        $this->markTestIncomplete('This is broken. It tries to use empty string, which getTranslations cannot use. It should change empty string to empty array or throw an exception');

        $data = $this->field->getFormData($model);

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = ['en' => 'default-value'];

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function applies_translatable_request_data_correctly()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans;

        $data = [
            'main' => [
                'slug' => [
                    'en' => 'en-value',
                    'es' => 'es-value',
                ],
            ],
        ];

        $this->field->applyRequestData($model, $data);

        $this->assertEquals('en-value', $model->getTranslation('slug', 'en', true));
        $this->assertEquals('es-value', $model->getTranslation('slug', 'es', true));
    }

    /** @test */
    public function disabled_translatable_fields_dont_write_data()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans([
            'main' => [
                'slug' => [
                    'en' => 'en-value',
                    'es' => 'es-value',
                ]
            ]
        ]);

        $this->field->setDisabled();

        $data = [
            'main' => [
                'slug' => [
                    'en' => 'new-en-value',
                    'es' => 'new-es-value',
                ],
            ],
        ];

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('new-en-value', $model->getTranslation('slug', 'en', true));
        $this->assertEquals('new-es-value', $model->getTranslation('slug', 'es', true));

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');

        $this->assertEquals('en-value', $model->getTranslation('slug', 'en', true));
        $this->assertEquals('es-value', $model->getTranslation('slug', 'es', true));
    }

    /** @test */
    public function read_only_translatable_fields_dont_write_data()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans([
            'main' => [
                'slug' => [
                    'en' => 'en-value',
                    'es' => 'es-value',
                ]
            ]
        ]);

        $this->field->setReadonly();

        $data = [
            'main' => [
                'slug' => [
                    'en' => 'new-en-value',
                    'es' => 'new-es-value',
                ],
            ],
        ];

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('new-en-value', $model->getTranslation('slug', 'en', true));
        $this->assertEquals('new-es-value', $model->getTranslation('slug', 'es', true));

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is read only.');

        $this->assertEquals('en-value', $model->getTranslation('slug', 'en', true));
        $this->assertEquals('es-value', $model->getTranslation('slug', 'es', true));
    }

    /** @test */
    public function can_sanitize_translatable_data_when_writing_data()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans([
            'main' => [
                'slug' => [
                    'en' => 'en-value',
                    'es' => 'es-value',
                ]
            ]
        ]);

        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true);

        $data = [
            'main' => [
                'slug' => [
                    'en' => 'new-en-value',
                    'es' => 'new-es-value',
                ],
            ],
        ];

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('new-en-value', $model->getTranslation('slug', 'en', true));
        $this->assertEquals('new-es-value', $model->getTranslation('slug', 'es', true));

        $this->markTestIncomplete('This is incorrect. It should be attempting to sanitize the data');

        $this->assertEquals('<p>new-en-value</p>', $model->getTranslation('slug', 'en', true));
        $this->assertEquals('<p>new-es-value</p>', $model->getTranslation('slug', 'es', true));
    }

    /** @test */
    public function can_ultilise_alter_data_callbacks_for_translatable_data_when_writing_data()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans([
            'main' => [
                'slug' => [
                    'en' => 'en-value',
                    'es' => 'es-value',
                ]
            ]
        ]);

        $this->field->setAlterRequestDataCallback(function ($value) {
            return array_map(function ($single) {
                return 'altered-' . $single;
            }, $value);
        });

        $data = [
            'main' => [
                'slug' => [
                    'en' => 'new-en-value',
                    'es' => 'new-es-value',
                ],
            ],
        ];

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('new-en-value', $model->getTranslation('slug', 'en', true));
        $this->assertEquals('new-es-value', $model->getTranslation('slug', 'es', true));

        $this->markTestIncomplete('This is incorrect. It should be running the data through the callback');

        $this->assertEquals('altered-new-en-value', $model->getTranslation('slug', 'en', true));
        $this->assertEquals('altered-new-es-value', $model->getTranslation('slug', 'es', true));
    }

    /** @test */
    public function can_ultilise_filters_for_translatable_data_when_writing_data()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans([
            'main' => [
                'slug' => [
                    'en' => 'en-value',
                    'es' => 'es-value',
                ]
            ]
        ]);

        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('new'),
        ]);;

        $data = [
            'main' => [
                'slug' => [
                    'en' => 'new-en-value',
                    'es' => 'new-es-value',
                ],
            ],
        ];

        $this->section->applyRequestData($model, $data);

        $this->assertEquals('new-en-value', $model->getTranslation('slug', 'en', true));
        $this->assertEquals('new-es-value', $model->getTranslation('slug', 'es', true));

        $this->markTestIncomplete('This is broken. It should be attempting to apply filters');

        $this->assertEquals('***-en-value', $model->getTranslation('slug', 'en', true));
        $this->assertEquals('***-es-value', $model->getTranslation('slug', 'es', true));
    }

    /** @test */
    public function applies_translatable_request_data_correctly_when_data_is_null()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans;

        $data = [
            'main' => [
                'slug' => null,
                'text' => [
                    'en' => 'En New Value',
                ],
            ],
        ];

        $this->field->applyRequestData($model, $data);

        $this->assertEquals('en-new-value', $model->getTranslation('slug', 'en', false));
        $this->assertEquals('en-new-value', $model->getTranslation('slug', 'es', false));

        $this->markTestIncomplete('This should not be creating an "es" localization value');

        $this->assertEquals('en-new-value', $model->getTranslation('slug', 'en', false));
        $this->assertEquals('', $model->getTranslation('slug', 'es', false));
    }

    /** @test */
    public function applies_translatable_request_data_correctly_when_data_is_empty_string()
    {
        $this->setUpTranslatableConfig();

        $model = new ModelTrans;

        $data = [
            'main' => [
                'slug' => '',
                'text' => [
                    'en' => 'En New Value',
                ],
            ],
        ];

        $this->markTestIncomplete('This functionality is currently broken. It should convert the empty string to an empty array, or throw an exception');

        $this->field->applyRequestData($model, $data);

        $this->assertEquals('en-new-value', $model->getTranslation('slug', 'en', false));
        $this->assertEquals('', $model->getTranslation('slug', 'es', false));
    }
}
