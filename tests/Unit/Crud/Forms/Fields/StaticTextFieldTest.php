<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields;

use ErrorException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Yadda\Enso\Crud\Forms\Fields\StaticTextField;
use Yadda\Enso\Tests\Concerns\Field as BaseField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\Model;

class StaticTextFieldTest extends BaseFieldTest
{
    // use DatabaseMigrations;

    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->field = (new StaticTextField('text'))
            ->setDefaultValue('default-value');

        $this->setUpConfig();
    }

    /** @test */
    public function doesnt_have_a_component()
    {
        // It seems like expectException catching the expected exception
        // prevents further code (such as markTestIncomplete) from running
        // so this definition needs to be heres
        $this->markTestIncomplete('This doesn\'t see, right');

        $this->expectException(ErrorException::class);

        $this->assertEquals('enso-field-select', $this->field->getComponent());
    }
}
