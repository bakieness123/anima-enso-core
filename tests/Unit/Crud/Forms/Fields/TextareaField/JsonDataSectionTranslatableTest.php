<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields\TextareaField;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Config;
use TypeError;
use Yadda\Enso\Crud\Forms\Fields\TextareaField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\ModelJson;
use Yadda\Enso\Tests\Concerns\ModelJsonTrans;
use Yadda\Enso\Tests\Concerns\WordFilter;

/**
 * JsonDataSections get and set data differently enought form
 * regular sections that you can't just pass in a model
 * and directly set/get the data and have the results you would
 * get running it through the crud. It needs to take a step back
 * up the chain and have calls made directly on the Section object.
 */
class JsonDataSectionTranslatableTest extends BaseFieldTest
{
    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new ModelJsonTrans([
            'content' => [
                'textarea' => [
                    'en' => '<p>En Value</p>',
                    'es' => '<p>Es Value</p>',
                ]
            ]
        ]);

        $this->field = (new TextareaField('textarea'))
            ->setDefaultValue('<p>Default Value</p>');

        $this->setUpTranslatableJsonDataConfig();
    }

    /**
     * Data Reading
     */

    /** @test */
    public function reads_unaltered_form_data()
    {
        $expected_output = [
            'textarea' => [
                "en" => "<p>En Value</p>",
                "es" => "<p>Es Value</p>",
            ],
        ];

        $data = $this->section->getFormData($this->model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_falsey_but_valid()
    {
        $this->model = new ModelJsonTrans([
            'content' => [
                'textarea' => [],
            ]
        ]);

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = [
            'textarea' => [
                'en' => '<p>Default Value</p>',
            ],
        ];

        $data = $this->section->getFormData($this->model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_column_not_set()
    {
        $this->model = new ModelJsonTrans;

        $expected_output = [
            'textarea' => [
                'en' => '<p>Default Value</p>',
            ],
        ];

        $data = $this->section->getFormData($this->model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_field_not_set()
    {
        $this->model = new ModelJsonTrans(['content' => []]);

        $expected_output = [
            'textarea' => [
                'en' => '<p>Default Value</p>',
            ],
        ];

        $data = $this->section->getFormData($this->model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_null()
    {
        $this->model = new ModelJsonTrans([
            'content' => [
                'textarea' => null,
            ]
        ]);

        $data = $this->section->getFormData($this->model);
        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.

        $expected_output = [
            'textarea' => [
                'en' => '<p>Default Value</p>',
            ],
        ];

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_empty_string()
    {
        $this->model = new ModelJsonTrans([
            'content' => [
                'textarea' => "",
            ]
        ]);

        $data = $this->section->getFormData($this->model);

        $this->assertEquals(['textarea' => $this->field->getDefaultValue()], $data);
    }

    /** @test */
    public function reads_altered_form_data()
    {
        $this->field->setAlterFormDataCallback(function ($value) {
            $this->assertEquals([
                'en' => '<p>En Value</p>',
                'es' => '<p>Es Value</p>',
            ], $value);

            return [
                'en' => '<p>Modified En Value</p>',
                'es' => '<p>Modified Es Value</p>',
            ];
        });

        $expected_output = [
            'textarea' => [
                "en" => "<p>Modified En Value</p>",
                "es" => "<p>Modified Es Value</p>",
            ],
        ];

        $data = $this->section->getFormData($this->model);

        $this->assertEquals($expected_output, $data);
    }

    /**
     * Data Writing
     */

    /** @test */
    public function writes_unaltered_request_data()
    {
        $data = [
            'content' => [
                'textarea' => [
                    'en' => '<p>En Value</p>',
                    'es' => '<p>Es Value</p>',
                ],
            ],
        ];

        $expected_output = [
            'en' => '<p>En Value</p>',
            'es' => '<p>Es Value</p>',
        ];

        $this->section->applyRequestData($this->model, $data);

        // Need to use a workaround to circumvent HasTranslations overriding the getAttribute('content') method.
        $this->assertEquals($expected_output, json_decode($this->model->getAttributes()['content'], true)['textarea']);
    }

    /** @test */
    public function writes_request_data_when_data_is_falsey_but_valid()
    {
        $data = [
            'content' => [
                'textarea' => [],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $expected_output = ['en' => '<p>Default Value</p>'];

        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'textarea');

        $this->markTestIncomplete('This should not be setting a value when none are provided');

        // Desired State
        $expected_output = [];

        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'textarea');
    }

    /** @test */
    public function doesnt_write_request_data_when_data_is_not_set()
    {
        $data = [
            'content' => [],
        ];

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $this->assertTranslationsEqual(null, $this->model, 'content', 'text');

        $this->markTestIncomplete('This should not be setting a value when none are provided');

        // Desired State
        $expected_output = [
            'en' => '<p>En Value</p>',
            'es' => '<p>Es Value</p>',
        ];
        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'text');
    }

    /** @test */
    public function writes_request_data_when_data_is_null()
    {
        $data = [
            'content' => [
                'textarea' => null,
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $expected_output = [
            'en' => '<p>Default Value</p>',
        ];
        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'textarea');

        $this->markTestIncomplete('This should not be setting a value when none are provided');

        // Desired State
        $expected_output = [];
        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'textarea');
    }

    /** @test */
    public function writes_request_data_when_data_is_emtpy_string()
    {
        $data = [
            'content' => [
                'textarea' => '',
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $expected_output = ['en' => '<p>Default Value</p>'];
        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'textarea');

        $this->markTestIncomplete('Probably shouldn\'t do this. I think would expect it so set it to empty string');

        // Desired State
        $expected_output = [];
        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'textarea');
    }

    /** @test */
    public function does_not_write_bad_request_data()
    {
        $data = [
            'content' => [
                'textarea' => '<p>Bad Value</p>',
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $this->assertTranslationsEqual(null, $this->model, 'content', 'textarea');

        $this->markTestIncomplete('This is wrong. It shouldnt be overwriting good values when the values present are bad');

        // Desired State
        $expected_output = [
            'en' => "<p>En Value</p>",
            'es' => '<p>Es Value</p>',
        ];
        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'textarea');
    }

    /** @test */
    public function can_ultilise_alter_data_callbacks_when_writing_request_data()
    {
        $this->field->setAlterRequestDataCallback(function ($value) {
            $this->assertEquals([
                'en' => '<p>New En Value</p>',
                'es' => '<p>New Es Value</p>',
            ], $value);

            return [
                'en' => '<p>Altered En Value</p>',
                'es' => '<p>Altered Es Value</p>',
            ];
        });

        $data = [
            'content' => [
                'textarea' => [
                    'en' => '<p>New En Value</p>',
                    'es' => '<p>New Es Value</p>',
                ],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        $expected_output = [
            'en' => '<p>Altered En Value</p>',
            'es' => '<p>Altered Es Value</p>',
        ];

        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'textarea');
    }

    /** @test */
    public function can_sanitize_data_when_writing_request_data()
    {
        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true)
            ->setPurifyHtml(true);

        $data = [
            'content' => [
                'textarea' => [
                    'en' => 'New En Value',
                    'es' => 'New Es Value',
                ],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        $expected_output = [
            'en' => '<p>New En Value</p>',
            'es' => '<p>New Es Value</p>',
        ];

        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'textarea');
    }

    /** @test */
    public function can_ultilise_filters_when_writing_request_data()
    {
        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('New'),
        ]);
        ;

        $data = [
            'content' => [
                'textarea' => [
                    'en' => '<p>New En Value</p>',
                    'es' => '<p>New Es Value</p>',
                ],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        $expected_output = [
            'en' => '<p>*** En Value</p>',
            'es' => '<p>*** Es Value</p>',
        ];

        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'textarea');
    }

    /** @test */
    public function disabled_fields_dont_write_request_data()
    {
        $this->field->setDisabled();

        $data = [
            'content' => [
                'textarea' => [
                    'en' => '<p>New En Value</p>',
                    'es' => '<p>New Es Value</p>',
                ],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $expected_output = [
            'en' => '<p>New En Value</p>',
            'es' => '<p>New Es Value</p>',
        ];
        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'textarea');

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');

        // Desired State
        $expected_output = [
            'en' => '<p>En Value</p>',
            'es' => '<p>Es Value</p>',
        ];
        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'textarea');
    }

    /** @test */
    public function read_only_translatable_fields_dont_write_data_through_a_json_data_section()
    {
        $this->field->setReadonly();

        $data = [
            'content' => [
                'textarea' => [
                    'en' => '<p>New En Value</p>',
                    'es' => '<p>New Es Value</p>',
                ],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $expected_output = [
            'en' => '<p>New En Value</p>',
            'es' => '<p>New Es Value</p>',
        ];
        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'textarea');

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is disabled.');

        // Desired State
        $expected_output = [
            'en' => '<p>En Value</p>',
            'es' => '<p>Es Value</p>',
        ];
        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'textarea');
    }
}
