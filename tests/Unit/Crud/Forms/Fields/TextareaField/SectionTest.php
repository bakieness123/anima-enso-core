<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields\TextareaField;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Forms\Fields\TextareaField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\Model;
use Yadda\Enso\Tests\Concerns\ModelTrans;
use Yadda\Enso\Tests\Concerns\WordFilter;

class SectionTest extends BaseFieldTest
{
    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new Model(['textarea' => '<p>Initial Value</p>']);

        $this->field = (new TextareaField('textarea'))
            ->setDefaultValue('<p>Default Value</p>');

        $this->setUpConfig();
    }

    /**
     * Data Reading
     */

    /** @test */
    public function has_correct_component()
    {
        $this->assertEquals('enso-field-textarea', $this->field->getComponent());
    }

    /** @test */
    public function reads_unaltered_form_data()
    {
        $data = $this->field->getFormData($this->model);

        $this->assertEquals('<p>Initial Value</p>', $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_falsey_but_valid()
    {
        $this->model = new Model(['textarea' => '<p>0</p>']);

        $data = $this->field->getFormData($this->model);

        $this->assertEquals('<p>0</p>', $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_not_set()
    {
        $this->model = new Model();

        $data = $this->field->getFormData($this->model);

        // Current State
        $this->assertEquals(null, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "<p>Default Value</p>"');

        // Desired State
        $this->assertEquals('<p>Default Value</p>', $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_null()
    {
        $this->model = new Model(['textarea' => null]);

        $data = $this->field->getFormData($this->model);

        // Current State
        $this->assertEquals(null, $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "<p>Default Value</p>"');

        // Desired State
        $this->assertEquals('<p>Default Value</p>', $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_empty_string()
    {
        $this->model = new Model(['textarea' => '']);

        $data = $this->field->getFormData($this->model);

        // Current State
        $this->assertEquals('', $data);

        $this->markTestIncomplete('This functionality is incorrect. It should return "<p>Default Value</p>"');

        // Desired State
        $this->assertEquals('<p>Default Value</p>', $data);
    }

    /** @test */
    public function reads_altered_form_data()
    {
        $this->field->setAlterFormDataCallback(function ($value) {
            $this->assertEquals('<p>Initial Value</p>', $value);

            return '<p>Modified Value</p>';
        });

        $data = $this->field->getFormData($this->model);

        $this->assertEquals('<p>Modified Value</p>', $data);
    }

    /**
     * Data Writing
     */

    /** @test */
    public function writes_unaltered_request_data()
    {
        $data = [
            'main' => [
                'textarea' => '<p>New Value</p>',
            ],
        ];

        $this->assertEquals('<p>Initial Value</p>', $this->model->textarea);

        $this->field->applyRequestData($this->model, $data);

        $this->assertEquals('<p>New Value</p>', $this->model->textarea);
    }

    /** @test */
    public function writes_request_data_when_data_is_falsey_but_valid()
    {
        $data = [
            'main' => [
                'textarea' => '0',
            ],
        ];

        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', false);

        $this->assertEquals('<p>Initial Value</p>', $this->model->textarea);

        $this->field->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('<p>Default Value</p>', $this->model->textarea);

        $this->markTestIncomplete('This functionality is incorrect. It should save "0"');

        // Desired State
        $this->assertEquals('0', $this->model->textarea);
    }

    /** @test */
    public function writes_request_data_when_data_is_not_set()
    {
        $data = [
            'main' => [],
        ];

        $this->assertEquals('<p>Initial Value</p>', $this->model->textarea);

        $this->field->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('<p>Default Value</p>', $this->model->textarea);

        $this->markTestIncomplete('This functionality is incorrect. It should save no value');

        // Desired State
        $this->assertEquals(null, $this->model->textarea);
    }

    /** @test */
    public function writes_request_data_when_data_is_null()
    {
        $data = [
            'main' => [
                'textarea' => null,
            ],
        ];

        $this->assertEquals('<p>Initial Value</p>', $this->model->textarea);

        $this->field->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('<p>Default Value</p>', $this->model->textarea);

        $this->markTestIncomplete('This functionality is incorrect. It should save no value');

        // Desired State
        $this->assertEquals(null, $this->model->textarea);
    }

    /** @test */
    public function writes_request_data_when_data_is_empty_string()
    {
        $data = [
            'main' => [
                'textarea' => '',
            ],
        ];

        $this->assertEquals('<p>Initial Value</p>', $this->model->textarea);

        $this->field->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('<p>Default Value</p>', $this->model->textarea);

        $this->markTestIncomplete('This functionality is incorrect. It should save no value');

        // Desired State
        $this->assertEquals(null, $this->model->textarea);
    }

    /** @test */
    public function can_ultilise_alter_data_callbacks_when_writing_request_data()
    {
        $data = [
            'main' => [
                'textarea' => '<p>New Value</p>',
            ],
        ];

        $this->field->setAlterRequestDataCallback(function ($value) {
            $this->assertEquals('<p>New Value</p>', $value);

            return '<p>Altered Value</p>';
        });

        $this->assertEquals('<p>Initial Value</p>', $this->model->textarea);

        $this->field->applyRequestData($this->model, $data);

        $this->assertEquals('<p>Altered Value</p>', $this->model->textarea);
    }

    /** @test */
    public function can_sanitize_data_when_writing_request_data()
    {
        $data = [
            'main' => [
                'textarea' => 'New Value',
            ],
        ];

        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true)
            ->setPurifyHtml(true);

        $sanitized = $this->field->getRequestData($data);

        $this->assertEquals('<p>New Value</p>', $sanitized);
    }

    /** @test */
    public function can_ultilise_filters_when_writing_request_data()
    {
        $data = [
            'main' => [
                'textarea' => '<p>New Value</p>',
            ],
        ];

        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('New'),
            'mask_test' => (new WordFilter)->setWord('Value'),
        ]);

        $this->section->applyRequestData($this->model, $data);

        $this->assertEquals('<p>*** *****</p>', $this->model->textarea);
    }

    /** @test */
    public function disabled_fields_dont_write_request_data()
    {
        $this->field->setDisabled();

        $data = [
            'main' => [
                'textarea' => '<p>Updated Value</p>'
            ]
        ];

        $this->field->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('<p>Updated Value</p>', $this->model->textarea);

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');

        // Desired State
        $this->assertEquals('<p>Initial Value</p>', $this->model->textarea);
    }

    /** @test */
    public function read_only_fields_dont_write_request_data()
    {
        $this->field->setReadonly();

        $data = [
            'main' => [
                'textarea' => '<p>Updated Value</p>'
            ]
        ];

        $this->field->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('<p>Updated Value</p>', $this->model->textarea);

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is disabled.');

        // Desired State
        $this->assertEquals('<p>Initial Value</p>', $this->model->textarea);
    }
}
