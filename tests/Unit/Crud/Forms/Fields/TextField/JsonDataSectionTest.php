<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields\TextField;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\ModelJson;
use Yadda\Enso\Tests\Concerns\WordFilter;

/**
 * JsonDataSections get and set data differently enought form
 * regular sections that you can't just pass in a model
 * and directly set/get the data and have the results you would
 * get running it through the crud. It needs to take a step back
 * up the chain and have calls made directly on the Section object.
 */
class JsonDataSectionTest extends BaseFieldTest
{
    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new ModelJson([
            'content' => [
                'text' => 'Initial Value'
            ]
        ]);

        $this->field = (new TextField('text'))
            ->setDefaultValue('Default Value');

        $this->setUpJsonDataConfig();
    }

    /**
     * Data Reading
     */

    /** @test */
    public function reads_unaltered_form_data()
    {
        $expected_output = ['text' => 'Initial Value'];

        $this->assertEquals($expected_output, $this->section->getFormData($this->model));
    }

    /** @test */
    public function reads_form_data_when_source_value_is_falsey_but_valid()
    {
        $this->model = new ModelJson(['content' => ['text' => '0']]);

        $data = $this->section->getFormData($this->model);

        $expected_output = ['text' => '0'];
        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_column_not_set()
    {
        $this->model = new ModelJson;

        $data = $this->section->getFormData($this->model);

        // Current State
        $expected_output = ['text' => null];
        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This should be fetching a default value');

        // Desired State
        $expected_output = ['text' => 'Default Value'];
        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_field_not_set()
    {
        $this->model = new ModelJson(['content' => []]);

        $data = $this->section->getFormData($this->model);

        // Current State
        $expected_output = ['text' => null];
        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This should be fetching a default value');

        // Desired State
        $expected_output = ['text' => 'Default Value'];
        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_null()
    {
        $this->model = new ModelJson(['content' => ['text' => null]]);

        $data = $this->section->getFormData($this->model);

        // Current State
        $expected_output = ['text' => null];
        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This should be fetching a default value');

        // Desired State
        $expected_output = ['text' => 'Default Value'];
        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_empty_string()
    {
        $this->model = new ModelJson(['content' => ['text' => '']]);

        $data = $this->section->getFormData($this->model);

        // Current State
        $expected_output = ['text' => null];
        $this->assertEquals($expected_output, $data);

        $this->markTestIncomplete('This should be fetching a default value');

        // Desired State
        $expected_output = ['text' => 'Default Value'];
        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_altered_form_data()
    {
        $this->field->setAlterFormDataCallback(function ($value) {
            return 'Modified ' . $value;
        });

        $data = $this->section->getFormData($this->model);

        $expected_output = ['text' => 'Modified Initial Value'];

        $this->assertEquals($expected_output, $data);
    }

    /**
     * Data Writing
     */

    /** @test */
    public function writes_unaltered_request_data()
    {
        $data = [
            'content' => [
                'text' => 'New Value',
            ],
        ];

        $this->assertEquals('Initial Value', $this->model->content['text']);

        $this->section->applyRequestData($this->model, $data);

        $this->assertEquals('New Value', $this->model->content['text']);
    }

    /** @test */
    public function writes_request_data_when_data_is_falsey_but_valid()
    {
        $data = [
            'content' => [
                'text' => '0',
            ],
        ];

        $this->assertEquals('Initial Value', $this->model->content['text']);

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('Default Value', $this->model->content['text']);

        $this->markTestIncomplete('This functionality is incorrect. It should save "0"');

        // Desired State
        $this->assertEquals('0', $this->model->content['text']);
    }

    /** @test */
    public function does_not_write_request_data_when_data_is_not_set()
    {
        $data = [
            'content' => [],
        ];

        $this->assertEquals('Initial Value', $this->model->content['text']);

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('Default Value', $this->model->content['text']);

        $this->markTestIncomplete('It shouldnt be setting the modifying the value');

        // Desired State
        $this->assertEquals('Initial Value', $this->model->content['text']);
    }

    /** @test */
    public function writes_request_data_when_data_is_null()
    {
        $data = [
            'content' => [
                'text' => null,
            ],
        ];

        $this->assertEquals('Initial Value', $this->model->content['text']);

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('Default Value', $this->model->content['text']);

        $this->markTestIncomplete('This should be saving null');

        // Desired State
        $this->assertEquals(null, $this->model->content['text']);
    }

    /** @test */
    public function writes_request_data_when_data_is_emtpy_string()
    {
        $data = [
            'content' => [
                'text' => '',
            ],
        ];

        $this->assertEquals('Initial Value', $this->model->content['text']);

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('Default Value', $this->model->content['text']);

        $this->markTestIncomplete('This should be saving null');

        // Desired State
        $this->assertEquals(null, $this->model->content['text']);
    }

    /** @test */
    public function can_ultilise_alter_data_callbacks_when_writing_request_data()
    {
        $this->field->setAlterRequestDataCallback(function ($value) {
            return 'Altered ' . $value;
        });

        $data = [
            'content' => [
                'text' => 'New Value',
            ],
        ];

        $this->assertEquals('Initial Value', $this->model->content['text']);

        $this->section->applyRequestData($this->model, $data);

        $this->assertEquals('Altered New Value', $this->model->content['text']);
    }

    /** @test */
    public function can_sanitize_data_when_writing_request_data()
    {
        $data = [
            'content' => [
                'text' => 'New Value',
            ],
        ];

        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true)
            ->setPurifyHtml(true);

        $this->assertEquals('Initial Value', $this->model->content['text']);

        $this->section->applyRequestData($this->model, $data);

        $this->assertEquals('<p>New Value</p>', $this->model->content['text']);
    }

    /** @test */
    public function can_ultilise_filters_when_writing_request_data()
    {
        $data = [
            'content' => [
                'text' => 'New Test',
            ],
        ];

        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('New'),
            'mask_test' => (new WordFilter)->setWord('Test'),
        ]);

        $this->assertEquals('Initial Value', $this->model->content['text']);

        $this->section->applyRequestData($this->model, $data);

        $this->assertEquals('*** ****', $this->model->content['text']);
    }

    /** @test */
    public function disabled_fields_dont_write_request_data()
    {
        $this->field->setDisabled();

        $data = [
            'content' => [
                'text' => 'Updated Value',
            ],
        ];

        $this->assertEquals('Initial Value', $this->model->content['text']);

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('Updated Value', $this->model->content['text']);

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');

        // Desired State
        $this->assertEquals('Initial Value', $this->model->content['text']);
    }

    /** @test */
    public function read_only_fields_dont_write_request_data()
    {
        $this->field->setReadonly();

        $data = [
            'content' => [
                'text' => 'Updated Value',
            ],
        ];

        $this->assertEquals('Initial Value', $this->model->content['text']);

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('Updated Value', $this->model->content['text']);

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is disabled.');

        // Desired State
        $this->assertEquals('Initial Value', $this->model->content['text']);
    }
}
