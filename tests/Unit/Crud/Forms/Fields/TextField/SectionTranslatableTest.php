<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields\TextField;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Config;
use TypeError;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\Model;
use Yadda\Enso\Tests\Concerns\ModelTrans;
use Yadda\Enso\Tests\Concerns\WordFilter;

class SectionTranslatableTest extends BaseFieldTest
{
    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new ModelTrans([
            'text' => [
                "en" => "En Value",
                "es" => "Es Value",
            ],
        ]);

        $this->field = (new TextField('text'))
            ->setDefaultValue('Default Value');

        $this->setUpTranslatableConfig();
    }

    /**
     * Data Reading
     */

    /** @test */
    public function reads_unaltered_form_data()
    {
        $expected_output = [
            "en" => "En Value",
            "es" => "Es Value",
        ];

        $data = $this->field->getFormData($this->model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_valid_but_falsey()
    {
        $this->model = new ModelTrans([
            'text' => [],
        ]);

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = ['en' => 'Default Value'];

        $data = $this->field->getFormData($this->model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_not_set()
    {
        $this->model = new ModelTrans;

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = ['en' => 'Default Value'];

        $data = $this->field->getFormData($this->model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_null()
    {
        $this->model = new ModelTrans([
            'text' => null,
        ]);

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = ['en' => 'Default Value'];

        $data = $this->field->getFormData($this->model);

        $this->assertEquals($expected_output, $data);
    }

    /**
     * @test
     *
     * This isn't a state the Database 'should' ever reach.
     */
    public function reads_form_data_when_source_value_is_empty_string()
    {
        $this->model = new ModelTrans([
            'text' => '',
        ]);

        $data = $this->field->getFormData($this->model);

        $this->assertEquals($this->field->getDefaultValue(), $data);
    }

    /** @test */
    public function reads_altered_form_data()
    {
        $this->field->setAlterFormDataCallback(function ($value) {
            return array_map(function ($single) {
                return 'Modified ' . $single;
            }, $value);
        });

        $expected_output = [
            "en" => "Modified En Value",
            "es" => "Modified Es Value",
        ];

        $data = $this->field->getFormData($this->model);

        $this->assertEquals($expected_output, $data);
    }

    /**
     * Data Writing
     */

    /** @test */
    public function writes_unaltered_request_data()
    {
        $data = [
            'main' => [
                'text' => [
                    'en' => 'En Value',
                    'es' => 'Es Value',
                ],
            ],
        ];

        $this->field->applyRequestData($this->model, $data);

        $this->assertEquals('En Value', $this->model->getTranslation('text', 'en', true));
        $this->assertEquals('Es Value', $this->model->getTranslation('text', 'es', true));
    }

    /** @test */
    public function writes_request_data_when_data_is_falsey_but_valid()
    {
        $data = [
            'main' => [
                'text' => [],
            ],
        ];

        $this->field->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('Default Value', $this->model->getTranslation('text', 'en', false));
        $this->assertEquals('Es Value', $this->model->getTranslation('text', 'es', false));

        $this->markTestIncomplete('It should be setting the values to null or empty arrays');

        // Desired State
        $this->assertEquals(null, $this->model->getTranslation('text', 'en', false));
        $this->assertEquals(null, $this->model->getTranslation('text', 'es', false));
    }

    /** @test */
    public function does_not_write_request_data_when_data_is_not_set()
    {
        $data = [
            'main' => [],
        ];

        $this->field->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('Default Value', $this->model->getTranslation('text', 'en', false));
        $this->assertEquals('Es Value', $this->model->getTranslation('text', 'es', false));

        $this->markTestIncomplete('It shouldnt be setting the modifying the value');

        // Desired State
        $this->assertEquals('En Value', $this->model->getTranslation('text', 'en', false));
        $this->assertEquals('Es Value', $this->model->getTranslation('text', 'es', false));
    }

    /** @test */
    public function writes_request_data_when_data_is_null()
    {
        $data = [
            'main' => [
                'text' => null,
            ],
        ];

        $this->field->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('Default Value', $this->model->getTranslation('text', 'en', false));
        $this->assertEquals('Es Value', $this->model->getTranslation('text', 'es', false));

        $this->markTestIncomplete('It should be setting the values to null or empty arrays');

        // Current State
        $this->assertEquals(null, $this->model->getTranslation('text', 'en', false));
        $this->assertEquals(null, $this->model->getTranslation('text', 'es', false));
    }

    /** @test */
    public function writes_request_data_when_data_is_emtpy_string()
    {
        $data = [
            'main' => [
                'text' => '',
            ],
        ];

        $this->field->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('Default Value', $this->model->getTranslation('text', 'en', false));
        $this->assertEquals('Es Value', $this->model->getTranslation('text', 'es', false));

        $this->markTestIncomplete('It should be setting the values to empty strings or arrays');

        // Current State
        $this->assertEquals(null, $this->model->getTranslation('text', 'en', false));
        $this->assertEquals(null, $this->model->getTranslation('text', 'es', false));
    }

    /** @test */
    public function can_ultilise_alter_data_callbacks_when_writing_request_data()
    {
        $this->field->setAlterRequestDataCallback(function ($value) {
            return array_map(function ($single) {
                return 'Altered ' . $single;
            }, $value);
        });

        $data = [
            'main' => [
                'text' => [
                    'en' => 'New En Value',
                    'es' => 'New Es Value',
                ],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        $this->assertEquals('Altered New En Value', $this->model->getTranslation('text', 'en', true));
        $this->assertEquals('Altered New Es Value', $this->model->getTranslation('text', 'es', true));
    }

    /** @test */
    public function can_sanitize_data_when_writing_request_data()
    {
        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true)
            ->setPurifyHtml(true);

        $data = [
            'main' => [
                'text' => [
                    'en' => 'New En Value',
                    'es' => 'New Es Value',
                ],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        $this->assertEquals('<p>New En Value</p>', $this->model->getTranslation('text', 'en', true));
        $this->assertEquals('<p>New Es Value</p>', $this->model->getTranslation('text', 'es', true));
    }

    /** @test */
    public function can_ultilise_filters_when_writing_request_data()
    {
        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('New'),
        ]);
        ;

        $data = [
            'main' => [
                'text' => [
                    'en' => 'New En Value',
                    'es' => 'New Es Value',
                ],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        $this->assertEquals('*** En Value', $this->model->getTranslation('text', 'en', true));
        $this->assertEquals('*** Es Value', $this->model->getTranslation('text', 'es', true));
    }

    /** @test */
    public function disabled_fields_dont_write_request_data()
    {
        $this->field->setDisabled();

        $data = [
            'main' => [
                'text' => [
                    'en' => 'New En Value',
                    'es' => 'New Es Value',
                ],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('New En Value', $this->model->getTranslation('text', 'en', true));
        $this->assertEquals('New Es Value', $this->model->getTranslation('text', 'es', true));

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');

        // Desired State
        $this->assertEquals('En Value', $this->model->getTranslation('text', 'en', true));
        $this->assertEquals('Es Value', $this->model->getTranslation('text', 'es', true));
    }

    /** @test */
    public function read_only_fields_dont_write_request_data()
    {
        $this->field->setReadonly();

        $data = [
            'main' => [
                'text' => [
                    'en' => 'New En Value',
                    'es' => 'New Es Value',
                ],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $this->assertEquals('New En Value', $this->model->getTranslation('text', 'en', true));
        $this->assertEquals('New Es Value', $this->model->getTranslation('text', 'es', true));

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is disabled.');

        // Desired State
        $this->assertEquals('En Value', $this->model->getTranslation('text', 'en', true));
        $this->assertEquals('Es Value', $this->model->getTranslation('text', 'es', true));
    }
}
