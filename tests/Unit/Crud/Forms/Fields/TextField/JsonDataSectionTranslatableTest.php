<?php

namespace Yadda\Enso\Tests\Unit\Crud\Forms\Fields\TextField;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Config;
use TypeError;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Tests\Concerns\FieldTest as BaseFieldTest;
use Yadda\Enso\Tests\Concerns\ModelJson;
use Yadda\Enso\Tests\Concerns\ModelJsonTrans;
use Yadda\Enso\Tests\Concerns\WordFilter;

/**
 * JsonDataSections get and set data differently enought form
 * regular sections that you can't just pass in a model
 * and directly set/get the data and have the results you would
 * get running it through the crud. It needs to take a step back
 * up the chain and have calls made directly on the Section object.
 */
class JsonDataSectionTranslatableTest extends BaseFieldTest
{
    protected $field;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new ModelJsonTrans([
            'content' => [
                'text' => [
                    "en" => "En Value",
                    "es" => "Es Value",
                ],
            ]
        ]);

        $this->field = (new TextField('text'))
            ->setDefaultValue('Default Value');

        $this->setUpTranslatableJsonDataConfig();
    }

    /**
     * Data Reading
     */

    /** @test */
    public function JsonDataSectionTranslatable_reads_unaltered_form_data()
    {
        $expected_output = [
            'text' => [
                "en" => "En Value",
                "es" => "Es Value",
            ],
        ];

        $data = $this->section->getFormData($this->model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_falsey_but_valid()
    {
        $this->model = new ModelJsonTrans([
            'content' => [
                'text' => [],
            ],
        ]);

        $expected_output = [
            'text' => [
                'en' => 'Default Value',
            ],
        ];

        $data = $this->section->getFormData($this->model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_column_not_set()
    {
        $this->model = new ModelJsonTrans;

        $expected_output = [
            'text' => [
                'en' => 'Default Value',
            ],
        ];

        $data = $this->section->getFormData($this->model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_field_not_set()
    {
        $this->model = new ModelJsonTrans(['content' => []]);

        $expected_output = [
            'text' => [
                'en' => 'Default Value',
            ],
        ];

        $data = $this->section->getFormData($this->model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_null()
    {
        $this->model = new ModelJsonTrans([
            'content' => [
                'text' => null,
            ]
        ]);

        // 'en' is set as the fallback locale, and so will be
        // used to provide the a default langauge value from
        // the default value.
        $expected_output = [
            'text' => [
                'en' => 'Default Value',
            ],
        ];

        $data = $this->section->getFormData($this->model);

        $this->assertEquals($expected_output, $data);
    }

    /** @test */
    public function reads_form_data_when_source_value_is_empty_string()
    {
        $this->model = new ModelJsonTrans([
            'content' => [
                'text' => '',
            ]
        ]);

        $data = $this->section->getFormData($this->model);

        $this->assertEquals(['text' => $this->field->getDefaultValue()], $data);
    }

    /** @test */
    public function reads_altered_form_data()
    {
        $this->field->setAlterFormDataCallback(function ($value) {
            return array_map(function ($single) {
                return 'Modified ' . $single;
            }, $value);
        });

        $expected_output = [
            'text' => [
                "en" => "Modified En Value",
                "es" => "Modified Es Value",
            ],
        ];

        $data = $this->section->getFormData($this->model);

        $this->assertEquals($expected_output, $data);
    }

    /**
     * Data Writing
     */

    /** @test */
    public function writes_unaltered_request_data()
    {
        $data = [
            'content' => [
                'text' => [
                    'en' => 'New En Value',
                    'es' => 'New Es Value',
                ],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        $expected_output = [
            'en' => 'New En Value',
            'es' => 'New Es Value',
        ];

        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'text');
    }

    /** @test */
    public function writes_request_data_when_data_is_falsey_but_valid()
    {
        $data = [
            'content' => [
                'text' => [],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $expected_output = ['en' => 'Default Value'];

        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'text');

        $this->markTestIncomplete('This should not be setting a value when none are provided');

        // Desired State
        $expected_output = [];

        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'text');
    }

    /** @test */
    public function does_not_write_request_data_when_data_is_not_set()
    {
        $data = [
            'content' => [],
        ];

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $expected_output = ['en' => 'Default Value'];

        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'text');

        $this->markTestIncomplete('It shouldnt be setting the modifying the value');

        // Desired State
        $expected_output = [
            "en" => "En Value",
            "es" => "Es Value",
        ];

        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'text');
    }

    /** @test */
    public function writes_request_data_when_data_is_null()
    {
        $data = [
            'content' => [
                'text' => null,
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $expected_output = ['en' => 'Default Value'];

        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'text');

        $this->markTestIncomplete('This should not be setting a value when none are provided');

        // Desired State
        $expected_output = [];

        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'text');
    }

    /** @test */
    public function writes_request_data_when_data_is_emtpy_string()
    {
        $data = [
            'content' => [
                'text' => '',
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $expected_output = ['en' => 'Default Value'];

        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'text');

        $this->markTestIncomplete('Probably shouldn\'t do this. I think would expect it so set it to empty string');

        // Desired State
        $expected_output = ['en' => ''];

        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'text');
    }

    /** @test */
    public function does_not_write_bad_request_data()
    {
        $initial_state = [
            'en' => "En Value",
            'es' => 'Es Value',
        ];

        $data = [
            'content' => [
                'text' => 'Bad Value',
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $this->assertTranslationsEqual(null, $this->model, 'content', 'text');

        $this->markTestIncomplete('This is wrong. It shouldnt be overwriting good values when the values present are bad');

        // Desired State
        $this->assertTranslationsEqual($initial_state, $this->model, 'content', 'text');
    }

    /** @test */
    public function can_ultilise_alter_data_callbacks_when_writing_request_data()
    {
        $this->field->setAlterRequestDataCallback(function ($value) {
            return array_map(function ($single) {
                return 'Altered ' . $single;
            }, $value);
        });

        $data = [
            'content' => [
                'text' => [
                    'en' => 'New En Value',
                    'es' => 'New Es Value',
                ],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        $expected_output = [
            'en' => 'Altered New En Value',
            'es' => 'Altered New Es Value',
        ];

        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'text');
    }

    /** @test */
    public function can_sanitize_data_when_writing_request_data()
    {
        $this->field->setSanitizationSetting('AutoFormat.AutoParagraph', true)
            ->setPurifyHtml(true);

        $data = [
            'content' => [
                'text' => [
                    'en' => 'New En Value',
                    'es' => 'New Es Value',
                ],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        $expected_output = [
            'en' => '<p>New En Value</p>',
            'es' => '<p>New Es Value</p>',
        ];

        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'text');
    }

    /** @test */
    public function can_ultilise_filters_when_writing_request_data()
    {
        $this->field->addWriteFilters([
            'mask_new' => (new WordFilter)->setWord('New'),
        ]);
        ;

        $data = [
            'content' => [
                'text' => [
                    'en' => 'New En Value',
                    'es' => 'New Es Value',
                ],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        $expected_output = [
            'en' => '*** En Value',
            'es' => '*** Es Value',
        ];

        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'text');
    }

    /** @test */
    public function disabled_fields_dont_write_request_data()
    {
        $this->field->setDisabled();

        $data = [
            'content' => [
                'text' => [
                    'en' => 'New En Value',
                    'es' => 'New Es Value',
                ],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $expected_output = ['en' => 'New En Value', 'es' => 'New Es Value'];
        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'text');

        $this->markTestIncomplete('This is saving data, despite being field being disabled. Furthermore, there is no way to test if the field actually is disabled.');

        // Desired State
        $expected_output = ['en' => 'En Value', 'es' => 'Es Value'];
        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'text');
    }

    /** @test */
    public function read_only_fields_dont_write_request_data()
    {
        $this->field->setReadonly();

        $data = [
            'content' => [
                'text' => [
                    'en' => 'New En Value',
                    'es' => 'New Es Value',
                ],
            ],
        ];

        $this->section->applyRequestData($this->model, $data);

        // Current State
        $expected_output = ['en' => 'New En Value', 'es' => 'New Es Value',];
        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'text');

        $this->markTestIncomplete('This is saving data, despite being field being read_only. Furthermore, there is no way to test if the field actually is disabled.');

        // Desired State
        $expected_output = ['en' => 'En Value', 'es' => 'Es Value',];
        $this->assertTranslationsEqual($expected_output, $this->model, 'content', 'text');
    }
}
