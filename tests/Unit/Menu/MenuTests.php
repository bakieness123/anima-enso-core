<?php

namespace Yadda\Enso\Tests\Unit\Menu;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Yadda\Enso\EnsoMenu\Exceptions\MenuException;
use Yadda\Enso\EnsoMenu\Item;
use Yadda\Enso\EnsoMenu\Menu;
use Yadda\Enso\Tests\TestCase;

class MenuTests extends TestCase
{
    /** @test */
    public function a_new_menus_is_created_empty()
    {
        $menu = new Menu;

        $this->assertCount(0, $menu->getItems());
    }

    /** @test */
    public function an_empty_menu_can_have_an_item_added_as_an_array()
    {
        $menu = new Menu;

        $this->assertCount(0, $menu->getItems());

        $menu->addItem([]);

        $this->assertCount(1, $menu->getItems());
    }

    /** @test */
    public function an_empty_menu_can_have_an_item_added_directly()
    {
        $menu = new Menu;

        $item = new Item([]);

        $this->assertCount(0, $menu->getItems());

        $menu->addItem($item);

        $this->assertCount(1, $menu->getItems());
    }

    /** @test */
    public function a_menu_will_add_the_correct_depth_to_directly_added_items()
    {
        $menu = new Menu;
        $item = new Item([], 12);

        $menu->addItem($item);

        $this->assertEquals(1, $menu->getItems()->first()->getDepth());
    }

    /** @test */
    public function a_menu_exception_will_be_thrown_when_invalid_item_is_added()
    {
        $menu = new Menu;

        $this->expectException(MenuException::class);

        $menu->addItem('invalid-item-data');
    }

    /** @test */
    public function a_non_empty_menu_can_have_an_item_added()
    {
        $menu = new Menu;
        $menu->addItem([]);

        $this->assertCount(1, $menu->getItems());

        $menu->addItem([]);

        $this->assertCount(2, $menu->getItems());
    }

    /** @test */
    public function an_empty_menu_can_have_multiple_items_added_at_once_as_arrays()
    {
        $menu = new Menu;

        $this->assertCount(0, $menu->getItems());

        $menu->addItems([
            [],
            [],
        ]);

        $this->assertCount(2, $menu->getItems());
    }

    /** @test */
    public function an_empty_menu_can_have_multiple_items_added_at_once_as_items()
    {
        $menu = new Menu;

        $item_1 = new Item([]);
        $item_2 = new Item([]);

        $this->assertCount(0, $menu->getItems());

        $menu->addItems([
            $item_1,
            $item_2,
        ]);

        $this->assertCount(2, $menu->getItems());
    }

    /** @test */
    public function an_empty_menu_can_have_multiple_mixed_items_added_at_once()
    {
        $menu = new Menu;

        $item = new Item([]);

        $this->assertCount(0, $menu->getItems());

        $menu->addItems([
            [],
            $item,
        ]);

        $this->assertCount(2, $menu->getItems());
    }

    /** @test */
    public function an_non_empty_menu_can_have_multiple_items_added_at_once_as_arrays()
    {
        $menu = new Menu;

        $menu->addItem([]);

        $this->assertCount(1, $menu->getItems());

        $menu->addItems([
            [],
            [],
        ]);

        $this->assertCount(3, $menu->getItems());
    }

    /** @test */
    public function an_non_empty_menu_can_have_multiple_items_added_at_once_as_items()
    {
        $menu = new Menu;

        $menu->addItem([]);

        $item_1 = new Item([]);
        $item_2 = new Item([]);

        $this->assertCount(1, $menu->getItems());

        $menu->addItems([
            $item_1,
            $item_2,
        ]);

        $this->assertCount(3, $menu->getItems());
    }

    /** @test */
    public function an_non_empty_menu_can_have_multiple_mixed_items_added_at_once()
    {
        $menu = new Menu;

        $menu->addItem([]);

        $item = new Item([]);

        $this->assertCount(1, $menu->getItems());

        $menu->addItems([
            [],
            $item,
        ]);

        $this->assertCount(3, $menu->getItems());
    }

    /** @test */
    public function a_menu_returns_its_items_ordered_alphabetically()
    {
        $menu = new Menu;

        $item_1_data = ['label' => 'Evangeline'];
        $item_2_data = ['label' => 'Adamson'];
        $item_3_data = ['label' => 'David'];

        $menu->addItems([
            $item_1_data,
            $item_2_data,
            $item_3_data,
        ]);

        $items = $menu->getItems();

        $this->assertEquals('Adamson', $items->shift()->getLabel());
        $this->assertEquals('David', $items->shift()->getLabel());
        $this->assertEquals('Evangeline', $items->shift()->getLabel());
    }

    /** @test */
    public function a_menu_can_have_an_item_updated_by_its_label()
    {
        $menu = new Menu;

        $menu->addItem([
            'label' => 'test-item',
        ]);

        $this->assertEquals('fa fa-file-o', $menu->getItems()->first()->getIcon());

        $menu->updateItemByLabel('test-item', ['icon' => 'fa fa-bike-o']);

        $this->assertEquals('test-item', $menu->getItems()->first()->getLabel());
        $this->assertEquals('fa fa-bike-o', $menu->getItems()->first()->getIcon());
    }

    /** @test */
    public function a_menu_can_have_items_remove_by_label()
    {
        $menu = new Menu;

        $menu->addItem([
            'label' => 'test-item',
        ]);

        $this->assertCount(1, $menu->getItems());

        $menu->removeItemByLabel('test-item');

        $this->assertCount(0, $menu->getItems());
    }

    /** @test */
    public function retricted_items_wont_appear_in_the_items_list()
    {
        $menu = new Menu;

        $item = new Item([
            'restrict' => function ($item) {
                return false;
            }
        ]);

        $this->assertFalse($item->isAllowed());

        $menu->addItem($item);

        $this->assertCount(0, $menu->getItems());
    }
}
