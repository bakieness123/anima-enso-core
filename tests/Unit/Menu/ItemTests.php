<?php

namespace Yadda\Enso\Tests\Unit\Menu;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Yadda\Enso\EnsoMenu\Exceptions\MenuException;
use Yadda\Enso\EnsoMenu\Item;
use Yadda\Enso\Tests\TestCase;

class ItemTests extends TestCase
{
    /** @test */
    public function a_new_item_is_created_empty()
    {
        $item = new Item([]);

        $this->assertCount(0, $item->getItems());
    }

    /** @test */
    public function an_empty_item_can_have_a_child_item_added_as_an_array()
    {
        $item = new Item([]);

        $this->assertCount(0, $item->getItems());

        $item->addItem([]);

        $this->assertCount(1, $item->getItems());
    }

    /** @test */
    public function an_empty_item_can_have_a_child_item_added_directly()
    {
        $parent = new Item([]);

        $child = new Item([]);

        $this->assertCount(0, $parent->getItems());

        $parent->addItem($child);

        $this->assertCount(1, $parent->getItems());
    }

    /** @test */
    public function an_item_will_add_the_correct_depth_to_directly_added_child_items()
    {
        $parent = new Item([], 1);
        $child = new Item([], 12);

        $parent->addItem($child);

        $this->assertEquals(2, $parent->getItems()->first()->getDepth());
    }

    /** @test */
    public function a_menu_exception_will_be_thrown_when_invalid_item_is_added()
    {
        $parent = new Item([]);

        $this->expectException(MenuException::class);

        $parent->addItem('invalid-item-data');
    }

    /** @test */
    public function a_non_empty_menu_can_have_an_item_added()
    {
        $parent = new Item([]);
        $parent->addItem([]);

        $this->assertCount(1, $parent->getItems());

        $parent->addItem([]);

        $this->assertCount(2, $parent->getItems());
    }

    /** @test */
    public function an_empty_menu_can_have_multiple_items_added_at_once_as_arrays()
    {
        $parent = new Item([]);

        $this->assertCount(0, $parent->getItems());

        $parent->addItems([
            [],
            [],
        ]);

        $this->assertCount(2, $parent->getItems());
    }

    /** @test */
    public function an_empty_menu_can_have_multiple_items_added_at_once_as_items()
    {
        $parent = new Item([]);

        $child_item_1 = new Item([]);
        $child_item_2 = new Item([]);

        $this->assertCount(0, $parent->getItems());

        $parent->addItems([
            $child_item_1,
            $child_item_2,
        ]);

        $this->assertCount(2, $parent->getItems());
    }

    /** @test */
    public function an_empty_menu_can_have_multiple_mixed_items_added_at_once()
    {
        $parent = new Item([]);

        $child_item = new Item([]);

        $this->assertCount(0, $parent->getItems());

        $parent->addItems([
            [],
            $child_item,
        ]);

        $this->assertCount(2, $parent->getItems());
    }

    /** @test */
    public function an_non_empty_menu_can_have_multiple_items_added_at_once_as_arrays()
    {
        $parent = new Item([]);

        $parent->addItem([]);

        $this->assertCount(1, $parent->getItems());

        $parent->addItems([
            [],
            [],
        ]);

        $this->assertCount(3, $parent->getItems());
    }

    /** @test */
    public function an_non_empty_menu_can_have_multiple_items_added_at_once_as_items()
    {
        $parent = new Item([]);

        $parent->addItem([]);

        $child_item_1 = new Item([]);
        $child_item_2 = new Item([]);

        $this->assertCount(1, $parent->getItems());

        $parent->addItems([
            $child_item_1,
            $child_item_2,
        ]);

        $this->assertCount(3, $parent->getItems());
    }

    /** @test */
    public function an_non_empty_menu_can_have_multiple_mixed_items_added_at_once()
    {
        $parent = new Item([]);

        $parent->addItem([]);

        $child_item = new Item([]);

        $this->assertCount(1, $parent->getItems());

        $parent->addItems([
            [],
            $child_item,
        ]);

        $this->assertCount(3, $parent->getItems());
    }

    /** @test */
    public function a_menu_item_returns_its_items_ordered_alphabetically()
    {
        $parent = new Item([]);

        $child_item_3_data = ['label' => 'Ghaeran'];
        $child_item_2_data = ['label' => 'Tseren'];
        $child_item_1_data = ['label' => 'Abrinath'];

        $parent->addItems([
            $child_item_1_data,
            $child_item_2_data,
            $child_item_3_data,
        ]);

        $items = $parent->getItems();

        $this->assertEquals('Abrinath', $items->shift()->getLabel());
        $this->assertEquals('Ghaeran', $items->shift()->getLabel());
        $this->assertEquals('Tseren', $items->shift()->getLabel());
    }

    /** @test */
    public function a_menu_can_have_an_item_updated_by_its_label()
    {
        $parent = new Item([]);

        $parent->addItem([
            'label' => 'test-item',
        ]);

        $this->assertEquals('fa fa-file-o', $parent->getItems()->first()->getIcon());

        $parent->updateItemByLabel('test-item', ['icon' => 'fa fa-bike-o']);

        $this->assertEquals('test-item', $parent->getItems()->first()->getLabel());
        $this->assertEquals('fa fa-bike-o', $parent->getItems()->first()->getIcon());
    }

    /** @test */
    public function a_menu_can_have_items_remove_by_label()
    {
        $parent = new Item([]);

        $parent->addItem([
            'label' => 'test-item',
        ]);

        $this->assertCount(1, $parent->getItems());

        $parent->removeItemByLabel('test-item');

        $this->assertCount(0, $parent->getItems());
    }

    /** @test */
    public function restrictions_can_be_set_by_function()
    {
        $item = new Item([
            'restrict' => function ($item) {
                return false;
            }
        ]);

        $this->assertFalse($item->isAllowed());
    }
}
