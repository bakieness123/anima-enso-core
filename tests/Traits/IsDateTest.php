<?php

namespace Yadda\Enso\Tests\Traits;

use Carbon\Carbon;

trait IsDateTest
{
    protected function dateInstance($time_string)
    {
        return Carbon::parse($time_string);
    }

    protected function defaultDateInstance()
    {
        return $this->dateInstance('1900-01-01 00:00:00');
    }
    
    protected function validFormResponse(Carbon $date = null)
    {
        return json_decode(
            json_encode($date ?? $this->dateInstance('2000-01-01 12:00:00')),
            true
        );
    }
}
