<?php

namespace Yadda\Enso\Tests;

use Illuminate\Support\Facades\Route;
use Yadda\Enso\Categories\Controllers\Admin\CategoryController;
use Yadda\Enso\Categories\Crud\Category as CategoryCrud;
use Yadda\Enso\Categories\Models\Category;
use Yadda\Enso\Facades\EnsoCategories;
use Yadda\Enso\Tests\Concerns\Config;
use Yadda\Enso\Users\Models\Role;
use Yadda\Enso\Users\Models\User;
use Yadda\Enso\Users\Repositories\PermissionRepository;
use Yadda\Enso\Users\Repositories\RoleRepository;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    // /**
    //  * Creates the application.
    //  *
    //  * @return \Illuminate\Foundation\Application
    //  */
    // public function createApplication()
    // {
    //     $app = require __DIR__.'/../vendor/laravel/laravel/bootstrap/app.php';

    //     $app->make(Kernel::class)->bootstrap();

    //     return $app;
    // }

    /**
     * Add the parent setUp function by setting up an in-memory sqlite db
     * and then running all the migrations in this package
     */
    protected function setUp(): void
    {
        parent::setUp();

        // $this->withFactories(__DIR__ . '/Concerns/Database/Factories');
        // $this->withFactories(__DIR__ . '/../src/Categories/Database/Factories');

        Route::post('admin/categories', '\Yadda\Enso\Categories\Controllers\Admin\CategoryController@store')->name('admin.categories.store');
        Route::get('admin/categories', '\Yadda\Enso\Categories\Controllers\Admin\CategoryController@index')->name('admin.categories.index');
        Route::post('admin/categories/{category}', '\Yadda\Enso\Categories\Controllers\Admin\CategoryController@update')->name('admin.categories.update');
        Route::get('admin/users', '\Yadda\Enso\Users\Controllers\UserController@index')->name('admin.users.index');
        Route::get('admin/roles', '\Yadda\Enso\Users\Controllers\RoleController@index')->name('admin.roles.index');
        Route::get('admin/settings', '\Yadda\Enso\Settings\Controllers\SettingController@index')->name('admin.settings.index');
        Route::get('admin/menus', '\Yadda\Enso\SiteMenus\Controllers\MenuController@index')->name('admin.site-menus.index');
        Route::get('admin/pages', '\Yadda\Enso\Pages\Controllers\PageController@index')->name('admin.pages.index');

        EnsoCategories::routes('json/categories', 'Yadda\Enso\Categories\Controllers\CategoryController', 'json.categories');

        $public_path = base_path('../../../../../../../public');

        $this->app->instance('path.public', $public_path);
    }

    protected function getPackageProviders($app)
    {
        return [
            \Orchestra\Database\ConsoleServiceProvider::class,
            \TorMorten\Eventy\EventServiceProvider::class,
            \Mews\Purifier\PurifierServiceProvider::class,
            \Spatie\Translatable\TranslatableServiceProvider::class,
            \Mcamara\LaravelLocalization\LaravelLocalizationServiceProvider::class,
            \Yadda\Enso\Utilities\EnsoUtilitiesServiceProvider::class,
            \Yadda\Enso\EnsoServiceProvider::class,
            \Yadda\Enso\EnsoCategoriesServiceProvider::class,
            \Yadda\Enso\EnsoMediaServiceProvider::class,
            \Yadda\Enso\EnsoSettingsServiceProvider::class,
            \Yadda\Enso\EnsoUserServiceProvider::class,
            \Yadda\Enso\SiteMenus\EnsoSiteMenuServiceProvider::class,
            \Kalnoy\Nestedset\NestedSetServiceProvider::class,
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            'Alert' => \Yadda\Enso\Utilities\Alerts\Facades\AlertsFacade::class,
            'Enso' => \Yadda\Enso\Facades\EnsoFacade::class,
            'EnsoCategories' => \Yadda\Enso\Facades\EnsoCategories::class,
            'EnsoCrud' => \Yadda\Enso\Facades\EnsoCrud::class,
            'EnsoMedia' => \Yadda\Enso\Facades\EnsoMedia::class,
            'EnsoMenu' => \Yadda\Enso\Facades\EnsoMenu::class,
            'EnsoMeta' => \Yadda\Enso\Facades\EnsoMeta::class,
            'EnsoSettings' => \Yadda\Enso\Facades\EnsoSettings::class,
            'Eventy' => \TorMorten\Eventy\Facades\Events::class,
            'LaravelLocalization' => \Mcamara\LaravelLocalization\Facades\LaravelLocalization::class,
            'Purifier' => \Mews\Purifier\Facades\Purifier::class,
        ];
    }
    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => '',
        ]);
        $app['config']->set('enso.users.role_repository_concrete_class', RoleRepository::class);
        $app['config']->set('enso.users.permission_repository_concrete_class', PermissionRepository::class);
        $app['config']->set('enso.crud.test.config', Config::class);
    }

    protected function setSetting($key, $value, $type = 'scalar')
    {
        $setting = resolve(\Yadda\Enso\Settings\Contracts\Model::class)::firstOrNew(['slug' => $key]);

        $setting->name = $key;
        $setting->slug = $key;
        $setting->value = [
            "type" => $type,
            'content' => [
                'value' => $value,
            ],
        ];

        $setting->save();
    }

    protected function removeSetting($key)
    {
        $setting = resolve(\Yadda\Enso\Settings\Contracts\Model::class)->where('slug', $key)->first();

        if ($setting) {
            $setting->delete();
        }
    }

    protected function makeSuperuser($attributes = [])
    {
        $user = User::factory()->createOne();

        $role = Role::class;
        $admin_role = $role::where('slug', 'superuser')->first();

        if (!$admin_role) {
            $admin_role = Role::factory()->createOne([
                'name' => 'Superuser',
                'slug' => 'superuser',
            ]);
        }

        $user->addRole('superuser');

        return $user;
    }
}
