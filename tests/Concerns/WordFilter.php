<?php

namespace Yadda\Enso\Tests\Concerns;

use Yadda\Enso\Crud\Exceptions\CrudException;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\Fields\Filters\FieldFilterInterface;
use Yadda\Enso\Utilities\Filters\Filter;
use Yadda\Enso\Utilities\Filters\FilterException;

/**
 * A basic filter with known properties for testing that Filters are applied.
 */
class WordFilter extends Filter implements FieldFilterInterface
{
    public function setWord($word)
    {
        $this->word = $word;

        return $this;
    }

    public function applyFilter($value)
    {
        $replacement = str_pad('', strlen($this->word), "*");

        return preg_replace("@{$this->word}@", $replacement, $value);
    }

    /**
     * Applies this FieldFilter to the provided data, making
     * and necessary alterations based on the type of 
     * field the data belongs to.
     *
     * @param mixed $data
     * @param FieldInterface $field
     * 
     * @return mixed
     */
    public function applyFilterViaField($data, FieldInterface $field)
    {
        if ($field->getTranslatable()) {
            try {
                return $field->setTextData(array_map(function($single_item) {
                    return (string) $this->applyFilter($single_item);
                }, $field->getTextData($data)), $data);
            } catch (FilterException $e) {
                throw new CrudException($e);
            }
        }

        try {
            return $field->setTextData((string) $this->applyFilter($field->getTextData($data)), $data);
        } catch (FilterException $e) {
            throw new CrudException($e);
        }
    }
}