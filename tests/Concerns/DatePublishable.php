<?php

namespace Yadda\Enso\Tests\Concerns;

use Illuminate\Database\Eloquent\Model as BaseModel;
use Yadda\Enso\Crud\Contracts\Model\IsPublishable as ModelIsPublishable;
use Yadda\Enso\Crud\Traits\Model\IsPublishable;

class DatePublishable extends BaseModel implements ModelIsPublishable
{
    use IsPublishable;

    protected $table = "test_models";

    protected $guarded = [];

    protected $dates = [
        'publish_at',
    ];

    protected $casts = [
        'published' => 'boolean',
    ];

    /**
     * Gets the name of the Publish At DateTime column on this publishable
     *
     * @return string|null
     */
    public function getPublishAtColumn()
    {
        return 'publish_at';
    }
}
