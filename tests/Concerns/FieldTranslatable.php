<?php

namespace Yadda\Enso\Tests\Concerns;

use Yadda\Enso\Crud\Forms\Field;
use Yadda\Enso\Crud\Forms\FieldInterface;

class FieldTranslatable extends Field implements FieldInterface
{
    protected $tag_name = 'test-enso';

    protected $classes = ['initial_state'];
}