<?php

namespace Yadda\Enso\Tests\Concerns\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Yadda\Enso\Categories\Models\Category;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => 'Test Category',
            'slug' => 'test-category',
        ];
    }
}
