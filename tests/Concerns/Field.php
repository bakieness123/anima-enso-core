<?php

namespace Yadda\Enso\Tests\Concerns;

use Yadda\Enso\Crud\Forms\Field as BaseField;
use Yadda\Enso\Crud\Forms\FieldInterface;

class Field extends BaseField implements FieldInterface
{
    protected $tag_name = 'test-enso';

    protected $classes = ['test-class'];
}