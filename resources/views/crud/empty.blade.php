@extends(config('enso.settings.layout'))
{{-- This is an empty view that will only display Alert messages --}}

@section('content')
    <div class="container is-fluid">
      @include('enso-crud::partials.alerts')
    </div>
@endsection
