@foreach ($crud->getTableColumns() as $name => $value)
    <th>
        {{ is_numeric($name) ? $value : $name }}
    </th>
@endforeach
<th>
    Actions
</th>
