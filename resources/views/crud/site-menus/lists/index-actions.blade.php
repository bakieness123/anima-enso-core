@if (Auth::check() && Auth::user()->hasPermission('site-menu-create'))
  <a href="{{ route($crud->getRoute() . '.create') }}" class="button is-primary is-pulled-right">Create</a>
@endif
