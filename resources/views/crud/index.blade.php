@extends(config('enso.settings.layout'))

@section('content')
    <div class="container is-fluid">
        <header class="is-clearfix">
            <h1 class="title is-pulled-left">{{ $crud->getNamePlural() }}</h1>

            @include($crud->getCrudView('lists.index-actions'))
        </header>

        <hr>

        @include($crud->getCrudView('lists.index-head'))

        @include('enso-crud::partials.alerts')

        <enso-crud-index-table></enso-crud-index-table>
    </div>
@endsection
