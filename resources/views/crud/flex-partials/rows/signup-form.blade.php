@php
  $row_data = Yadda\Enso\Crud\Forms\Rows\SignupForm::unpack($row);
@endphp

<div data-label="{{ $row_data->row_label }}" id="{{ $row_data->row_id }}" class="md:max-w-2xl px-5 mx-auto my-10 md:my-12">
  @component('enso-crud::flex-partials.components.title', [
    'class' => 'text-center mb-4',
  ])
    {{ $row_data->title }}
  @endcomponent
  @component('enso-crud::flex-partials.components.content')
    {!! $row_data->content !!}
  @endcomponent
  @include('enso-crud::flex-partials.signup-forms.' . $row_data->type, [
    'row_data' => $row_data,
  ])
</div>
