@php
  $row_data = Yadda\Enso\Crud\Forms\Rows\Media::unpack($row);
  $image = $row_data->images->isNotEmpty() ? $row_data->images->first() : null;
  $is_carousel = empty($row_data->video) && $row_data->images->count() > 1;
@endphp

<div data-label="{{ $row_data->row_label }}" id="{{ $row_data->row_id }}" class="my-10 md:my-12 {{ $is_carousel ? '' : 'md:max-2-2xl sm:px-5 mx-auto lg:max-w-screen-lg' }}">
  @component('enso-crud::flex-partials.components.title', [
    'class' => 'mx-5 md:mx-auto md:max-2-2xl lg:max-w-screen-lg'
  ])
    {{ $row_data->title }}
  @endcomponent
  @component('enso-crud::flex-partials.components.media', [
    'video' => $row_data->video,
    'external_video' => $row_data->external_video,
    'images' => $row_data->images,
  ])
  @endcomponent
</div>
