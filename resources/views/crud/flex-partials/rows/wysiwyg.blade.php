@php
  $row_data = \Yadda\Enso\Crud\Forms\Sections\WysiwygSection::unpack($row);
  $id = $row_data->id ? $row_data->id : $id_prefix . '-' . $row_index;
@endphp
<section class="section" id="{{ $id }}">
  <div class="container">
    <div class="content">
      @if (!empty($row_data->title))
        <h2>{{ $row_data->title }}</h2>
      @endif
      @if (!empty($row_data->section_title))
        <h3>{{ $row_data->section_title }}</h3>
      @endif
      {!! $row_data->content !!}
    </div>
  </div>
</section>
