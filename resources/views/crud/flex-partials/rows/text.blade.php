@php
  $row_data = Yadda\Enso\Crud\Forms\Rows\Text::unpack($row);
@endphp

<div data-label="{{ $row_data->row_label }}" id="{{ $row_data->row_id }}" class="md:max-w-2xl mx-auto px-5 my-10 md:my-12">
  @component('enso-crud::flex-partials.components.title', [
    'class' => 'mb-5'
  ])
    {{ $row_data->title }}
  @endcomponent
  @component('enso-crud::flex-partials.components.content', [
    'class' => $row_data->style === 'feature' ? 'text-2xl' : ''
  ])
    {!! $row_data->content !!}
  @endcomponent
  @component('enso-crud::flex-partials.components.buttons', [
    'wrapper_class' => 'mt-8',
    'buttons' => $row_data->buttons,
  ])
  @endcomponent
</div>
