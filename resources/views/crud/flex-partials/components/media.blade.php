@php
  if (!empty($external_video)) {
    $type = 'external_video';
  } else if (!empty($video)) {
    $type = 'video';
  } else if ($images->count() === 1) {
    $type = 'image';
  } else if ($images->count() > 1) {
    $type = 'carousel';

    $carousel_content = $images->map(function ($image) {
      return [
        'image' => [
          'alt_text' => $image->alt_text,
          'urls' => [
            'size_768_x' => $image->getResizeUrl('768_x', true),
            'size_1536_x' => $image->getResizeUrl('1536_x', true),
            'size_1024_x' => $image->getResizeUrl('1024_x', true),
            'size_1920_x' => $image->getResizeUrl('1920_x', true),
            'size_3840_x' => $image->getResizeUrl('3840_x', true),
          ]
        ],
      ];
    });
  } else {
    $type = 'empty';
  }

  $first_image = $images->first();
@endphp

<div class="{{ $type !== 'carousel' ? 'bg-gray-300' : '' }}">
  @if ($type === 'external_video')
    <div class="aspect-16x9">
      @include('enso-fields::video-embed', [
        'video' => $external_video,
      ])
    </div>
  @endif

  @if ($type === 'video')
    <video class="w-full hidden md:block" poster="{{ $images->isNotEmpty() ? '' . $images->first()->getResizeUrl('16_9_1920', true) : '' }}" autoplay muted loop>
      <source src="{{ $video->getUrl() }}" type="{{  $video->mimetype }}">
    </video>
  @endif

  @if($type === 'image' || ($type === 'video' && $images->isNotEmpty()))
    <picture>
      <source
        srcset="{{ $first_image->getResizeUrl('768_x', true) }} 1x,
          {{ $first_image->getResizeUrl('1536_x', true) }} 2x`"
        sizes="100vw"
        media="(max-width: 1024px)">
      <source
        srcset="{{ $first_image->getResizeUrl('1024_x', true) }} 1024w,
              {{ $first_image->getResizeUrl('1920_x', true) }}  1920w,
              {{ $first_image->getResizeUrl('3840_x', true) }} 3840w"
        sizes="100vw"
        media="(min-width: 1024px)">
      <img
        src="{{ $first_image->getResizeUrl('1024_x', true) }}"
        alt="{{ $first_image->alt_text }}"
        class="{{ $type === 'video' ? 'md:hidden' : '' }}">
    </picture>
  @endif

  @if($type === 'carousel')
    <enso-carousel
      :slides="{{ json_encode($carousel_content) }}"
    >
      <div class="enso-carousel relative" slot-scope="{ prev, next, goto, isCurrentIndex, slides }">
        <div class="enso-carousel__slides">
          <picture
            v-for="(slide, index) in slides"
            :key="index"
            class="enso-carousel__slide"
          >
            <source
              :srcset="`${slide.image.urls.size_768_x} 1x, ${slide.image.urls.size_1536_x} 2x`"
              sizes="100vw"
              media="(max-width: 1024px)">
            <source
              :srcset="`${slide.image.urls.size_1024_x} 1024w,
                    ${slide.image.urls.size_1920_x} 1920w,
                    ${slide.image.urls.size_3840_x} 3840w`"
              sizes="100vw"
              media="(min-width: 1024px)">
            <img
              :src="slide.image.urls.size_1024_x"
              :alt="slide.image.alt_text">
          </picture>
        </div>

        <div class="flex align-center justify-center">
          <ul class="flex">
            <li class="p-2" v-for="(slide, index) in slides" :key="index">
              <button class="w-5 h-5 rounded-full border-none bg-gray-300 hover:bg-gray-400" @click="goto(index)" :class="{ 'bg-gray-600': isCurrentIndex(index) }">
                <span class="sr-only">@{{ index }}</span>
              </button>
            </li>
          </ul>
        </div>
      </div>
    </enso-carousel>
  @endif
</div>
