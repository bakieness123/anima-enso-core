@if ($row_data->form_url)
  <div class="text-center mt-6">
    <a href="{{ $row_data->form_url }}" target="_blank" rel="noopener noreferrer" class="button">
      {{ $row_data->button }}
    </a>
  </div>
@endif
