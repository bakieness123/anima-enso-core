<enso-crud-form-wrapper
    csrf-token="{{ $csrf_token }}"
    action="{{ $action }}"
    method="{{ $method }}"
></enso-crud-form-wrapper>
