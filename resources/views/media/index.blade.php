@extends(config('enso.settings.layout'))

@section('content')
  <div class="container is-fluid">
    <header class="is-clearfix">
      <h1 class="title is-pulled-left">Media Library</h1>
      <a href="{{ route($crud->getRoute() . '.index') }}" class="button is-primary is-pulled-right">List View</a>
    </header>

    <hr>

    <media-browser :max-file-size="{{ Config::get('enso.media.max_file_size') }}"></media-browser>
  </div>
@endsection
