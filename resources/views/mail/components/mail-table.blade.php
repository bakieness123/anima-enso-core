<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%">
    <tbody>
        <tr>
            <td align="center" valign="top" style="height:100%;margin:0;padding:0;width:100%">
                {{ $slot }}
            </td>
        </tr>
    </tbody>
</table>